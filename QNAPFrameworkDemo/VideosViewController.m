//
//  VideosViewController.m
//  QNAPFramework
//
//  Created by Change.Liao on 13/10/14.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "VideosViewController.h"
#import "QNVideoPlayerViewController.h"
#import "UPnPManager.h"
#import "MLFile.h"
#import "MLMediaLibrary.h"
#import "QNUpnpTableViewCell.h"
#import "VLCMediaFileDiscoverer.h"
@interface VideosViewController ()

@end

@implementation VideosViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    VLCMediaFileDiscoverer *discoverer = [VLCMediaFileDiscoverer sharedInstance];
    [discoverer addObserver:self];
    [discoverer startDiscovering:[self directoryPath]];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    /*
    UPnPManager *manager = [UPnPManager GetInstance];
    upnpDevices = [[manager DB] rootDevices];
    
    [[manager DB] addObserver:(UPnPDBObserver*)self];
    //Optional; set User Agent
    [[manager SSDP] setUserAgentProduct:@"QNAPPlayer" andOS:@"iOS"];
    
    [[manager SSDP] startSSDP];
    [[manager SSDP] searchSSDP];
    [[manager SSDP] SSDPDBUpdate];
    */
    [self updateContent];
}

- (void)UPnPDBWillUpdate:(UPnPDB*)sender{
    NSLog(@"UPnPDBWillUpdate %d", upnpDevices.count);
}

- (void)UPnPDBUpdated:(UPnPDB*)sender{
    NSLog(@"UPnPDBUpdated %d", upnpDevices.count);
    
    NSUInteger count = upnpDevices.count;
    BasicUPnPDevice *device;
    NSMutableArray *mutArray = [[NSMutableArray alloc] init];
    for (NSUInteger x = 0; x < count; x++) {
        device = upnpDevices[x];
        if ([[device urn] isEqualToString:@"urn:schemas-upnp-org:device:MediaServer:1"])
            [mutArray addObject:device];
    }
    _filteredUPNPDevices = nil;
    _filteredUPNPDevices = [NSArray arrayWithArray:mutArray];
    
    [self.tableView performSelectorOnMainThread : @ selector(reloadData) withObject:nil waitUntilDone:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger r = 0;
    switch (section) {
        case 0:
            r = [_foundMedia count];
            break;
        default:
            r = [_filteredUPNPDevices count];
            break;
    }
    return r;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section == 0)
        return @"Local Files";
    else
        return @"Local Network";
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"VideoCell";
    QNUpnpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[QNUpnpTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if([indexPath section] == 0){
        MLFile *thisSong =[_foundMedia objectAtIndex:[indexPath row]];
        [cell.upnpName setText:thisSong.title];
        [cell.upnpIcon setImage:thisSong.computedThumbnail];
        [cell.upnpDetail setText:thisSong.shortSummary];
        [cell.textLabel setText:thisSong.title];
    }
    else if([indexPath section] == 1){
        NSInteger row = [indexPath row];
        BasicUPnPDevice *device = _filteredUPNPDevices[row];
        [cell.upnpName setText:[device friendlyName]];
        [cell.upnpIcon setImage:[device smallIcon]];
        [cell.upnpDetail setText:[device udn]];
    }
    return cell;
}


#pragma mark - Navigation

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([indexPath section] == 0){
        //TODO 判斷是iphone or ipad
        QNVideoPlayerViewController *videoPlayer = [[QNVideoPlayerViewController alloc] initWithNibName:@"VLCMovieViewController~ipad" bundle:nil];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (cell == nil)
            return;
        videoPlayer.mediaItem = [_foundMedia objectAtIndex:[indexPath row]];
        [self presentViewController:videoPlayer animated:YES completion:nil];
    }else if([indexPath section] == 1){
        
    }
}

- (void)updateContent{
    _foundMedia = [NSMutableArray arrayWithArray:[MLFile allFiles]];
    [self.tableView reloadData];
}
#pragma mark - media discovering

- (void)mediaFileAdded:(NSString *)fileName loading:(BOOL)isLoading {
    if (!isLoading) {
        MLMediaLibrary *sharedLibrary = [MLMediaLibrary sharedMediaLibrary];
        [sharedLibrary addFilePaths:@[fileName]];
        
        /* exclude media files from backup (QA1719) */
        NSURL *excludeURL = [NSURL fileURLWithPath:fileName];
        [excludeURL setResourceValue:@YES forKey:NSURLIsExcludedFromBackupKey error:nil];
        
        // TODO Should we update media db after adding new files?
        [sharedLibrary updateMediaDatabase];
        [self updateContent];
    }
}

- (void)mediaFileDeleted:(NSString *)name {
    [[MLMediaLibrary sharedMediaLibrary] updateMediaDatabase];
    [self updateContent];
}

- (NSString *)directoryPath
{
#define LOCAL_PLAYBACK_HACK 0
#if LOCAL_PLAYBACK_HACK && TARGET_IPHONE_SIMULATOR
    NSString *directoryPath = @"/Users/fkuehne/Desktop/VideoLAN docs/Clips/sel/";
#else
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directoryPath = searchPaths[0];
#endif
    
    return directoryPath;
}

@end
