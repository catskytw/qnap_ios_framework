//
//  VideosViewController.h
//  QNAPFramework
//
//  Created by Change.Liao on 13/10/14.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VLCMediaFileDiscoverer.h"

@interface VideosViewController : UITableViewController<VLCMediaFileDiscovererDelegate>{
    NSMutableArray *upnpDevices;
    NSArray *_filteredUPNPDevices;
    NSMutableArray *_foundMedia;
}
@property(nonatomic, strong) IBOutlet UIButton *refreshBtn;
@end
