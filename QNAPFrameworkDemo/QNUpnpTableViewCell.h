//
//  QNUpnpTableViewCell.h
//  QNAPFramework
//
//  Created by Change.Liao on 2013/10/24.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QNUpnpTableViewCell : UITableViewCell
@property(nonatomic, strong) IBOutlet UIImageView *upnpIcon;
@property(nonatomic, strong) IBOutlet UILabel *upnpName;
@property(nonatomic, strong) IBOutlet UILabel *upnpDetail;
@end
