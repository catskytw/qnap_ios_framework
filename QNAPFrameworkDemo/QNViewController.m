//
//  QNViewController.m
//  QNAPFrameworkDemo
//
//  Created by Chen-chih Liao on 13/9/4.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNViewController.h"
#import "QNAPCommunicationManager.h"
#import <CocoaLumberjack/DDLog.h>
#import "SettingInfo.h"
#import <RestKit/RestKit.h>
#import "QNAPFrameworkUtil.h"
#import "QNAPFramework.h"
#import "VideosViewController.h"
#import "MLMediaLibrary.h"

@interface QNViewController ()

@end

@implementation QNViewController

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[MLMediaLibrary sharedMediaLibrary] performSelector:@selector(libraryDidAppear) withObject:nil afterDelay:1.];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[MLMediaLibrary sharedMediaLibrary] libraryDidDisappear];
}

- (void)viewDidLoad{
    [super viewDidLoad];

    [[QNAPCommunicationManager share] settingMisc:nil withDownloadSession:nil];

    [[QNAPCommunicationManager share] activateAllStation:@{
     @"NASURL":NASURL,
     @"MyCloudURL":MyCloudServerBaseURL,
     @"ClientId":CLIENT_ID,
     @"ClientSecret":CLIENT_SECRET,
     @"NASAccount":NAS_ACCOUNT,
     @"NASPassword":NAS_PASSWORD,
     @"MyCloudAccount":MyCloud_ACCOUNT,
     @"MyCloudPassword":MyCloud_PASSWORD
     }];
    

    self.fileStationManager = [QNAPCommunicationManager share].fileStationsManager;
    self.myCloudManager = [QNAPCommunicationManager share].myCloudManager;
    self.musicStationManager = [QNAPCommunicationManager share].musicStationManager;
    self.videoStationManager = [QNAPCommunicationManager share].videoStationManager;
    self.appCenterManager = [QNAPCommunicationManager share].appCenterManager;
    [self.fileStationManager loginWithAccount:NAS_ACCOUNT
                                 withPassword:NAS_PASSWORD
                             withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult, QNFileLogin *login){
                                 [self.loginLabel setText:@"登入成功"];
                             }
                             withFailureBlock:^(RKObjectRequestOperation *operation, QNFileLoginError *error){
                                 [self.loginLabel setText:@"登入失敗"];
                             }];

    [self.videoStationManager loginViaVideoStation:NAS_ACCOUNT
                                      withPassword:NAS_PASSWORD
                                  withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                      [self.loginLabel setText:@"登入成功"];
                                  }
                                  withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
                                      [self.loginLabel setText:@"登入失敗"];
                                  }];
    
    [self.myCloudManager fetchOAuthToken:MyCloud_ACCOUNT
                            withPassword:MyCloud_PASSWORD
                        withSuccessBlock:^(AFOAuthCredential *credential){
                            
                        } withFailureBlock:^(NSError *error){
                            
                        }];
    [QNAPFrameworkUtil waitUntilConditionBlock:^BOOL(){
        return ([QNAPCommunicationManager share].sidForMultimedia && [QNAPCommunicationManager share].sidForQTS);
    }];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.›‹‹
}

#pragma mark - ActionMethod
- (IBAction)uploadFile:(id)sender {
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"test" withExtension:@"mp4"];
    [self.videoStationManager uploadVideoFilm:@""
                         withLocalPath:url
                          withHomePath:videoFileHomePathPublic
                        withRemotePath:@""
              withNSURLSessionDelegate:self
                    withCompletedBlock:^(id response, NSError *error){
                        if (error)
                            DDLogError(@"upload error: %@", error);
                        else
                            DDLogInfo(@"uploading proceed! %@", response);
                    }];

}

- (IBAction)downloadSampleFile:(id)sender{
    [self.fileStationManager downloadFileWithFilePath:@"/Public"
                                         withFileName:@"1.mov"
                                             isFolder:NO
                                            withRange:nil
                                     withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                         [self.downloadPercent setText:@"下載完成"];
                                     }
                                     withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                         [self.downloadPercent setText:@"下載失敗"];
                                     }
                                  withInProgressBlock:^(long long r, long long t){
                                      CGFloat per = (CGFloat)r/(CGFloat)t;
                                      CGFloat rMB = r/1048576.0f;
                                      CGFloat tMB = t/1048576.0f;
                                      per *= 100.0f;
                                      
                                      NSString *s = [NSString stringWithFormat:@"p:%0.1f,r:%0.1f,t:%0.1f", per, rMB, tMB];
                                      [self.downloadPercent setText:s];
                                  }];
}

- (IBAction)openVideoPlayer:(id)sender{
    VideosViewController *songsList = [[VideosViewController alloc] initWithStyle:UITableViewStylePlain];
    [self presentViewController:songsList animated:YES completion:nil];
}

- (IBAction)openNasList:(id)sender{
}

- (IBAction)addNAS:(id)sender{
}

#pragma mark - NSURLSessionTaskDelegate
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    DDLogInfo(@"didBecomeInvalidWithError");
}

/* An HTTP request is attempting to perform a redirection to a different
 * URL. You must invoke the completion routine to allow the
 * redirection, allow the redirection with a modified request, or
 * pass nil to the completionHandler to cause the body of the redirection
 * response to be delivered as the payload of this request. The default
 * is to follow redirections.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
willPerformHTTPRedirection:(NSHTTPURLResponse *)response
        newRequest:(NSURLRequest *)request
 completionHandler:(void (^)(NSURLRequest *))completionHandler{
    DDLogInfo(@"willPerformHTTPRedirection");
}

/* The task has received a request specific authentication challenge.
 * If this delegate is not implemented, the session specific authentication challenge
 * will *NOT* be called and the behavior will be the same as using the default handling
 * disposition.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler{
    DDLogError(@"didReceiveChallenge");
}

/* Sent if a task requires a new, unopened body stream.  This may be
 * necessary when authentication has failed for any request that
 * involves a body stream.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
 needNewBodyStream:(void (^)(NSInputStream *bodyStream))completionHandler{
    DDLogVerbose(@"needNewBodyStream");
}

/* Sent periodically to notify the delegate of upload progress.  This
 * information is also available as properties of the task.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    DDLogVerbose(@"didSendBodyData, %lld send, %lld total, %lld expectedToSend", bytesSent, totalBytesSent, totalBytesExpectedToSend);
}

/* Sent as the last message related to a specific task.  Error may be
 * nil, which implies that no error occurred and this task is complete.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error{
    if (error) {
        DDLogError(@"didCompleteWithError %@", error);
    }else{
        DDLogInfo(@"upload task completed! task id: %@", task.taskDescription);
        DDLogVerbose(@"response %@", task.response);
    }
}
@end
