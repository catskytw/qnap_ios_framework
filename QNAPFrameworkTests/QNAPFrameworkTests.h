//
//  QNAPFrameworkTests.h
//  QNAPFrameworkTests
//
//  Created by Change.Liao on 13/9/2.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "QNAPCommunicationManager.h"
#import "QNViewController.h"
#import <CoreData/CoreData.h>

@interface QNAPFrameworkTests : XCTestCase<NSURLSessionTaskDelegate>{
    int _isUploadCompleted;
}
@property QNViewController *viewController;
@property QNAPCommunicationManager *communicationManager;
@property QNMyCloudManager *myCloudManager;
@property QNFileStationAPIManager *fileManager;
@property QNMusicStationAPIManager *musicManager;
@property QNVideoStationAPIManager *videoManager;
@property QNAppCenterAPIManager *appCenterManager;
@end
