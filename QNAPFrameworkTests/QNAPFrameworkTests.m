//
//  QNAPFrameworkTests.m
//  QNAPFrameworkTests
//
//  Created by Change.Liao on 13/9/2.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNAPFrameworkTests.h"
#import <CocoaLumberjack/DDLog.h>
#define EXP_SHORTHAND YES

#import <Expecta/Expecta.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/MagicalRecord.h>
#import "QNAPFramework.h"
#import "SettingInfo.h"
#import "QNAppDelegate.h"
#import "QNAPFrameworkUtil.h"
#import "QNMusicListResponse.h"
#import "QNFolderSummary.h"
#import "QNFolder.h"
#import "MyCloudCloudLinkResponse.h"
#import "QNSearchFileInfo.h"
#import "QNVideoFileItem.h"
#import "QNVideoFileList.h"
#import "QNVideoTimeLineResponse.h"
#import "QNVideoTimeLine.h"
#import "QNVideoDateItem.h"
#import "QNVideoCollectionResponse.h"
#import "QNCollection.h"
#import "QNVideoCollectionCreateResponse.h"
#import "QPKGItem.h"
#import "QNQPKGInstallStatus.h"
#import "QNService.h"
#import "MCCloudLinkFromDeviceResponse.h"


@implementation QNAPFrameworkTests

- (void)setUp {
    [super setUp];
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);

    QNAppDelegate *appDelegate = (QNAppDelegate *)[[UIApplication sharedApplication] delegate ];
    self.viewController = (QNViewController *)appDelegate.window.rootViewController;
    [[QNAPCommunicationManager share] settingMisc:nil withDownloadSession:nil];
    [Expecta setAsynchronousTestTimeout:60];

    self.myCloudManager = self.viewController.myCloudManager;
    self.fileManager = self.viewController.fileStationManager;
    self.musicManager = self.viewController.musicStationManager;
    self.videoManager = self.viewController.videoStationManager;
    self.appCenterManager = self.viewController.appCenterManager;
    __block BOOL hasCredential = NO;
    [self.myCloudManager fetchOAuthToken:^(AFOAuthCredential *credetial){
        hasCredential = YES;
    }
                        withFailureBlock:^(NSError *error){
                            hasCredential = YES;
                        }];
    
//    [QNAPFrameworkUtil waitUntilConditionBlock:^(void){
//        DDLogVerbose(@"wait OAuthToken...");
//        return hasCredential;
//    }];
}

- (void)tearDown {
    self.myCloudManager = nil;
    self.videoManager = nil;
    self.appCenterManager = nil;
    self.fileManager = nil;
    [super tearDown];
}

//#pragma mark - MyCloud TestCase
//- (void)testCase999_MyCloudFetchingToken {
//    //we should always test this function at the last case(set up the name as testCase999), or interference other mycloudAPIs testing.
//    __block AFOAuthCredential *_credential = nil;
//    [self.myCloudManager fetchOAuthToken:^(AFOAuthCredential *credential) {
//        DDLogInfo(@"credential %@", credential.accessToken);
//        _credential = credential;
//    }
//                        withFailureBlock:^(NSError *error) {
//                            _credential = [AFOAuthCredential new];
//                            DDLogError(@"error while acquiring accessToken %@", error);
//                        }
//     ];
//    while (!_credential.accessToken) {
//        NSDate* nextTry = [NSDate dateWithTimeIntervalSinceNow:0.1];
//        [[NSRunLoop currentRunLoop] runUntilDate:nextTry];
//    }
//}
//
//- (void)testCase12_MyCloudReadMyInformation {
//    __block RKObjectRequestOperation *_operation = nil;
//    [self.myCloudManager readMyInformation:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
//        _operation = operation;
//    }                    withFailiureBlock:nil];
//    expect(_operation).willNot.beNil();
//}
//
//- (void)testCase13_MyCloudUpdateMyInformation{
//    NSDictionary *userInfo = @{@"email":@"catskytw@gmail.com",
//                               @"first_name":@"Change",
//                               @"last_name":@"Chen",
//                               @"mobile_number":@"0912345678",
//                               @"language":@"ch",
//                               @"gender":[NSNumber numberWithInt:1],
//                               @"birthday":@"1976-10-20",
//                               @"subscribed":[NSNumber numberWithBool:YES]
//                               };
//    __block RKObjectRequestOperation *_operation = nil;
//    [self.myCloudManager updateMyInformation:userInfo
//                            withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
//                                _operation = operation;
//                            }
//                            withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error, MyCloudResponse *response) {
//                            }];
//    expect(_operation).willNot.beNil();
//}
//
//- (void)testCase14_MyCloudListMyActivities{
//    __block RKObjectRequestOperation *_operation = nil;
//    [self.myCloudManager listMyActivities:0
//                                withLimit:10
//                                   isDesc:YES
//                         withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                             _operation = operation;
//                         }
//                         withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error, MyCloudResponse *response){
//                         }];
//    
//    expect(_operation).willNot.beNil();
//}
//
//- (void)testCase15_MyCloudChangePassword{
//    __block BOOL hasResponse = NO;
//    [self.myCloudManager changeMyPassword:@"12345678"
//                          withNewPassword:@"12345678"
//                         withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingRestul){
//                             MyCloudResponse *response = [mappingRestul firstObject];
//                             DDLogInfo(@"changePassword response code:%@  message:%@",response.code, response.message);
//                             hasResponse = YES;
//                         }
//                         withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error, MyCloudResponse *response){
//                             hasResponse = YES;
//                         }];
//    
//    expect(hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase16_MyCloudGetCloudLink{
//    __block BOOL _finished = NO;
//    [self.myCloudManager getCloudLinkWithOffset:0
//                                      withLimit:0
//                                ithSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult, MyCloudCloudLinkResponse *cloudlink){
//                                    _finished = YES;
//                                    DDLogVerbose(@"myCloudLink %@", cloudlink.cloud_link_id);
//                                }
//                               withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                                   DDLogError(@"myCloudLink Error: %@", error);
//                                   _finished = NO;
//                               }];
//    expect(_finished).willNot.beFalsy();
//}
//#pragma mark - FileManager TestCase
//- (void)testCase9999_FileManagerLogin{
//    __block BOOL _hasResponse = false;
//    [self.fileManager loginWithAccount:NAS_ACCOUNT
//                          withPassword:NAS_PASSWORD
//                      withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult, QNFileLogin *login){
//                          _hasResponse = true;
//                          DDLogInfo(@"login information %@", login.authSid);
//                      }
//                      withFailureBlock:^(RKObjectRequestOperation *operation, QNFileLoginError *error){
//                          _hasResponse = true;
//                          if(error)
//                              DDLogError(@"Error while FileStationLogin %@", error.errorValue);
//                      }];
//    while (!self.fileManager.authSid) {
//        NSDate* nextTry = [NSDate dateWithTimeIntervalSinceNow:0.1];
//        [[NSRunLoop currentRunLoop] runUntilDate:nextTry];
//    }
//}
//
//- (void)testCase20_FileManagerDownloadFile{
//    __block BOOL _hasDownload = false;
//    [self.fileManager downloadFileWithFilePath:@"/Public"
//                                  withFileName:@"1.mov"
//                                      isFolder:NO
//                                     withRange:nil
//                              withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                                  DDLogVerbose(@"download success");
//                                  _hasDownload = YES;
//                              }
//                              withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                                  DDLogError(@"download error, Error: %@", error);
//                                  _hasDownload = YES;
//                              }
//                           withInProgressBlock:^(long long totalBytesRead, long long totalBytesExpectedToRead){
//                               DDLogVerbose(@"download file progress %lldbytes/%lldbytes", totalBytesRead, totalBytesExpectedToRead);
//                               _hasDownload = YES;
//                           }];
//    while (!_hasDownload){
//        NSDate* nextTry = [NSDate dateWithTimeIntervalSinceNow:0.1];
//        [[NSRunLoop currentRunLoop] runUntilDate:nextTry];
//    }
//}
//
//- (void)testCase21_FileManagerDownloadThumbnail{
//    __block BOOL _hasDownload = NO;
//    [self.fileManager thumbnailWithFile:@"1.JPG"
//                               withPath:@"/Public"
//                       withSuccessBlock:^(UIImage *image){
//                           DDLogVerbose(@"received thumbnailImage %@", image);
//                           _hasDownload = YES;
//                       }
//                       withFailureBlock:^(NSError *error){
//                           DDLogError(@"received thumbnail failiure %@", error);
//                           _hasDownload = YES;
//                       }
//                    withInProgressBlock:^(NSUInteger receivedSize, long long expectedSize){
//                        DDLogVerbose(@"received thumbnail %i bytes/%lld bytes", receivedSize, expectedSize);
//                    }];
//    while (!_hasDownload){
//        NSDate* nextTry = [NSDate dateWithTimeIntervalSinceNow:0.1];
//        [[NSRunLoop currentRunLoop] runUntilDate:nextTry];
//    }
//}
//
//- (void)testCase22_FileManagerSearchFiles{
//    __block BOOL _finished = NO;
//    [self.fileManager searchFiles:@"mp3"
//                   withSourcePath:@"/Multimedia"
//                    withSortField:QNFileModefiedTimeSort
//                  withLimitNumber:20
//                   withStartIndex:0
//                            isASC:NO
//                 withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *result, QNSearchResponse *obj){
//                     _finished = YES;
//                 }withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                     _finished = YES;
//                 }];
//     expect(_finished).willNot.beFalsy();
//}
//
//- (void)testCase23_FileManagerDeleteFile{
//    __block BOOL _finished = NO;
//    [self.fileManager deleteFilePath:@"/Public"
//                       withFileNames:@[@"1.jpg", @"music.png"]
//                    withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                        _finished = YES;
//                    }
//                    withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                        _finished = YES;
//                    }];
//    expect(_finished).willNot.beFalsy();
//}
//#pragma mark - MusicStationManager TestCase
//- (void)testCase30_MusicManagerGetFolderList{
//    __block BOOL _hasResponse = NO;
//    [self.musicManager getFolderListWithFolderID:nil
//                                withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                                    _hasResponse = YES;
//                                    [self analysisMusicResponse:[mappingResult firstObject]];
//                                }
//                                withFaliureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                                    _hasResponse = YES;
//                                }];
//    expect(_hasResponse).willNot.beFalsy();
//
//}
//
//- (void)testCase31_MusicManagerGetSongList{
//    __block BOOL _hasResponse = NO;
//    [self.musicManager getSongListWithArtistId:nil
//                              withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                                  _hasResponse = YES;
//                                  [self analysisMusicResponse:[mappingResult firstObject]];
//                              }
//                              withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                                  _hasResponse = YES;
//                              }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase32_MusicManagerGetAlbumList{
//    __block BOOL _hasResponse = NO;
//    [self.musicManager getAlbumListWithAlbumId:nil
//                                      pageSize:10
//                                      currPage:0
//                              withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                                  _hasResponse = YES;
//                                  [self analysisMusicResponse:[mappingResult firstObject]];
//                              }
//                              withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                                  _hasResponse = YES;
//                              }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase33_MusicManagerGetGenreList{
//    __block BOOL _hasResponse = NO;
//    [self.musicManager getGenreListWithGenreId:nil
//                                      pageSize:10
//                                      currPage:0
//                              withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                                  _hasResponse = YES;
//                                  [self analysisMusicResponse:[mappingResult firstObject]];
//                              }
//                              withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                                  _hasResponse = YES;
//                              }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase34_MusicManagerGetRecentList{
//    __block BOOL _hasResponse = NO;
//    [self.musicManager getRecentListWithPageSize:10
//                                        currPage:0
//                              withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                                  _hasResponse = YES;
//                                  [self analysisMusicResponse:[mappingResult firstObject]];
//                              }
//                              withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                                  _hasResponse = YES;
//                              }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase35_MusicManagerGetFavoriteList{
//    __block BOOL _hasResponse = NO;
//    [self.musicManager getMyFavoriteListWithSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//        _hasResponse = YES;
//        [self analysisMusicResponse:[mappingResult firstObject]];
//    }
//                                        withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                                            _hasResponse = YES;
//                                        }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase36_MusicManagerGetUPNPList{
//    __block BOOL _hasResponse = NO;
//    [self.musicManager getUPNPListWithLinkId:nil
//                            withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                                _hasResponse = YES;
//                                [self analysisMusicResponse:[mappingResult firstObject]];
//                            }
//                            withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                                _hasResponse = NO;
//                            }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase37_MusicManagerGetFile{
//    __block BOOL _hasResponse = NO;
//    [self.musicManager getFileWithFileID:@"4"
//                       withFileExtension:@""
//                        withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                            _hasResponse = YES;
//                        }
//                        withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                            _hasResponse = NO;
//                        }
//                     withInProgressBlock:^(long long totalBytesRead, long long totalBytesExpectedToRead){
//                         DDLogVerbose(@"MusicManager Get File received data: %lld/%lld", totalBytesRead, totalBytesExpectedToRead);
//                         _hasResponse = YES;
//                     }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase38_MusicManagerLogin{
//    __block BOOL _hasResponse = NO;
//    [self.musicManager loginForMultimediaSid:NAS_ACCOUNT
//                                withPassword:NAS_PASSWORD
//                            withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                                _hasResponse = YES;
//                            } withFailureBlock:^(RKObjectRequestOperation *o, NSError *error){
//                                _hasResponse = YES;
//                            }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//#pragma mark - VideoStation TestCase
- (void)testCase40_VideoManagerGetAllFileList{
    __block BOOL _hasResponse = NO;
    [self.videoManager getAllFileListWithSortBy:videoFileListSortByCreate
                                 withPageNumber:1
                              withCountsPerPage:200
                                   withHomePath:videoFileHomePathPublic
                                       withUUID:[[NSUUID UUID] UUIDString]
                                  withFilterDic:@{@"mt":@"HD"}
                                          isASC:YES
                               withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *m, QNVideoFileList *videoFileList){
                                   if(videoFileList){
                                       _hasResponse = YES;
                                       DDLogVerbose(@"getAllVideoFileList Done!!");
                                       NSArray *allItems = [videoFileList.relationship_FileItem allObjects];
                                       for (QNVideoFileItem *item in allItems) {
                                           DDLogVerbose(@"item id:%@", item.f_id);
                                       }
                                   }
                               }
                               withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                   _hasResponse = YES;
                                   DDLogError(@"getAllVideoFileList Failure! %@", e);
                               }];
    expect(_hasResponse).willNot.beFalsy();
}
//
//- (void)testCase41_VideoManagerGetTimeLineList{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager getTimeLineListWithHomePath:videoFileHomePathPublic
//                                  withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoTimeLineResponse *obj){
//                                      _hasResponse = YES;
//                                      NSArray *timeLines = [obj.relationship_timeLine allObjects];
//                                      for (QNVideoTimeLine *timeLine in timeLines) {
//                                          DDLogVerbose(@"TimeLinte:%@, count:%i", timeLine.yearMonth, [timeLine.count intValue]);
//                                          NSArray *dates = [timeLine.relationship_dateItem allObjects];
//                                          for (QNVideoDateItem *item in dates) {
//                                              DDLogVerbose(@"item date: %@, count:%i", item.date, [item.count intValue]);
//                                          }
//                                      }
//                                  }
//                                  withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                                      DDLogError(@"error in VideoGetTimeLineList %@", e);
//                                  }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase42_VideoManagerGetTimeLineFileList{
////    __block BOOL _hasResponse = NO;
////    [self.videoManager getTimeLineFileListWithTimeLineLabel:@"2013-04-16"
////                                                 withSortBy:videoFileListSortByCreate
////                                             withPageNumber:1
////                                          withCountsPerPage:20
////                                               withHomePath:videoFileHomePathPublic
////                                                      isASC:YES
////                                           withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *m, QNVideoFileList *videoFileList){
////                                               if(videoFileList){
////                                                   _hasResponse = YES;
////                                                   DDLogVerbose(@"getTimeLineFileList Done!!");
////                                                   NSArray *allItems = [videoFileList.relationship_FileItem allObjects];
////                                                   NSInteger count = 1;
////                                                   for (QNVideoFileItem *item in allItems) {
////                                                       DDLogVerbose(@"item id:%@, count:%i", item.f_id, count);
////                                                       count++;
////                                                   }
////                                               }
////                                           }
////                                            withFailueBlock:^(RKObjectRequestOperation *o, NSError *e){
////                                                _hasResponse = YES;
////                                            }];
////
////    expect(_hasResponse).willNot.beFalsy();
//
//}
//
//- (void)testCase43_VideoManagerGetCollections{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager getCollections:videoAlbums
//                             sortType:videoFileListSortByName
//                                isASC:YES
//                           pageNumber:1
//                          countInPage:50
//                     withSuccessBlock:^(RKObjectRequestOperation *r, RKMappingResult *m){
//                         _hasResponse = YES;
//                         QNVideoCollectionResponse *collectionResponse = (QNVideoCollectionResponse *)m.firstObject;
//                         NSArray *allCollections = [collectionResponse.relationship_DataList allObjects];
//                         for (QNCollection *collection in allCollections) {
//                             DDLogVerbose(@"collection name %@", collection.iVideoAlbumId);
//                         }
//
//                     }
//                     withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                         _hasResponse = YES;
//                     }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase44_VideoManagerGetVideoItemsFromCollection{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager getCollectionVideoItems:@"cJinsP" //WTF
//                                      sortType:videoFileListSortByName
//                                         isASC:YES
//                                    pageNumber:1
//                                   countInPage:50
//                                      homePath:videoFileHomePathPrivate withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                                          _hasResponse = YES;
//                                          QNVideoFileList *list = (QNVideoFileList *)r.firstObject;
//                                          NSArray *allItems = [list.relationship_FileItem allObjects];
//                                          for (QNVideoFileItem *item in allItems) {
//                                              DDLogVerbose(@"video cFileName %@", item.cFileName);
//                                          }
//                                      }
//                              withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                                  _hasResponse = YES;
//                              }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase45_VideoManagerDownloadVideoItems{
////    NSString *urlString =@"http://change419p.myqnapcloud.com:8080/video/api/video.php?a=download&f=6PBNDj&vt=default&sid=8c5717e99b49b0c23329cac105493089";
////    [self.videoManager downloadVideoFilm:urlString
////                           withLocalPath:nil
////                               witFilmID:@"6PBNDj"
////                            withDelegate:nil];
////    
////    int count = 0;
////    while (count < 10) {
////        NSDate* nextTry = [NSDate dateWithTimeIntervalSinceNow:0.1];
////        [[NSRunLoop currentRunLoop] runUntilDate:nextTry];
////        count++;
////    }
////    
//}
//
//- (void)testCase46_VideoManagerTrashCan{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager getTrashCanWithSortType:videoFileListSortByName
//                                         isASC:YES pageNumber:1
//                                   countInPage:50
//                              withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                                  _hasResponse = YES;
//                              }
//                              withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                                  _hasResponse = YES;
//                              }];
//    expect(_hasResponse).willNot.beFalsy();
//    
//}
//
//- (void)testCase47_VideoManagerAllFolderFileList{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager getFolderFileList:@""
//                            withHomePath:videoFileHomePathPublic
//                            withSortType:videoFileListSortByName
//                                   isASC:YES
//                              pageNumber:1
//                             countInPage:50
//                        withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                            _hasResponse = YES;
//                        }
//                        withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                            _hasResponse = YES;
//                        }];
//    expect(_hasResponse).willNot.beFalsy();
//
//}
//
//- (void)testCase48_VideoManagerAllRenders{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager getRenders:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                            _hasResponse = YES;
//                        }
//                        withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                            _hasResponse = YES;
//                        }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase49_VideoManagerAllPlayer{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager getRenders:^(RKObjectRequestOperation *o, RKMappingResult *r){
//        _hasResponse = YES;
//    }
//                 withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                     _hasResponse = YES;
//                 }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//#pragma mark - Private Methods
//- (void)analysisMusicResponse:(QNMusicListResponse *)response{
//    DDLogVerbose(@"QNFolderSummary pageSize:%@, totalCount: %i", [[response relationship_QNFolderSummary] pageSize], [[[response relationship_QNFolderSummary] relationship_QNFolder] count]);
//}
//
//- (void)testCase9999_VideoManagerLogin{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager loginViaVideoStation:NAS_ACCOUNT
//                               withPassword:NAS_PASSWORD
//                           withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                               _hasResponse = YES;
//                               DDLogVerbose(@"video station login successful!");
//                           }
//                           withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                               _hasResponse = YES;
//                               DDLogError(@"video station Login error! %@", e);
//                           }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase50_playContent{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager playContent:@"uuid:87ad13ef-4c91-e610-0771-e09d157b8b73"
//                        withFileId:@[@"G4e9m"]
//                  withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                      _hasResponse = YES;
//                      DDLogVerbose(@"video station login successful!");
//                  }
//                  withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                      _hasResponse = YES;
//                      DDLogError(@"video station Login error! %@", e);
//                  }];
//
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase51_createCollection{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager createCollection:@"createByIOS"
//                         collectionType:videoAlbums
//                               isOpened:YES
//                               isShared:YES
//                          isEditByOwner:YES
//                     withExpireDateTime:@"2015/01/01 00:00:00"
//                       withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QNVideoCollectionCreateResponse *obj){
//                           DDLogVerbose(@"obj output %@", obj.output);
//                           _hasResponse = YES;
//                       }
//                       withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                           DDLogError(@"error in createCollection");
//                           _hasResponse = YES;
//                       }];
//    expect(_hasResponse).willNot.beFalsy();
//
//}
//
//- (void)testCase52_email{
//    __block BOOL _hasResponse = NO;
//    [self.videoManager sendEmailForSharing:@"KE3d8p"
//                                    withIP:@"172.17.20.36"
//                                    sender:@"catskytw@gmail.com"
//                                  receiver:@"arestrasi@qnap.com"
//                              withSubtitle:@"This is a test email from the Unit test of qnap's framework in iOS."
//                               withComment:@"No comment is the best comment."
//                          withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                              DDLogVerbose(@"email success");
//                              _hasResponse = YES;
//                          }
//                          withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                              DDLogError(@"sendemail has some errors: %@, error code = %i", e.domain, e.code);
//                              _hasResponse = YES;
//
//                          }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase60_fetchServiceStatus{
//    __block BOOL _hasResponse = NO;
//    [self.appCenterManager getNASServiceStatus:^(RKObjectRequestOperation *o, RKMappingResult *r){
//        QNService *service = r.firstObject;
//        DDLogVerbose(@"service %@", service.service_id);
//        _hasResponse = YES;
//    }
//                              withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                                  _hasResponse = YES;
//                                  DDLogError(@"Error fetching service list");
//                              }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
- (void)testCase61_fetchQPKGItemStatus{
    __block BOOL _hasResponse = NO;
    [self.appCenterManager searchQPKGDownloadInfo:@"VideoStationPro"
                                 withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, QPKGItem *item){
                                     DDLogInfo(@"QPKG %@ searched!", item.internalName);
                                     _hasResponse = YES;
                                 }
                                 withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
                                     DDLogError(@"error! %@", e.domain);
                                     _hasResponse = YES;
                                 }];
//    expect(_hasResponse).willNot.beFalsy();
    [QNAPFrameworkUtil waitUntilConditionBlock:^(void){
        return _hasResponse;
    }];
}
//
//- (void)testCase62_installStation{
//    __block BOOL _hasResponse = NO;
//    [self.appCenterManager installQPKGFromURL:@"http://download.qnap.com/QPKG/VideoStationPro_2.0_all_140206.zip"
//                             withInternalName:@"VideoStationPro"
//                             withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                                 _hasResponse = YES;
//                                 DDLogVerbose(@"install successfully!");
//                             }
//                             withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                                 _hasResponse = YES;
//                                 DDLogError(@"install failure! %@", e);
//                             }];
//
//    expect(_hasResponse).willNot.beFalsy();
//
//}
//
//- (void)testCase63_enableVideoStation{
//    __block BOOL _hasResponse = NO;
//    [self.appCenterManager settingQPKGActivationWithQPKGName:@"VideoStationPro"
//                                                 enableValue:YES
//                                            withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r){
//                                                DDLogVerbose(@"enable videostation succcessfully!");
//                                                _hasResponse = YES;
//                                            }
//                                            withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                                                DDLogError(@"enable videostation in failure.");
//                                                _hasResponse = YES;
//                                            }];
//    expect(_hasResponse).willNot.beFalsy();
//
//}
//
//
//- (void)testCase64_getInstallStatus{
//    __block BOOL _hasResponse = NO;
//    [self.appCenterManager getInstallStatus:^(RKObjectRequestOperation *o, RKMappingResult *r, QNQPKGInstallStatus *status){
//        DDLogVerbose(@"getStatus: %@", status);
//        _hasResponse = YES;
//    }
//                           withFailureBlock:^(RKObjectRequestOperation *o, NSError *e){
//                               DDLogError(@"error in getInstallStatus %@", e);
//                               _hasResponse = YES;
//                           }];
//    expect(_hasResponse).willNot.beFalsy();
//
//}
//
//- (void)testCase65_getDeviceInfo{
//    __block BOOL _hasResponse = NO;
//    [self.myCloudManager getDeviceByName:@"Jeffery659"
//                        withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
//                            DDLogVerbose(@"get device successfully!");
//                            _hasResponse = YES;
//                        }
//                        withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
//                            DDLogError(@"error while fetching device information");
//                            _hasResponse = YES;
//                        }];
//    expect(_hasResponse).willNot.beFalsy();
//}
//
//- (void)testCase66_getCloudLink{
//    __block BOOL _hasResponse = NO;
//    [self.myCloudManager getCloudLinkWithDeviceId:@"5397e0ca75413f17572445a7"
//                                 withSuccessBlock:^(RKObjectRequestOperation *o, RKMappingResult *r, MyCloudCloudLinkResponse *cloudLinkResponse){
//                                     _hasResponse = YES;
//                                     MCCloudLinkFromDeviceResponse *myCloudLinkResponse = (MCCloudLinkFromDeviceResponse *)r.firstObject;
//                                     DDLogVerbose(@"cloudlink id: %@", myCloudLinkResponse.relationship_result.cloud_link_id);
//                                 }
//                                 withFailureBlock:^(RKObjectRequestOperation *o, NSError *error){
//                                     _hasResponse = YES;
//                                     DDLogError(@"getCloudLink error");
//                                 }];
//    expect(_hasResponse).willNot.beFalsy();
//}


- (void)testCase7_uploadTask{
    _isUploadCompleted = NO;
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"test" withExtension:@"mp4"];
    [self.videoManager uploadVideoFilm:@"TGtDVc"
                         withLocalPath:url
                          withHomePath:videoFileHomePathPublic
                        withRemotePath:@""
              withNSURLSessionDelegate:self
                      withStartupBlock:nil
                    withCompletedBlock:^(id response, NSError *error){
                        _isUploadCompleted = YES;
                        if (error)
                            DDLogError(@"upload error: %@", error);
                        else
                            DDLogInfo(@"uploading proceed! %@", response);
                    }];
    [QNAPFrameworkUtil waitUntilConditionYES:&_isUploadCompleted];
}

#pragma mark - NSURLSessionTaskDelegate
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error{
    DDLogInfo(@"didBecomeInvalidWithError");
}

/* An HTTP request is attempting to perform a redirection to a different
 * URL. You must invoke the completion routine to allow the
 * redirection, allow the redirection with a modified request, or
 * pass nil to the completionHandler to cause the body of the redirection
 * response to be delivered as the payload of this request. The default
 * is to follow redirections.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
willPerformHTTPRedirection:(NSHTTPURLResponse *)response
        newRequest:(NSURLRequest *)request
 completionHandler:(void (^)(NSURLRequest *))completionHandler{
    DDLogInfo(@"willPerformHTTPRedirection");
}

/* The task has received a request specific authentication challenge.
 * If this delegate is not implemented, the session specific authentication challenge
 * will *NOT* be called and the behavior will be the same as using the default handling
 * disposition.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler{
    _isUploadCompleted = YES;
    DDLogError(@"didReceiveChallenge");
}

/* Sent if a task requires a new, unopened body stream.  This may be
 * necessary when authentication has failed for any request that
 * involves a body stream.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
 needNewBodyStream:(void (^)(NSInputStream *bodyStream))completionHandler{
    DDLogVerbose(@"needNewBodyStream");
}

/* Sent periodically to notify the delegate of upload progress.  This
 * information is also available as properties of the task.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    DDLogVerbose(@"didSendBodyData, %lld send, %lld total, %lld expectedToSend", bytesSent, totalBytesSent, totalBytesExpectedToSend);
}

/* Sent as the last message related to a specific task.  Error may be
 * nil, which implies that no error occurred and this task is complete.
 */
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error{
    _isUploadCompleted = YES;
    DDLogError(@"didCompleteWithError");
}
@end
