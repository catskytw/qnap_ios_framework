//
//  QFTUTKTunnel.m
//  QFile
//
//  Created by QNAP on 12/19/12.
//  Copyright (c) 2012 Zoaks Co., Ltd. All rights reserved.
//
#import "QFTUTKTunnel.h"
//#import "QFNetworkCredentials.h"
#import "IOTCAPIs.h"
#import "RDTAPIs.h"

#define ZSLog(msg)

@implementation QFTUTKTunnel

@synthesize TUTKInitRet;
@synthesize TUTKAddress;

@synthesize connectInfoArray;

- (id)init
{
    self = [super init];
    if (self)
    {
        TUTKInitRet = -1;
        TUTKAddress = @"127.0.0.1";
        
        oplock = [[NSLock alloc]init];
        
        self.connectInfoArray = [[[NSMutableArray alloc] init] autorelease];
    }
    return self;
}

- (void)dealloc
{
    [connectInfoArray release];
    
    [TUTKAddress release];
    [oplock release];
    
    [super dealloc];
}

+ (QFTUTKTunnel *)sharedManager
{
    static QFTUTKTunnel *shareInstance = nil;
	if (!shareInstance)
    {
		shareInstance = [[QFTUTKTunnel alloc] init];
	}
	return shareInstance;
}

- (NSDictionary*)checkVLinkID:(NSString*)vID
{
    NSDictionary *dict = nil;
    
    int count = self.connectInfoArray.count;
    for (int i=0; i<count; i++)
    {
        NSDictionary *tmpDict = [self.connectInfoArray objectAtIndex:i];
        if (tmpDict)
        {
            NSString *tmpVlinkID = [tmpDict objectForKey:@"tutkVLinkID"];
            if (tmpVlinkID)
            {
                if ([tmpVlinkID isEqualToString:vID])
                {
                    dict = tmpDict;
                    break;
                }
            }
        }
    }
    
    return dict;
}

- (BOOL)removeFromSid:(NSInteger)sid
{
    int index = -1;
    
    int count = self.connectInfoArray.count;
    for (int i=0; i<count; i++)
    {
        NSDictionary *tmpDict = [self.connectInfoArray objectAtIndex:i];
        if (tmpDict)
        {
            NSString *tmpVlinkID = [tmpDict objectForKey:@"tutkSid"];
            if (tmpVlinkID)
            {
                if ([tmpVlinkID intValue] == sid)
                {
                    index = i;
                    break;
                }
            }
        }
    }
    
    if (index > -1)
    {
        [self.connectInfoArray removeObjectAtIndex:index];
    }
    
    return index > -1 ? YES:NO;
}

- (NSDictionary*)openTUTKTunnel:(NSString *)uid port:(NSInteger)port account:(NSString *)account password:(NSString *)password
{
    ZSLog(@"openTUTKTunnel");
    
    [oplock lock];
    
    NSMutableDictionary *returnData = nil;
    
    returnData = (NSMutableDictionary *)[self checkVLinkID:uid];
    if (!returnData)
    {
        // init
        NSInteger connectSID = -1;
        NSInteger portMappinRet = -1;
        NSInteger portNumber = 0;
        
        returnData = [NSMutableDictionary dictionary];
        
        if (TUTKInitRet < 0) // not init yet
        {
            ZSLog(@"P2PTunnelAgentInitialize");
            TUTKInitRet = P2PTunnelAgentInitialize(MAX_AGENT_CONNECTION);
        }
        
        sAuthData authData;
        strcpy(authData.szUsername, [account cStringUsingEncoding:NSASCIIStringEncoding]);
        strcpy(authData.szPassword, [password cStringUsingEncoding:NSASCIIStringEncoding]);
        
        int nErrFromDevice;
        if (TUTKInitRet >= 0)
        {
            ZSLog(@"P2PTunnelAgent_Connect");
            for (int i=0; i<5; i++)
            {
                connectSID = P2PTunnelAgent_Connect([uid UTF8String], (void *)&authData, sizeof(sAuthData), &nErrFromDevice);
                if (connectSID == IOTC_ER_DEVICE_NOT_LISTENING ||
                    connectSID == IOTC_ER_TCP_TRAVEL_FAILED ||
                    connectSID == IOTC_ER_TCP_CONNECT_TO_SERVER_FAILED ||
                    connectSID == IOTC_ER_SERVER_NOT_RESPONSE ||
                    connectSID == IOTC_ER_FAIL_SETUP_RELAY ||
                    connectSID == IOTC_ER_NETWORK_UNREACHABLE ||
                    connectSID == RDT_ER_TIMEOUT ||
                    connectSID == TUNNEL_ER_NETWORK_UNREACHABLE ||
                    connectSID == TUNNEL_ER_FAILED_SETUP_CONNECTION)
                {
                    sleep(3);
                }
                else
                {
                    break;
                }
            }
            
            if (connectSID >= 0)
            {
                NSInteger mappingPort = TUNNEL_START_PORT_MAPPING_NUMBER;
                while (mappingPort <= TUNNEL_END_PORT_MAPPING_NUMBER)
                {
                    portMappinRet = P2PTunnelAgent_PortMapping(connectSID, mappingPort, port);
                    if(portMappinRet >= 0)
                    {
                        ZSLog(@"P2PTunnelAgent_PortMapping");
                        portNumber = mappingPort;
                        P2PTunnelAgent_GetStatus(tunnelStatusCallBack,nil);
                        break;
                    }
                    mappingPort++;
                }
            }
        }
        
        if (TUTKInitRet >= 0 && connectSID >= 0 && portMappinRet >= 0 && portNumber > 0)
        {
            [returnData setValue:uid forKey:@"tutkVLinkID"];
            [returnData setValue:[NSString stringWithFormat:@"%d", connectSID] forKey:@"tutkSid"];
            [returnData setValue:[NSString stringWithFormat:@"%d", portMappinRet] forKey:@"tutkMappingPortIndex"];
            [returnData setValue:[NSString stringWithFormat:@"%d", portNumber] forKey:@"tutkMappingPort"];
            
            [self.connectInfoArray addObject:returnData];
        }
        else
        {
            if (portMappinRet >= 0)
            {
                ZSLog(@"OpenTunnel fail: Stop Port Mapping");
                P2PTunnelAgent_StopPortMapping(portMappinRet);
            }
            
            if (connectSID >= 0)
            {
                ZSLog(@"OpenTunnel fail: Disconnect");
                P2PTunnelAgent_Disconnect(connectSID);
            }
            
            if (self.connectInfoArray.count == 0)
            {
                if (TUTKInitRet >= 0)
                {
                    P2PTunnelAgentDeInitialize();
                    TUTKInitRet = -1;
                }
            }
        }
    }
    
    [oplock unlock];
    
    return returnData;
}

- (void)closeTUTKTunnel:(NSInteger)closeFlag sid:(NSInteger)sid index:(NSInteger)index
{
    ZSLog(@"closeTUTKTunnel");

    [oplock lock];
    
    BOOL exist = [self removeFromSid:sid];
    if (exist)
    {
        if (closeFlag & TUTK_TUNNEL_UNMAPPED)
        {
            if (index >= 0)
            {
                ZSLog(@"CloseTunnel: Stop Port Mapping");
                P2PTunnelAgent_StopPortMapping(index);
            }
        }
        
        if (closeFlag & TUTK_TUNNEL_DISCONNECT)
        {
            if (sid >= 0)
            {
                ZSLog(@"CloseTunnel: Disconnect");
                P2PTunnelAgent_Disconnect(sid);
            }
        }
        
        if (closeFlag & TUTK_TUNNEL_DEINITIALIZE)
        {
            if (self.connectInfoArray.count == 0)
            {
                if (TUTKInitRet >= 0)
                {
                    ZSLog(@"CloseTunnel: deinitialize");
                    P2PTunnelAgentDeInitialize();
                    TUTKInitRet = -1;
                }
            }
        }
    }
    
    [oplock unlock];
}

void __stdcall tunnelStatusCallBack(int nErrorCode ,int nSessionID ,void *pArg)
{
    switch (nErrorCode)
    {
        case TUNNEL_ER_DISCONNECTED:
            ZSLog(@"Call Back tunnel disconnect !!!!!!!!!!!!!!!");
            
            //TODO: post notification
//            NSMutableDictionary *infoData = [NSMutableDictionary dictionary];
//            [infoData setValue:[NSString stringWithFormat:@"%d", nSessionID] forKey:@"tutkConnectSid"];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kTUTKReconnectNotification object:nil userInfo:infoData];
            break;
        default:
            break;
    }
}

@end
