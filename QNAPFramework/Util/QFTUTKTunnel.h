//
//  QFTUTKTunnel.h
//  QFile
//
//  Created by QNAP on 12/19/12.
//  Copyright (c) 2012 Zoaks Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "P2PTunnelAPIs.h"

#define TUNNEL_START_PORT_MAPPING_NUMBER 		10000
#define TUNNEL_END_PORT_MAPPING_NUMBER 			10010

#define TUTK_TUNNEL_DEINITIALIZE    1
#define TUTK_TUNNEL_DISCONNECT      2
#define TUTK_TUNNEL_UNMAPPED        4

#define MAX_AGENT_CONNECTION 1

typedef struct st_AuthData
{
	char szUsername[64];
	char szPassword[64];
} sAuthData;

@interface QFTUTKTunnel : NSObject {
    NSInteger TUTKInitRet;
    NSString *TUTKAddress;
    
    NSLock *oplock;
    
    NSMutableArray *connectInfoArray;
}

@property (assign)NSInteger TUTKInitRet;
@property (assign, nonatomic)NSString *TUTKAddress;

@property (retain, nonatomic) NSMutableArray *connectInfoArray;


+ (QFTUTKTunnel *)sharedManager;

- (NSDictionary*)openTUTKTunnel:(NSString *)uid port:(NSInteger)port account:(NSString *)account password:(NSString *)password;
- (void)closeTUTKTunnel:(NSInteger)closeFlag sid:(NSInteger)sid index:(NSInteger)index;


static void __stdcall tunnelStatusCallBack(int nErrorCode ,int nSessionID ,void *pArg);
@end
