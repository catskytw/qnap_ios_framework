//
//  QNChromecastTool.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/7/28.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNChromecastTool.h"
#define DEFAULT_APP_LAUNCH_ID kGCKMediaDefaultReceiverApplicationID

@implementation QNChromecastTool
#pragma mark - Connection
- (void)initGooglecaseSender{
    self.deviceScanner = [[GCKDeviceScanner alloc] init];
    [self.deviceScanner addListener:self];
    
    self.onlineDevices = [NSMutableArray array];
    self.offlineDevices = [NSMutableArray array];
}

- (void)startScanDevices{
    [self.deviceScanner startScan];
}

- (void)connectToDevice:(GCKDevice *)targetDevice{
    _deviceManager = [[GCKDeviceManager alloc] initWithDevice:targetDevice clientPackageName:@"QNAPFramework"];
    _deviceManager.delegate = self;
    [_deviceManager connect];
}

- (void)launchApp{
    NSString *launchID = (self.appLaunchID)?self.appLaunchID:DEFAULT_APP_LAUNCH_ID;
    [_deviceManager launchApplication:launchID];
}

//videoTitle(NSString), imageURL(NSString), videoSubtitle(NSString), duration(CGFloat), imageWidth(CGFloat), imageHeight(CGFloat)
- (void)castVideo:(NSString *)videoAbsoluteURL startPosition:(NSInteger)position withInfoDic:(NSDictionary *)infoDic{
    NSString *videoTitle = infoDic[@"videoTitle"];
    NSString *videoSubtitle = infoDic[@"videoSubtitle"];
    NSString *imageURL = infoDic[@"imageURL"];
    CGFloat width = (infoDic[@"imageWidth"])?[infoDic[@"imageWidth"] floatValue]: 100.0f;
    CGFloat height = (infoDic[@"imageHeight"])?[infoDic[@"imageHeight"] floatValue]: 100.0f;
    CGFloat duration = (infoDic[@"duration"])?[infoDic[@"duration"] floatValue]: 0.0f;

    GCKMediaMetadata *_metaData = [[GCKMediaMetadata alloc] init];
    [_metaData setString:videoTitle forKey:kGCKMetadataKeyTitle];
    [_metaData setString:videoSubtitle
                  forKey:kGCKMetadataKeySubtitle];
    
    [_metaData addImage:[[GCKImage alloc] initWithURL:[NSURL URLWithString:imageURL] width:width height:height]];
    
    GCKMediaInformation *_information = [[GCKMediaInformation alloc] initWithContentID:videoAbsoluteURL
                                                                            streamType:GCKMediaStreamTypeNone
                                                                           contentType:@"video/mp4"
                                                                              metadata:_metaData
                                                                        streamDuration:duration
                                                                            customData:nil];
    NSInteger requestID = [_mediaControlChannel loadMedia:_information autoplay:TRUE playPosition:position];
    DDLogError(@"requestID %i", requestID);
}

#pragma mark - Playback Actions
- (void)devicePause{
    if (_mediaControlChannel && _mediaControlChannel.mediaStatus.playerState == GCKMediaPlayerStatePlaying) {
        [_mediaControlChannel pause];
    }
}

- (void)devicePlay{
    if (_mediaControlChannel && (_mediaControlChannel.mediaStatus.playerState == GCKMediaPlayerStatePaused || _mediaControlChannel.mediaStatus.playerState == GCKMediaPlayerStateIdle)) {
        [_mediaControlChannel play];
    }
}

- (void)deviceStop{
    if (_mediaControlChannel) {
        [_mediaControlChannel stop];
    }
}

- (void)deviceSeekPosition:(NSTimeInterval)position{
    [_mediaControlChannel seekToTimeInterval:position];
}

- (void)deviceMute:(BOOL)isMuted{
    [_mediaControlChannel setStreamMuted:isMuted];
}

- (void)deviceVolume:(CGFloat)volume{
    [_mediaControlChannel setStreamVolume:volume];
}

#pragma mark -
- (GCKMediaStatus *)playerStatus{
    return _mediaControlChannel.mediaStatus;
}
#pragma mark - ChannelDelegate
- (void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel
didFailToLoadMediaWithError:(NSError *)error{
    DDLogError(@"didFailToLoadMediaWithError: %@", error);
}

- (void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel
       requestDidFailWithID:(NSInteger)requestID
                      error:(NSError *)error{
    DDLogError(@"requestDidFailWithID: %@", error);
}

- (void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel
   requestDidCompleteWithID:(NSInteger)requestID{
    DDLogVerbose(@"requestDidCompleteWithID: %i", requestID);
}


#pragma mark - GooglecastConnectDelegate
- (void)deviceManagerDidConnect:(GCKDeviceManager *)deviceManager{
    [self launchApp];
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager didFailToConnectWithError:(NSError *)error{
    DDLogError(@"can't connect the google cast, error: %@", error);
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager
didFailToStopApplicationWithError:(NSError *)error{
    DDLogError(@"didFailToStopApplicationWithError: %@", error);
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager
didFailToConnectToApplicationWithError:(NSError *)error{
    DDLogError(@"didFailToConnectToApplicationWithError: %@", error);
}


- (void)deviceManager:(GCKDeviceManager *)deviceManager
didConnectToCastApplication:(GCKApplicationMetadata *)applicationMetadata
            sessionID:(NSString *)sessionID
  launchedApplication:(BOOL)launchedApplication{
    _mediaControlChannel = [[GCKMediaControlChannel alloc] init];
    _mediaControlChannel.delegate = self;
    [_deviceManager addChannel:_mediaControlChannel];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GCChannelReady" object:nil userInfo:nil];
}

#pragma mark - GooglecastDelegate
- (void)deviceDidComeOnline:(GCKDevice *)device{
    GCKDevice *sameDevice = [self searchDeviceInOffline:device];
    [self.offlineDevices removeObject:sameDevice];
    [self.onlineDevices addObject:device];
}

- (void)deviceDidGoOffline:(GCKDevice *)device{
    GCKDevice *sameDevice = [self searchDeviceInOnline:device];
    [self.onlineDevices removeObject:sameDevice];
    [self.offlineDevices addObject:device];
}

#pragma mark - PrivateMethod
- (GCKDevice *)searchDeviceInOnline:(GCKDevice *)targetDevice{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deviceID == %@", targetDevice.deviceID];
    NSArray *filteredArray = [self.onlineDevices filteredArrayUsingPredicate:predicate];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChromecastDeviceSearched" object:nil userInfo:nil];
    return filteredArray.count > 0 ? ((GCKDevice *)filteredArray.firstObject) : nil;
}

- (GCKDevice *)searchDeviceInOffline:(GCKDevice *)targetDevice{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"deviceID == %@", targetDevice.deviceID];
    NSArray *filteredArray = [self.offlineDevices filteredArrayUsingPredicate:predicate];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChromecastDeviceSearched" object:nil userInfo:nil];
    
    return filteredArray.count > 0 ? ((GCKDevice *)filteredArray.firstObject) : nil;
}


@end
