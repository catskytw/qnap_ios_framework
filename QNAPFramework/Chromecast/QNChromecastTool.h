//
//  QNChromecastTool.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/7/28.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleCast/GoogleCast.h>

@interface QNChromecastTool : NSObject <GCKDeviceScannerListener, GCKDeviceManagerDelegate, GCKMediaControlChannelDelegate>{
    GCKDeviceManager *_deviceManager;
    GCKMediaControlChannel *_mediaControlChannel;
}
@property (nonatomic, strong) NSString *appLaunchID;
@property (nonatomic, strong) NSMutableArray *onlineDevices;
@property (nonatomic, strong) NSMutableArray *offlineDevices;
@property (nonatomic, strong) GCKDeviceScanner *deviceScanner;
#pragma mark - Connection
- (void)initGooglecaseSender;

- (void)startScanDevices;

- (void)connectToDevice:(GCKDevice *)targetDevice;

- (void)launchApp;

/**
 *
 *
 *  @param videoAbsoluteURL videoURL
 *  @param position         start position, NSTimeInterval
 *  @param infoDic          information dictionary, the keys are: videoTitle(NSString), imageURL(NSString), videoSubtitle(NSString), duration(CGFloat), imageWidth(CGFloat), imageHeight(CGFloat)
 */
- (void)castVideo:(NSString *)videoAbsoluteURL startPosition:(NSInteger)position withInfoDic:(NSDictionary *)infoDic;
#pragma mark - Status
- (GCKMediaStatus *)playerStatus;

#pragma mark - Playback actions
- (void)devicePause;

- (void)devicePlay;

- (void)deviceStop;

- (void)deviceSeekPosition:(NSTimeInterval)position;

- (void)deviceMute:(BOOL)isMuted;

- (void)deviceVolume:(CGFloat)volume;

@end
