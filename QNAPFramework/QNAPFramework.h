//
//  QNAPFramework.h
//  QNAPFramework
//
//  Created by Change.Liao on 13/9/2.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/DDLog.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <RestKit/RestKit.h>

@class QNSearchResponse;
@class QNFileLoginError;
@class MyCloudResponse;
@class MyCloudCloudLinkResponse;
@class QNVideoFileList;
@class QNVideoTimeLineResponse;
@class QNDomainIPListResponse;
@class QNVideoCollectionCreateResponse;
@class QPKGItem;
@class QNQPKGInstallStatus;
#define CredentialIdentifier @"myCloudCredential"

/**
 login station flag
 */
#define LoginItemNone 0
#define LoginItemQTS (1<<0)
#define LoginItemMultiMedia (1<<1)
#define LoginItemMyCloud (1<<2)

#define LoginNone LoginItemNone
#define LoginQTS LoginItemQTS
#define LoginQTS_MultiMedia (LoginItemQTS | LoginItemMultiMedia)
#define LoginQTS_MultiMedia_MyCloud (LoginItemQTS | LoginItemMultiMedia | LoginItemMyCloud)

#define APLog(format, ...) NSLog(format, ## __VA_ARGS__)

#import "VLCConstants.h"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#ifdef __IPHONE_7_0
#define SYSTEM_RUNS_IN_THE_FUTURE SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")
#else
#define SYSTEM_RUNS_IN_THE_FUTURE NO
#endif

/**
 *  Expent the successBlockName
 *
 *  @param classname the string of QNSuccessBlock's extention and parameter.
 *
 *  @return a specified success block with the response object's name.
 */
#ifndef QNSuccessBlockExt
#define QNSuccessBlockExt(className) typedef void (^QN##className##SuccessBlock)(RKObjectRequestOperation *operation, RKMappingResult *result, className *obj)
#endif

#ifndef QNFailureBlockExt
#define QNFailureBlockExt(className) typedef void (^QN##className##FailureBlock)(RKObjectRequestOperation *operation, NSError *error, className *obj)
#endif

#pragma mark - successBlocks
QNSuccessBlockExt(QNDomainIPListResponse);
QNSuccessBlockExt(QNSearchResponse);
QNSuccessBlockExt(MyCloudCloudLinkResponse);
QNSuccessBlockExt(QNVideoFileList);
QNSuccessBlockExt(QNVideoTimeLineResponse);
QNSuccessBlockExt(QNVideoCollectionCreateResponse);
QNSuccessBlockExt(QPKGItem);
QNSuccessBlockExt(QNQPKGInstallStatus);

#pragma mark - failureBlocks
QNFailureBlockExt(QNFileLoginError);
QNFailureBlockExt(MyCloudResponse);
/**
 *  success block executing after API success
 *
 *  @param ^QNSuccessBlock RKObjectRequestOperation and RKObjectMapping, you can access the return objects from mappingResult
 */
typedef void (^QNSuccessBlock)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult);

/**
 *  Failure block executing after API failure, maybe including client error or server error.
 *
 *  @param ^QNFailureBlock <#^QNFailureBlock description#>
 */

typedef void (^QNFailureBlock)(RKObjectRequestOperation *operation, NSError *error);

/**
 *  <#Description#>
 */
typedef void (^QNInProgressBlock)(long long totalBytesRead, long long totalBytesExpectedToRead);