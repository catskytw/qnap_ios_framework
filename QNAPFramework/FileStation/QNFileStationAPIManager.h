//
//  QNFileStationAPIManager.h
//  QNAPFramework
//
//  Created by Chen-chih Liao on 13/9/3.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QNModuleBaseObject.h"
#import <RestKit/RestKit.h>
#import "QNFileLogin.h"
#import "QNFileLoginError.h"
#import "QNAPFramework.h"
#import "QNSearchResponse.h"

typedef NS_ENUM(NSInteger, QNFileSortType){
    QNFileNameSort,
    QNFileSizeSort,
    QNFileTypeSort,
    QNFileModefiedTimeSort,
    QNFilePrivilegeSort,
    QNFileOwnerSort,
    QNFileGroupSort
};


typedef NS_ENUM(NSInteger, QNCompressLevel){
    QNCompressLevelNormal,
    QNCompressLevelLarge,
    QNCompressLevelFast
};

typedef NS_ENUM(NSInteger, QNFileExistingOption){
    QNFileExistingSkip,
    QNFileExistingOverwrite
};


@interface QNFileStationAPIManager : QNModuleBaseObject{
    NSString *_authSid;
}
@property(nonatomic, strong) RKObjectManager *rkObjectManager;
@property(nonatomic, strong, readonly) NSString *authSid;
@property(nonatomic, strong) NSString *account;
- (void)setting;

/**
 *  login action in FileStations Version 1.0
 *  TODO: checkout the version above
 *
 *  @param account  the account you want to login
 *  @param password the password for account above
 *  @param success  a success block excuting while login success in asynchronized mode.
 *  @param failure  a failure block
 */
- (void)loginWithAccount:(NSString*)account withPassword:(NSString*)password withSuccessBlock:(void (^)(RKObjectRequestOperation *operation, RKMappingResult * mappingResult, QNFileLogin *loginInfo))success withFailureBlock:(void (^)(RKObjectRequestOperation *operation, QNFileLoginError *loginError))failure;


/**
 *  Get a thumbNail for a specified pic. This method uses the url as a key to search the image from cache first, and downloads from network if there is no image in cache.
 *  In next time, the image could be fetched from cache directly unlee executing [QNFileStationAPIManager clearThumnailCache];
 *
 *  Attention: You have to give the correct fileName and filePath which are following up the relative's rule in NSURL
 *  Please see https://developer.apple.com/library/ios/documentation/cocoa/reference/foundation/Classes/NSURL_Class/Reference/Reference.html#//apple_ref/doc/uid/20000301-4355
 *
 *  @param fileName   picName
 *  @param filePath   picPath
 *  @param success    executing block after loading successful
 *  @param failure    executing block after loading failure
 *  @param inProgress invoke this block while download
 */
- (void)thumbnailWithFile:(NSString *)fileName withPath:(NSString *)filePath withSuccessBlock:(void(^)(UIImage *image))success withFailureBlock:(void(^)(NSError *error))failure withInProgressBlock:(void(^)(NSUInteger receivedSize, long long expectedSize))inProgress;

/**
 *
 *
 *  @param success <#success description#>
 *  @param failure <#failure description#>
 */
- (void)getDomainIPListWithSuccessBlock:(QNQNDomainIPListResponseSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  Clean all images,which is downloaded from [QNFileStationAPIManager tuhmbnailWithFile:withPath:withSuccessBlock:withFailureBlock:withInProgressBlock:], in ImageCache
 */
- (void)clearThumbnailCache;

#pragma mark - Normal File Operating
/**
 *  create a new folder
 *
 *  @param folerName      folderName which you want
 *  @param folderDestPath the destination path where your folder exists.
 *  @param success        success block
 *  @param failure        failure block
 */
- (void)createFolder:(NSString *)folderName withDestPath:(NSString *)folderDestPath withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  rename a folder or a file
 *
 *  @param path       destination path where the target exists.
 *  @param sourceName original name
 *  @param destName   changed name
 *  @param success    success name
 *  @param failure    failure name
 */
- (void)renameWithPath:(NSString *)path withSoureName:(NSString *)sourceName withDestName:(NSString *)destName withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  copy a file/folder from the source-path to dest-path.
 *  In QNAP NAS 4.x, only support one file copying.
 *
 *  @param fileName    file name, which you want to copy.
 *  @param sourcePath  source path where the target(file/folder) exists
 *  @param destPath    destination path
 *  @param mode        1: skip, 0 overwrite
 *  @param dup         The duplication file name when copying the same destination with source file/folder(s);
 e.g. Copy a file/folder named “test” from /Public to /Public. (/Public/test → /Public/test-copy(1))
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)copyWithSourceFile:(NSString *)fileName withSourcePath:(NSString *)sourcePath withDestPath:(NSString *)destPath withMode:(QNFileExistingOption)mode withDupMode:(NSString *)dup withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  move a file/folder(s) from source-path to dest-path
 *
 *  @param sourceFile  the name of file/folder which you want to move
 *  @param totalNumber total number
 *  @param sourcePath  source path
 *  @param destPath    destination path
 *  @param mode        mode
 */
- (void)moveWithSourceFile:(NSArray *)fileNames withSourcePath:(NSString *)sourcePath withDestPath:(NSString *)destPath withMode:(QNFileExistingOption)mode withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

#pragma mark - Extract File Operating
/**
 *  list extract files.
 *
 *  @param path
 *  @param startIndex <#startIndex description#>
 *  @param limit      <#limit description#>
 *  @param sortType   <#sortType description#>
 *  @param isASC      <#isASC description#>
 *  @param success    <#success description#>
 *  @param failure    <#failure description#>
 */
- (void)extractFileListWithPath:(NSString *)path withStartIndex:(NSInteger)startIndex withLimitNumber:(NSInteger)limit withSortType:(NSInteger)sortType isASC:(BOOL)isASC withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure;

/**
 *  extracting file.
 *
 *  @param extractFilePath <#extractFilePath description#>
 *  @param destPath        <#destPath description#>
 *  @param password        <#password description#>
 *  @param extractMode     <#extractMode description#>
 *  @param overWrite       <#overWrite description#>
 *  @param pathMode        <#pathMode description#>
 *  @param success         <#success description#>
 *  @param failure         <#failure description#>
 */
- (void)extractFile:(NSString *)extractFilePath withDestPath:(NSString *)destPath withPassword:(NSString *)password withMode:(NSInteger)extractMode isOverWrite:(BOOL)overWrite withPathMode:(NSInteger)pathMode withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure;

/**
 *  cancel the extracting operation
 *
 *  @param pid     this pid value is returned in extracting file's API
 *  @param success success block
 *  @param failure failure block
 */
- (void)cancelExtraction:(NSString *)pid withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

#pragma mark - Compress file(s) operating
/**
 *  compress some files
 *
 *  @param compressedName     The compressed name
 *  @param compressType       compressed format(7z/zip), default is zip.
 *  @param password           password if needed.
 *  @param compressLevel      compress level, normal/large/fast, default:normal
 *  @param encrypt            7z:AES256, zip:ZipCrypto/AES256, null
 *  @param compressedFilePath The compressed file path
 *  @param totalNumber        The amountof compressed files
 *  @param compressFileName   The compressed file name
 *  @param success            success block
 *  @param failure            failure block
 */
- (void)compressFile:(NSString *)compressedName withType:(NSInteger)compressType withPassword:(NSString *)password withCompressLevel:(QNCompressLevel)compressLevel withEncrypt:(NSInteger)encrypt withDestPath:(NSString *)compressedFilePath withTotalNumber:(NSInteger)totalNumber withCompressFileName:(NSString *)compressFileName withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  cancel the compressing process
 *
 *  @param pid     pid in the response of Compressed File API
 *  @param success success block
 *  @param failure failure block
 */
- (void)cancelCompress:(NSString *)pid withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  get the status of compressing process
 *
 *  @param pid     pid in the response of the compressing process
 *  @param success success block
 *  @param failure failure block
 */
- (void)getCompressingStatus:(NSString *)pid withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

#pragma mark - Downloading/Uploading
/**
 *  Download a file. You can use the 'fileRange' parameter to request some specified binary data of the file. Resume downloading is based on the machinism mentioned before.
 *
 *  @param filePath   file path
 *  @param fileName   file name
 *  @param isFolder   a directory or not.
 *  @param fileRange  the range of bytes, the defulat value is nil which means to download a whole file.
 *  @param success    success block
 *  @param failure    failure block
 *  @param inProgress the block of updateing downloading progress
 */
- (void)downloadFileWithFilePath:(NSString *)filePath withFileName:(NSString *)fileName isFolder:(BOOL)isFolder withRange:(NSRange *)fileRange withSuccessBlock:(void (^)(RKObjectRequestOperation *operation, RKMappingResult * mappingResult))success withFailureBlock:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure withInProgressBlock:(void(^)(long long totalBytesRead, long long totalBytesExpectedToRead))inProgress;

//TODO: upload? how to do that?
//need to create upload function

//TODO: open a file?how to do that?

#pragma mark - File/Folder Informations

/**
 *  get folder list under some specified path
 *
 *  @param targetFolderPath folder path
 *  @param isISO            is it a iso share? 1:YES, 2:NO, default: 0
 *  @param success          success block
 *  @param failure          failure block
 */
- (void)getFolderList:(NSString *)targetFolderPath isISO:(BOOL)isISO withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  get file list, including files and folders
 *
 *  @param folderPath       target path
 *  @param limit            number of response datas
 *  @param sortType         sort type, including filename, filesize, filetype, mt, privilege, owner, group
 *  @param startIndex       response data which starting from.
 *  @param isISO            is an ISO file?
 *  @param isHidden         want to list hidden files?
 *  @param multimediaOption There is a dictionary of multimedia files, you can pass these information or nil if you don't need these.
 The dictionary is described as below:
 key:type  value: integer,1~3  (1:music, 2:video, 3:photo)
 key:mp4_360 value: BOOL
 key:mp4_720 value: BOOL
 key:flv_720 value: BOOL
 key:filename value: NSString, search video file name
 
 If you give any wrong information, you may get some unexcepted responses.
 *  @param successExt       success block
 *  @param failure          failure block
 */
- (void)getFileList:(NSString *)folderPath withLimit:(NSInteger)limit withSortField:(QNFileSortType)sortType withStartIndex:(NSInteger)startIndex isISOShare:(BOOL)isISO isASC:(BOOL)isASC hasHiddenFile:(BOOL)hasHidden withMultimediaOption:(NSDictionary *)multimediaOption withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure;

/**
 *  get total file size(including hidden files and folders)
 *
 *  @param folderPath  folder path
 *  @param totalNumber The number of file/folder which are caculated the total size
 *  @param fileName    file/folder names
 *  @param successExt  success block
 *  @param failure     failure block
 */
- (void)getTotalFileSize:(NSString *)folderPath withFileName:(NSArray *)fileNames withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure;

/**
 *  delete the specified file/folder (s)
 *
 *  @param filePath    file path
 *  @param fileName    target file/folder name
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)deleteFilePath:(NSString *)filePath withFileNames:(NSArray *)fileNames withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  get the target(file/folder)'s status
 *
 *  @param filePath    the path which target file/folder(s) in
 *  @param fileNames    file/folder names
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)getFileStatus:(NSString *)filePath withFileNames:(NSArray *)fileNames withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  set file/folder(s) modified time
 *
 *  @param filePath   file path
 *  @param fileNames  file/folder names
 *  @param timeStamp  time stamp, based on sec.
 *  @param successExt success block
 *  @param failure    failure block
 */
- (void)setFileModifiedTime:(NSString *)filePath withFileNames:(NSArray *)fileNames withTimeStamp:(NSTimeInterval)timeStamp withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure;


/**
 *  search files of conditions(parameters)
 *
 *  @param keyword    a keyword of the files you want to search
 *  @param sourcePath the root directory
 *  @param sortType   sortType, an enum of QNFileSortType
 *  @param limit      a number of limitation
 *  @param startIndex start index
 *  @param isASC      if yes, sorting in ASC,vice versa.
 */
- (void)searchFiles:(NSString *)keyword withSourcePath:(NSString *)sourcePath withSortField:(QNFileSortType)sortType withLimitNumber:(NSUInteger)limit withStartIndex:(NSUInteger)startIndex isASC:(BOOL)isASC withSuccessBlock:(QNQNSearchResponseSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)cancelLoginRequest;

@end
