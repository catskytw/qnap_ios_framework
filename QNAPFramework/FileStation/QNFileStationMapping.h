//
//  QNFileStationMapping.h
//  QNAPFramework
//
//  Created by Change.Liao on 13/9/9.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QNMappingProtoType.h"
#import <RestKit/RestKit.h>

@interface QNFileStationMapping : QNMappingProtoType

+ (RKEntityMapping *)mappingForNormalSuccessResponse;

/**
 *  mapping for QTS login
 *
 *  @return RKEntityMapping
 */
+ (RKEntityMapping *)mappingForLogin;

/**
 *  mapping for QTS login error if happened.
 *
 *  @return RKEntityMapping
 */
+ (RKEntityMapping *)mappingForLoginError;

/**
 *  mapping for search file
 *
 *  @return RKEntityMapping
 */
+ (RKEntityMapping *)mappingForSearchFiles;

/**
 *  This is a special case for QTS login mapping.
 *  It's so...complicated that we have to give a manual mapping
 *
 *  @return NSDictionary,
 */
+ (NSDictionary *)allMappingInAuthLogin;

/**
 *  mapping for DomainIPList response, JSON format.
 *  Provide the mapping for fetching domainIPList API
 *  e.g. { "status": 1, "mycloudnas_hn": "", "ddns_hn": "", "external_ip": "114.34.59.214", "local_ip": "10.8.12.104", "host_ip": "10.8.12.104", "local_ip_list": "10.8.12.104," }
 *
 *  @return RKEntityMapping
 */
+ (RKEntityMapping *)mappingForDomainIPList;

/**
 *  give a dynamic mapping for login
 *
 *  @param correctResponseMapping the correct mapping if the response is in correct format
 *  @param errorMapping           use this error mapping if any error code or http status error.
 *
 *  @return RKDynamicMapping
 */
+ (RKDynamicMapping *)dynamicMappingLoginWithCorrectMapping:(RKEntityMapping *)correctResponseMapping withErrorResponseMapping:(RKEntityMapping *)errorMapping;
@end
