//
//  QNFileStationAPIManager.m
//  QNAPFramework
//
//  Created by Chen-chih Liao on 13/9/3.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNFileStationAPIManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AFNetworking/AFHTTPClient.h>
#import "QNFileStationMapping.h"
#import "QNAPCommunicationManager.h"
#import "RKXMLReaderSerialization.h"
#import "QNAPFramework.h"
#import "RKObjectManager_DownloadProgress.h"
#import "RKObjectManager+TrackingRequestOperation.h"
#import "QNSearchResponse.h"
#import "QNAPFrameworkUtil.h"
#import "QNDomainIPListResponse.h"
#import "QNNormalSuccessResponse.h"

@class RKNSJSONSerialization;



@implementation QNFileStationAPIManager
@synthesize authSid = _authSid;

- (id)initWithBaseURL:(NSString *)baseURL{
    if((self = [super initWithBaseURL:baseURL])){
        self.baseURL = baseURL;
        [self setting];
    }
    return self;
}

- (void)setting{
    self.rkObjectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:self.baseURL]];
    self.rkObjectManager.managedObjectStore = [QNAPCommunicationManager share].objectManager;
    [self.rkObjectManager.HTTPClient setAllowsInvalidSSLCertificate:YES];
    [self.rkObjectManager setAcceptHeaderWithMIMEType:@"text/xml"];
    [self.rkObjectManager setAcceptHeaderWithMIMEType:@"text/html"]; //video/quicktime
    [self.rkObjectManager setAcceptHeaderWithMIMEType:@"video/quicktime"];
    [RKMIMETypeSerialization registerClass:[RKXMLReaderSerialization class] forMIMEType:@"text/xml"];
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/html"];
}
#pragma mark - FileStation API
- (void)loginWithAccount:(NSString*)account withPassword:(NSString*)password withSuccessBlock:(void (^)(RKObjectRequestOperation *operation, RKMappingResult * mappingResult, QNFileLogin *loginInfo))success withFailureBlock:(void (^)(RKObjectRequestOperation *operation, QNFileLoginError *loginError))failure{
    
    if(!self.rkObjectManager){
        DDLogError(@"FileStationManager is nil!");
        return;
    }
    [QNFileLogin MR_truncateAll];
    //mapping the first layer
    RKEntityMapping *responseMapping = [QNFileStationMapping mappingForLogin];
    RKEntityMapping *errorMapping = [QNFileStationMapping mappingForLoginError];
    RKDynamicMapping *dynamicMapping = [QNFileStationMapping dynamicMappingLoginWithCorrectMapping:responseMapping withErrorResponseMapping:errorMapping];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:dynamicMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.rkObjectManager addResponseDescriptor:responseDescriptor];
    NSString *encodingPassword = [QNAPFrameworkUtil ezEncode:password];
    
    NSDictionary *parameters = @{@"pwd":encodingPassword, @"user":account, @"service":@"1"};
    RKObjectRequestOperation * operation = [self.rkObjectManager getObjectsAtPathWithTracking:@"cgi-bin/authLogin.cgi"
                                                                                   parameters:parameters
                                                                                      success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                                                                          [[QNAPCommunicationManager share].operationTrackingPool removeObject:operation];
                                                                                          
                                                                                          //fetching the result from coredata
                                                                                          id resultObject = [mappingResult firstObject];
                                                                                          [QNMappingProtoType dynamicMappingResult:resultObject];
                                                                                          if([NSStringFromClass([resultObject class]) isEqualToString:@"QNFileLogin"]){
                                                                                              NSArray *allQNLogin = [QNFileLogin MR_findAllInContext:self.rkObjectManager.managedObjectStore.mainQueueManagedObjectContext];
                                                                                              QNFileLogin *targetLogin = allQNLogin[0];
                                                                                              _authSid = [NSString stringWithString:targetLogin.authSid];
                                                                                              [QNAPCommunicationManager share].sidForQTS = _authSid;
                                                                                              DDLogInfo(@"fetching login information successfully...Sid: %@", targetLogin.authSid);
                                                                                              
                                                                                              if(success){
                                                                                                  self.account = account;
                                                                                                  success(operation, mappingResult, targetLogin);
                                                                                              }
                                                                                          }else{ //如果不是QNFileLogin, 那必定是QNFileLoginError
                                                                                              if(failure)
                                                                                                  failure(operation, resultObject);
                                                                                          }
                                                                                      }
                                                                                      failure:^(RKObjectRequestOperation *operation, NSError *error){
                                                                                          [[QNAPCommunicationManager share].operationTrackingPool removeObject:operation];
                                                                                          if ([operation isCancelled]) {
                                                                                              return ;
                                                                                          }
                                                                                          QNFileLoginError *loginError = [QNFileLoginError MR_createEntity];
                                                                                          [loginError setErrorValue:[error localizedDescription]];
                                                                                          [loginError setErrorCode:@(error.code)];

                                                                                          if(failure)
                                                                                              failure(operation, loginError);
                                                                                          DDLogError(@"HTTP Request Error! %@", error);
                                                                                      }];
    [[QNAPCommunicationManager share].operationTrackingPool addObject:operation];
}

- (void)thumbnailWithFile:(NSString *)fileName withPath:(NSString *)filePath withSuccessBlock:(void(^)(UIImage *image))success withFailureBlock:(void(^)(NSError *error))failure withInProgressBlock:(void(^)(NSUInteger receivedSize, long long expectedSize))inProgress{
    
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:self.baseURL]];
    NSMutableURLRequest *request =[client requestWithMethod:@"GET"
                                                       path:@"cgi-bin/filemanager/utilRequest.cgi"
                                                 parameters:
                                   @{
                                     @"func":@"get_thumb",
                                     @"sid":_authSid,
                                     @"name":fileName,
                                     @"path":filePath,
                                     @"size":@"320"}
                                   ];
    [[SDWebImageManager sharedManager] downloadWithURL:[request URL]
                                               options:0
                                              progress:^(NSInteger receivedSize, NSInteger expectedSize){
                                                  inProgress(receivedSize, expectedSize);
                                              }
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished){
                                                 if(finished)
                                                     (image)?success(image):failure(error);
                                             }];
    
}

- (void)getDomainIPListWithSuccessBlock:(QNQNDomainIPListResponseSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping * responseMapping = [QNFileStationMapping mappingForDomainIPList];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.rkObjectManager addResponseDescriptor:responseDescriptor];
    NSDictionary *dic = @{@"func":@"get_domain_ip_list", @"sid":_authSid};
    [self.rkObjectManager getObject:nil
                               path:@"cgi-bin/filemanager/utilRequest.cgi"
                         parameters:dic
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                QNDomainIPListResponse *response = (QNDomainIPListResponse *)[mappingResult firstObject];
                                DDLogInfo(@"domain IP list %@", response);
                                success(operation, mappingResult, response);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error){
                                failure(operation, error);
                            }];
    
}

- (void)clearThumbnailCache{
    [[[SDWebImageManager sharedManager] imageCache] clearMemory];
    [[[SDWebImageManager sharedManager] imageCache] clearDisk];
}

#pragma mark - PrivateMethod
- (NSString *)convertQNFileSortType:(QNFileSortType)sortType{
    NSString *r = nil;
    switch (sortType) {
        case QNFileGroupSort:
            r = @"group";
            break;
        case QNFileModefiedTimeSort:
            r = @"mt";
            break;
        case QNFileOwnerSort:
            r = @"owner";
            break;
        case QNFilePrivilegeSort:
            r = @"privilege";
            break;
        case QNFileSizeSort:
            r = @"filesize";
            break;
        case QNFileTypeSort:
            r = @"filetype";
            break;
        default:
        case QNFileNameSort:
            r = @"filename";
            break;
    }
    return r;
}

- (NSError *)judgeStatus:(QNNormalSuccessResponse *)statusInResponse{
    NSString *r = nil;
    NSInteger status = [statusInResponse.status intValue];
    switch (status) {
        case 2:
            r = @"File Exist";
            break;
        case 4:
            r = @"Permission denied";
        default:
            r = @"unknown";
            break;
    }
    NSError *e = [NSError errorWithDomain:@"StatusInResponse" code:status userInfo:@{@"message":r}];
    return e;
}

- (NSString *)appendingFileName:(NSArray *)fileNames{
    if (fileNames == nil)
        return nil;
    NSString *r = @"";
    BOOL isFirst = YES;
    for (NSString *fileName in fileNames) {
        NSString *contentString = (isFirst)?[NSString stringWithFormat:@"%@", fileName]:[NSString stringWithFormat:@",%@", fileName];
        r = [r stringByAppendingString:contentString];
        isFirst = NO;
    }
    return r;
}

#pragma mark - Normal File Operating
- (void)createFolder:(NSString *)folderName withDestPath:(NSString *)folderDestPath withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping * responseMapping = [QNFileStationMapping mappingForNormalSuccessResponse];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.rkObjectManager addResponseDescriptor:responseDescriptor];
    NSDictionary *dic = @{@"func":@"createdir", @"sid":_authSid, @"dest_folder":folderName, @"dest_path":folderDestPath};
    [self.rkObjectManager getObject:nil
                               path:@"cgi-bin/filemanager/utilRequest.cgi"
                         parameters:dic
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                QNNormalSuccessResponse *response = (QNNormalSuccessResponse *)[mappingResult firstObject];
                                if ([response.success isEqualToString:@"true"] && [response.status intValue] == 1) {
                                    success(operation, mappingResult);
                                }else{
                                    failure(operation, [self judgeStatus:response]);
                                }
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error){
                                failure(operation, error);
                            }];
}

- (void)renameWithPath:(NSString *)path withSoureName:(NSString *)sourceName withDestName:(NSString *)destName withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping * responseMapping = [QNFileStationMapping mappingForNormalSuccessResponse];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.rkObjectManager addResponseDescriptor:responseDescriptor];
    NSDictionary *dic = @{@"func":@"rename", @"sid":_authSid, @"source_name":sourceName, @"dest_name":destName};
    [self.rkObjectManager getObject:nil
                               path:@"cgi-bin/filemanager/utilRequest.cgi"
                         parameters:dic
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                QNNormalSuccessResponse *response = (QNNormalSuccessResponse *)[mappingResult firstObject];
                                if ([response.success isEqualToString:@"true"] && [response.status intValue] == 1)
                                    success(operation, mappingResult);
                                else
                                    failure(operation, [self judgeStatus:response]);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error){
                                failure(operation, error);
                            }];
    
}

- (void)copyWithSourceFile:(NSString *)fileName withSourcePath:(NSString *)sourcePath withDestPath:(NSString *)destPath withMode:(QNFileExistingOption)mode withDupMode:(NSString *)dup withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping * responseMapping = [QNFileStationMapping mappingForNormalSuccessResponse];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.rkObjectManager addResponseDescriptor:responseDescriptor];
    NSDictionary *dic = @{@"func":@"copy",
                          @"sid":_authSid,
                          @"source_file":fileName,
                          @"source_path":sourcePath,
                          @"dest_path":destPath,
                          @"source_total":@"1",
                          @"mode":(mode == QNFileExistingOverwrite)? @"0":@"1"
                          };
    [self.rkObjectManager getObject:nil
                               path:@"cgi-bin/filemanager/utilRequest.cgi"
                         parameters:dic
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                QNNormalSuccessResponse *response = (QNNormalSuccessResponse *)[mappingResult firstObject];
                                if ([response.success isEqualToString:@"true"] && [response.status intValue] == 1)
                                    success(operation, mappingResult);
                                else
                                    failure(operation, [self judgeStatus:response]);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error){
                                failure(operation, error);
                            }];
    
}

- (void)moveWithSourceFile:(NSString *)sourceFile withSourcePath:(NSString *)sourcePath withDestPath:(NSString *)destPath withMode:(QNFileExistingOption)mode withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping * responseMapping = [QNFileStationMapping mappingForNormalSuccessResponse];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.rkObjectManager addResponseDescriptor:responseDescriptor];
    NSDictionary *dic = @{@"func":@"move",
                          @"sid":_authSid,
                          @"source_file":sourceFile,
                          @"source_path":sourcePath,
                          @"dest_path":destPath,
                          @"source_total":@"1",
                          @"mode":(mode == QNFileExistingOverwrite)? @"0":@"1"
                          };
    [self.rkObjectManager getObject:nil
                               path:@"cgi-bin/filemanager/utilRequest.cgi"
                         parameters:dic
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                QNNormalSuccessResponse *response = (QNNormalSuccessResponse *)[mappingResult firstObject];
                                if ([response.status intValue] == 1)
                                    success(operation, mappingResult);
                                else
                                    failure(operation, [self judgeStatus:response]);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error){
                                failure(operation, error);
                            }];
    
}

#pragma mark - Extract File Operating
- (void)extractFileListWithPath:(NSString *)path withStartIndex:(NSInteger)startIndex withLimitNumber:(NSInteger)limit withSortType:(NSInteger)sortType isASC:(BOOL)isASC withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure{
}

- (void)extractFile:(NSString *)extractFilePath withDestPath:(NSString *)destPath withPassword:(NSString *)password withMode:(NSInteger)extractMode isOverWrite:(BOOL)overWrite withPathMode:(NSInteger)pathMode withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure{
}

- (void)cancelExtraction:(NSString *)pid withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
}

#pragma mark - Compress file(s) operating
- (void)compressFile:(NSString *)compressedName withType:(NSInteger)compressType withPassword:(NSString *)password withCompressLevel:(QNCompressLevel)compressLevel withEncrypt:(NSInteger)encrypt withDestPath:(NSString *)compressedFilePath withTotalNumber:(NSInteger)totalNumber withCompressFileName:(NSString *)compressFileName withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
}

- (void)cancelCompress:(NSString *)pid withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
}

- (void)getCompressingStatus:(NSString *)pid withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
}

#pragma mark - Downloading/Uploading
- (void)downloadFileWithFilePath:(NSString *)filePath withFileName:(NSString *)fileName isFolder:(BOOL)isFolder withRange:(NSRange *)fileRange withSuccessBlock:(void (^)(RKObjectRequestOperation *operation, RKMappingResult * mappingResult))success withFailureBlock:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure withInProgressBlock:(void(^)(long long totalBytesRead, long long totalBytesExpectedToRead))inProgress{
    /**
     *http://changenas.myqnapcloud.com:8080/cgi-bin/filemanager/utilRequest.cgi?func=download&sid=6nmadgva&isfolder=0&source_path=/Public&source_file=1.mov&source_total=1
     *http://IP:8080/cgi-bin/filemanager/utilRequest.cgi?func=download&sid=xxxx&isfolder=0&source_path=/Public&source_file=test.txt&source_file=test2.txt&source_total=2
     *two source_file? WTF.
     *
     *maybe Using AOP concept here is better. TODO item.
     */
    if(!filePath || !fileName || !_authSid)
        return;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:
                                       @{
                                         @"func":@"download",
                                         @"sid":_authSid,
                                         @"isfolder":@(isFolder),
                                         @"source_path":filePath,
                                         @"source_file":fileName,
                                         @"source_total":@"1"
                                         }];
    [self.rkObjectManager getObject:nil
                               path:@"cgi-bin/filemanager/utilRequest.cgi"
                         parameters:parameters
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                DDLogVerbose(@"downloadFile %@ successful!", fileName);
                                success(operation, mappingResult);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error){
                                DDLogError(@"downloadFile %@ failure!", fileName);
                                failure(operation, error);
                            }
                         inProgress:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead){
                             inProgress(totalBytesRead, totalBytesExpectedToRead);
                         }];
}


#pragma mark - File/Folder Informations

- (void)getFolderList:(NSString *)targetFolderPath isISO:(BOOL)isISO withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
}

- (void)getFileList:(NSString *)folderPath withLimit:(NSInteger)limit withSortField:(QNFileSortType)sortType withStartIndex:(NSInteger)startIndex isISOShare:(BOOL)isISO isASC:(BOOL)isASC hasHiddenFile:(BOOL)hasHidden withMultimediaOption:(NSDictionary *)multimediaOption withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure{
}

- (void)getTotalFileSize:(NSString *)folderPath withFileName:(NSArray *)fileNames withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure{
}

- (void)deleteFilePath:(NSString *)filePath withFileNames:(NSArray *)fileNames withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping * responseMapping = [QNFileStationMapping mappingForNormalSuccessResponse];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.rkObjectManager addResponseDescriptor:responseDescriptor];
    NSDictionary *dic = @{@"func":@"delete", @"sid":_authSid, @"path":filePath, @"file_name":@"1.jpg", @"file_name":@"music.png"};
    [self.rkObjectManager getObject:nil
                               path:@"cgi-bin/filemanager/utilRequest.cgi"
                         parameters:dic
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                QNNormalSuccessResponse *response = (QNNormalSuccessResponse *)[mappingResult firstObject];
                                if ([response.success isEqualToString:@"true"] && [response.status intValue] == 1) {
                                    success(operation, mappingResult);
                                }else{
                                    failure(operation, [self judgeStatus:response]);
                                }
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error){
                                failure(operation, error);
                            }];
    
}

- (void)getFileStatus:(NSString *)filePath withFileNames:(NSArray *)fileNames withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
}

- (void)setFileModifiedTime:(NSString *)filePath withFileNames:(NSArray *)fileNames withTimeStamp:(NSTimeInterval)timeStamp withSuccessBlock:(QNSuccessBlock)successExt withFailureBlock:(QNFailureBlock)failure{
}

- (void)searchFiles:(NSString *)keyword withSourcePath:(NSString *)sourcePath withSortField:(QNFileSortType)sortType withLimitNumber:(NSUInteger)limit withStartIndex:(NSUInteger)startIndex isASC:(BOOL)isASC withSuccessBlock:(QNQNSearchResponseSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    
    RKEntityMapping * responseMapping = [QNFileStationMapping mappingForSearchFiles];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.rkObjectManager addResponseDescriptor:responseDescriptor];
    
    NSDictionary *dic = @{@"func":@"search",
                          @"sid":_authSid,
                          @"keyword":keyword,
                          @"source_path":sourcePath,
                          @"dir":(isASC?@"ASC":@"DESC"),
                          @"start":@(startIndex),
                          @"limit":@(limit),
                          @"sort":[self convertQNFileSortType:sortType]
                          };
    
    [self.rkObjectManager getObject:nil
                               path:@"cgi-bin/filemanager/utilRequest.cgi"
                         parameters:dic
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                QNSearchResponse *response = (QNSearchResponse *)[mappingResult firstObject];
                                DDLogInfo(@"search files under %@: %i files", sourcePath, [response.relationship_QNSearchFileInfo count]);
                                success(operation, mappingResult, response);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error){
                                DDLogError(@"search file under %@ error: %@", sourcePath, error);
                                failure(operation, error);
                            }];
}

- (void)cancelLoginRequest{
    /*
     1. clean all operations which are not executed in operationQueue.
     */
    [self.weakRKObjectManager cancelAllObjectRequestOperationsWithMethod:RKRequestMethodGET matchingPathPattern:@"cgi-bin/authLogin.cgi"];
    /**
     2. Set canceld in each operation which is in tracking pool
     The success block would drop the result if this operation is canceled while running.
     */
    for (RKObjectRequestOperation *eachOperation in [QNAPCommunicationManager share].operationTrackingPool) {
        [eachOperation cancel];
    }
    
    /**
     3. TODO: reset the operationQueue?
     */
}

@end
