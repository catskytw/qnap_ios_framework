//
//  QNDomainIPListResponse.h
//  QNAPFramework
//
//  Created by Change.Liao on 2013/10/30.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNDomainIPListResponse : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * mycloudnas_hn;
@property (nonatomic, retain) NSString * ddns_hn;
@property (nonatomic, retain) NSString * external_ip;
@property (nonatomic, retain) NSString * local_ip;
@property (nonatomic, retain) NSString * host_ip;
@property (nonatomic, retain) NSString * local_ip_list;

@end
