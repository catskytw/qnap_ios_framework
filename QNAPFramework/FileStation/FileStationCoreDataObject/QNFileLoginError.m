//
//  QNFileLoginError.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/6.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNFileLoginError.h"


@implementation QNFileLoginError

@dynamic authPassed;
@dynamic doQuick;
@dynamic errorValue;
@dynamic is_booting;
@dynamic username;
@dynamic errorCode;

@end
