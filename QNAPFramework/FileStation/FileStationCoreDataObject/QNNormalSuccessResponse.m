//
//  QNNormalSuccessResponse.m
//  QNAPFramework
//
//  Created by Change.Liao on 2013/11/18.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNNormalSuccessResponse.h"


@implementation QNNormalSuccessResponse

@dynamic status;
@dynamic success;

@end
