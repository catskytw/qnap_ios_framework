//
//  QNFileLoginError.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/6.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNFileLoginError : NSManagedObject

@property (nonatomic, retain) NSNumber * authPassed;
@property (nonatomic, retain) NSString * doQuick;
@property (nonatomic, retain) NSString * errorValue;
@property (nonatomic, retain) NSNumber * is_booting;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSNumber * errorCode;

@end
