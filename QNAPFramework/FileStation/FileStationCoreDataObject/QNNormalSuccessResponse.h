//
//  QNNormalSuccessResponse.h
//  QNAPFramework
//
//  Created by Change.Liao on 2013/11/18.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNNormalSuccessResponse : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * success;

@end
