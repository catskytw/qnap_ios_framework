//
//  QNDomainIPListResponse.m
//  QNAPFramework
//
//  Created by Change.Liao on 2013/10/30.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNDomainIPListResponse.h"


@implementation QNDomainIPListResponse

@dynamic status;
@dynamic mycloudnas_hn;
@dynamic ddns_hn;
@dynamic external_ip;
@dynamic local_ip;
@dynamic host_ip;
@dynamic local_ip_list;

@end
