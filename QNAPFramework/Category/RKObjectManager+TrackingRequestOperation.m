//
//  RKObjectManager+TrackingRequestOperation.m
//  QVideo
//
//  Created by Change.Liao on 2014/1/16.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "RKObjectManager+TrackingRequestOperation.h"

@implementation RKObjectManager (TrackingRequestOperation)

- (RKObjectRequestOperation *)getObjectsAtPathWithTracking:(NSString *)path
                                                parameters:(NSDictionary *)parameters
                                                   success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                                                   failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure
{
    NSParameterAssert(path);
    RKObjectRequestOperation *operation = [self appropriateObjectRequestOperationWithObject:nil method:RKRequestMethodGET path:path parameters:parameters];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [self enqueueObjectRequestOperation:operation];
    return operation;
}

@end
