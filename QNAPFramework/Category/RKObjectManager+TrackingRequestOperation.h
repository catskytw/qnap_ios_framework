//
//  RKObjectManager+TrackingRequestOperation.h
//  QVideo
//
//  Created by Change.Liao on 2014/1/16.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "RKObjectManager.h"

@interface RKObjectManager (TrackingRequestOperation)
- (RKObjectRequestOperation *)getObjectsAtPathWithTracking:(NSString *)path
                                                parameters:(NSDictionary *)parameters
                                                   success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                                                   failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

@end
