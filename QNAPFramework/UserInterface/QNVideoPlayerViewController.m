//
//  QNVideoPlayerViewController.m
//  QNAPFramework
//
//  Created by Change.Liao on 13/10/9.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoPlayerViewController.h"

@interface QNVideoPlayerViewController ()

@end

@implementation QNVideoPlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closePlayback:(id)sender {
    [super closePlayback:sender];
    [self dismissModalViewControllerAnimated:YES];
}

@end
