//
//  QNAPCommunicationManager.m
//  QNAPFramework
//
//  Created by Change.Liao on 13/9/2.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNAPCommunicationManager.h"
#import "NSPersistentStoreCoordinator+MagicalRecord.h"
#import "NSManagedObjectContext+Extend.h"
#import <MagicalRecord/MagicalRecord+Setup.h>
#import <RestKit/CoreData.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <CocoaLumberjack/DDLog.h>
#import <CocoaLumberjack/DDTTYLogger.h>
#import "AOPProxy.h"
#import <objc/runtime.h>
#import "QNAPFrameworkUtil.h"
#import "QNAPFramework.h"
#import "UDPSearch_Constants.h"

static QNAPCommunicationManager *singletonCommunicationManager = nil;
static int udpPort = 8097;

@implementation QNAPCommunicationManager

+ (QNAPCommunicationManager *)share{
    if(singletonCommunicationManager == nil){
        /**
         1. create singletonCommunicationManager
         2. binding magical record's context with AFNetworking's context(which both mean the context of CoreData)
         3. other essential settings.
         */
        singletonCommunicationManager = [QNAPCommunicationManager new];
        singletonCommunicationManager.allModules = [NSMutableArray array];
        [singletonCommunicationManager activateDebugLogLevel:LOG_LEVEL_VERBOSE];
        singletonCommunicationManager.downloadQueue = [[NSOperationQueue alloc] init];
    }
    return singletonCommunicationManager;
}

- (BOOL)activateAllStation:(NSDictionary *)parameters{
    /**
     @{
     @"NASURL":NASURL,
     @"MyCloudURL":MyCloudServerBaseURL,
     @"ClientId":CLIENT_ID,
     @"ClientSecret":CLIENT_SECRET,
     }
     */
    self.fileStationsManager = [self factoryForFileStatioAPIManager:[parameters valueForKey:@"NASURL"]];
    self.musicStationManager = [self factoryForMusicStatioAPIManager:[parameters valueForKey:@"NASURL"]];
    self.myCloudManager = [self factoryForMyCloudManager:[parameters valueForKey:@"MyCloudURL"]
                                            withClientId:[parameters valueForKey:@"ClientId"]
                                        withClientSecret:[parameters valueForKey:@"ClientSecret"]];
    self.videoStationManager = [self factoryForVideoStationAPIManager:[parameters valueForKey:@"NASURL"]];
    self.appCenterManager = [self factoryForAppCenterAPIManager:[parameters valueForKey:@"NASURL"]];
    return (self.musicStationManager && self.fileStationsManager && self.myCloudManager && self.videoStationManager && self.appCenterManager)?YES:NO;
}

- (BOOL)loginAction:(int)loginOption withLoginInfo:(NSDictionary *)dic{
    if([dic valueForKey:@"NASAccount"] && [dic valueForKey:@"NASPassword"]){
        [self.musicStationManager loginForMultimediaSid:[dic valueForKey:@"NASAccount"]
                                           withPassword:[dic valueForKey:@"NASPassword"]
                                       withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
                                       }
                                       withFailureBlock:^(RKObjectRequestOperation *operation, NSError *error){
                                       }];
    }
    
    [self.fileStationsManager loginWithAccount:@""
                                  withPassword:@""
                              withSuccessBlock:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult, QNFileLogin *login){
                              }
                              withFailureBlock:^(RKObjectRequestOperation *operation, QNFileLoginError *error){
                              }];
    
    [self.myCloudManager fetchOAuthToken:@""
                            withPassword:@""
                        withSuccessBlock:^(AFOAuthCredential *credential){
                            
                        } withFailureBlock:^(NSError *error){
                            
                        }];
    /**
     1. should be synchronized
     2. caculate the result
     */
    return YES;
    
}
#pragma mark - Binding Interceptors
- (BOOL)bindAllInterceptorForFileManager:(id)classInstance{
    /** TODO:
     *  Maybe we should use `class_copyMethodList(object_getClass(t), &mc)` to replace this foolish array.
     *  The only one problem is that all properties' getter and setter are included in class_copyMethodList, which might cause a deadlock if checking sidForxxx.
     *  Sigh!
     */
    
    NSArray *bindingMethods = @[@"downloadFileWithFilePath:withFileName:isFolder:withRange:withSuccessBlock:withFailureBlock:withInProgressBlock:",
                                @"thumbnailWithFile:withPath:withSuccessBlock:withFailureBlock:withInProgressBlock:"
                                ];
    for(NSString *methodName in bindingMethods){
        [(AOPProxy *)classInstance interceptMethodStartForSelector:NSSelectorFromString(methodName)
                                             withInterceptorTarget:self
                                               interceptorSelector:@selector(beforeInterceptorForFileManager:)];
        [(AOPProxy *)classInstance interceptMethodEndForSelector:NSSelectorFromString(methodName)
                                           withInterceptorTarget:self
                                             interceptorSelector:@selector(afterInterceptorForFileManager:)];
    }
    return YES;
}

- (BOOL)bindAllInterceptorForMyCloud:(id)classInstance{
    /** TODO:
     *  Maybe we should use `class_copyMethodList(object_getClass(t), &mc)` to replace this foolish array.
     *  The only one problem is that all properties' getter and setter are included in class_copyMethodList, which might cause a deadlock if checking sidForxxx.
     *  Sigh!
     */
    NSArray *bindingMethods = @[@"readMyInformation:withFailiureBlock:",
                                @"updateMyInformation:withSuccessBlock:withFailureBlock:",
                                @"listMyActivities:withLimit:isDesc:withSuccessBlock:withFailureBlock:",
                                @"changeMyPassword:withNewPassword:withSuccessBlock:withFailureBlock:"
                                ];
    for(NSString *methodName in bindingMethods){
        [(AOPProxy *)classInstance interceptMethodStartForSelector:NSSelectorFromString(methodName)
                                             withInterceptorTarget:self
                                               interceptorSelector:@selector(beforeInterceptorForMyCloud:)];
        [(AOPProxy *)classInstance interceptMethodEndForSelector:NSSelectorFromString(methodName)
                                           withInterceptorTarget:self
                                             interceptorSelector:@selector(afterInterceptorForMyCloud:)];
    }
    return YES;
}

- (BOOL)bindAllInterceptorForMusicStation:(id)classInstance{
    //    [self listAllMethod:[QNMusicStationAPIManager new]];
    NSArray *bindingMethods = @[@"getFolderListWithFolderID:withSuccessBlock:withFaliureBlock:",
                                @"getSongListWithArtistId:withSuccessBlock:withFailureBlock:",
                                @"getAlbumListWithAlbumId:pageSize:currPage:withSuccessBlock:withFailureBlock:",
                                @"getGenreListWithGenreId:pageSize:currPage:withSuccessBlock:withFailureBlock:",@"getRecentListWithPageSize:currPage:withSuccessBlock:withFailureBlock:"];
    for(NSString *methodName in bindingMethods){
        [(AOPProxy *)classInstance interceptMethodStartForSelector:NSSelectorFromString(methodName)
                                             withInterceptorTarget:self
                                               interceptorSelector:@selector(beforeInterceptorForMusicStaion:)];
        [(AOPProxy *)classInstance interceptMethodEndForSelector:NSSelectorFromString(methodName)
                                           withInterceptorTarget:self
                                             interceptorSelector:@selector(afterInterceptorForMusicStaion:)];
    }
    return YES;
}

#pragma mark - Interceptors
- (void)beforeInterceptorForFileManager:(NSInvocation *)i{
    QNFileStationAPIManager *thisFileStation = (QNFileStationAPIManager *)[i target];
    //檢查fileManager的baseURL
    if(!thisFileStation.baseURL){
        DDLogError(@"The baseURL for fileStation is %@", thisFileStation.baseURL);
    }
    //檢查sidForQTS
    if(![QNAPCommunicationManager share].sidForQTS){
        DDLogError(@"sidForQTS is null!!");
        //TODO maybe login again?
    }
}

- (void)afterInterceptorForFileManager:(NSInvocation *)i{
    DDLogVerbose(@"Method %@ is finished!", NSStringFromSelector([i selector]));}

- (void)beforeInterceptorForMyCloud:(NSInvocation *)i{
    QNMyCloudManager *myCloudManager = (QNMyCloudManager *)[i target];
    //檢查myCloudManager的baseURL
    if(!myCloudManager.baseURL){
        DDLogError(@"myCloudManager URL is null");
    }
    
    //檢查credetial是否有token以及是否過期
    //TODO: dispatch
    AFOAuthCredential *credential = [AFOAuthCredential retrieveCredentialWithIdentifier: CredentialIdentifier];
    if(!credential.accessToken || [credential isExpired]){
        __block int isFetchingSuccess = NO;
        QNMyCloudManager *previousMyCloudManager = (QNMyCloudManager *)[self searchModuleWithClassName:@"QNMyCloudManager"];
        [previousMyCloudManager refetchOAuthTokenWithSuccessBlock:^(AFOAuthCredential *credential){isFetchingSuccess = YES;}
                                                 withFailureBlock:^(NSError *error){
                                                     isFetchingSuccess = YES;
                                                 }];
        //We should wait here as being synchronized.
        [QNAPFrameworkUtil waitUntilConditionYES:&isFetchingSuccess];
    }
}

- (void)afterInterceptorForMyCloud:(NSInvocation *)i{
    DDLogVerbose(@"Method %@ is finished!", NSStringFromSelector([i selector]));
}

- (void)beforeInterceptorForMusicStaion:(NSInvocation *)i{
    QNMusicStationAPIManager *thisMusicStation = (QNMusicStationAPIManager *)[i target];
    //檢查fileManager的baseURL
    if(!thisMusicStation.baseURL){
        DDLogError(@"The baseURL for fileStation is %@", thisMusicStation.baseURL);
    }
    //檢查sidForQTS
    if(![QNAPCommunicationManager share].sidForMultimedia){
        DDLogError(@"sidForMultimedia is null!!");
    }
}

- (void)afterInterceptorForMusicStaion:(NSInvocation *)i{
    DDLogVerbose(@"Method %@ is finished!", NSStringFromSelector([i selector]));
}
#pragma mark - Factory Methods
- (QNFileStationAPIManager*)factoryForFileStatioAPIManager:(NSString*)baseURL{
    if([self validateUrl:baseURL])
        return nil;
    
    //設定QNFileStationAPIManager
    QNFileStationAPIManager *fileStationsAPIManager = (QNFileStationAPIManager *)[[AOPProxy alloc] initWithNewInstanceOfClass:[QNFileStationAPIManager class]];
    [fileStationsAPIManager setBaseURL:baseURL];
    [fileStationsAPIManager setting];
    
    //攔截器
    [self bindAllInterceptorForFileManager:fileStationsAPIManager];
    
    QNModuleBaseObject *searchExistingModule = [self sameModuleWithTargetModule:fileStationsAPIManager];
    if(searchExistingModule==nil){
        self.rkObjectManager = fileStationsAPIManager.rkObjectManager;
        return fileStationsAPIManager;
    }
    else{
        return (QNFileStationAPIManager *)searchExistingModule;
    }
}

- (QNMyCloudManager *)factoryForMyCloudManager:(NSString *)baseURL withClientId:(NSString *)clientId withClientSecret:(NSString *)clientSecret{
    QNMyCloudManager *myCloudManager =(QNMyCloudManager *)[[AOPProxy alloc] initWithNewInstanceOfClass:[QNMyCloudManager class]];
    myCloudManager.baseURL = baseURL;
    myCloudManager.clientSecret = clientSecret;
    myCloudManager.clientId = clientId;
    
    
    //攔截器
    [self bindAllInterceptorForMyCloud:myCloudManager];
    
    QNModuleBaseObject *searchExistingModule = [self sameModuleWithTargetModule:myCloudManager];
    return (searchExistingModule == nil)?myCloudManager:(QNMyCloudManager *)searchExistingModule;
}

- (QNMusicStationAPIManager *)factoryForMusicStatioAPIManager:(NSString*)baseURL{
    QNMusicStationAPIManager *musicStationsAPIManager = (QNMusicStationAPIManager *)[[AOPProxy alloc] initWithNewInstanceOfClass:[QNMusicStationAPIManager class]];
    musicStationsAPIManager.baseURL = baseURL;
    [musicStationsAPIManager setting];
    [self bindAllInterceptorForMusicStation:musicStationsAPIManager];
    QNModuleBaseObject *searchExistingModule = [self sameModuleWithTargetModule:musicStationsAPIManager];
    return (searchExistingModule==nil)?musicStationsAPIManager:(QNMusicStationAPIManager *)searchExistingModule;
}

- (QNVideoStationAPIManager *)factoryForVideoStationAPIManager:(NSString *)baseURL{
    QNVideoStationAPIManager *videoStationAPIManager = (QNVideoStationAPIManager *)[[AOPProxy alloc] initWithNewInstanceOfClass:[QNVideoStationAPIManager class]];
    videoStationAPIManager.baseURL = baseURL;
    [videoStationAPIManager setting];
    //TODO binding interceptor
    QNModuleBaseObject *searchExistingModule = [self sameModuleWithTargetModule:videoStationAPIManager];
    return searchExistingModule == nil ? videoStationAPIManager:(QNVideoStationAPIManager *)searchExistingModule;
}

- (QNAppCenterAPIManager *)factoryForAppCenterAPIManager:(NSString*)baseURL{
    QNAppCenterAPIManager *appCenterManager = (QNAppCenterAPIManager *)[[AOPProxy alloc] initWithNewInstanceOfClass:[QNAppCenterAPIManager class]];
    appCenterManager.baseURL = baseURL;
    [appCenterManager setting];
    
    QNModuleBaseObject *searchExistingModule = [self sameModuleWithTargetModule:appCenterManager];
    return searchExistingModule == nil ? appCenterManager:(QNAppCenterAPIManager *)searchExistingModule;
}
#pragma mark - PrivateMethod
- (void)listAllMethod:(id)instance{
    int i=0;
    unsigned int mc = 0;
    Method * mlist = class_copyMethodList(object_getClass(instance), &mc);
    NSLog(@"%d methods", mc);
    for(i=0;i<mc;i++)
        NSLog(@"Method no #%d: %s", i, sel_getName(method_getName(mlist[i])));
    
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

- (QNModuleBaseObject *)sameModuleWithTargetModule:(QNModuleBaseObject*)targetModule{
    @synchronized(self.allModules){
        for (QNModuleBaseObject *examModule in self.allModules) {
            if([examModule.baseURL isEqualToString:targetModule.baseURL] && [targetModule isMemberOfClass:[examModule class]]){
                return examModule;
            }
        }
        return nil;
    }
}

- (QNModuleBaseObject *)searchModuleWithClassName:(NSString *)className{
    @synchronized(self.allModules){
        for (QNModuleBaseObject *examModule in self.allModules) {
            if([examModule isKindOfClass: NSClassFromString(className)]){
                return examModule;
            }
        }
        return nil;
    }
}

#pragma mark - Misc Setting
- (void)activateDebugLogLevel:(int)_ddLogLevel{
    [DDLog removeAllLoggers];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor greenColor] backgroundColor:nil forFlag:LOG_FLAG_INFO];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor blueColor] backgroundColor:nil forFlag:LOG_FLAG_VERBOSE];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor yellowColor] backgroundColor:nil forFlag:LOG_FLAG_WARN];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor redColor] backgroundColor:nil forFlag:LOG_FLAG_ERROR];
    
    if(_ddLogLevel & LOG_FLAG_VERBOSE)
        RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
}

- (void)settingMisc:(NSBundle *)resourceBundle withDownloadSession:(NSURLSession *)myDownloadSession{
    /*1. generate the needed context
     * 2. binding MagicalRecord's context with RESTKit's one
     **/
    NSError *error = nil;
    [MagicalRecord setupAutoMigratingCoreDataStack];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithPersistentStoreCoordinator:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    self.objectManager = managedObjectStore;
    [self.objectManager createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"QVideoCoreData.sqlite"];
    
    NSPersistentStore *addPersistentStore = [self.objectManager addSQLitePersistentStoreAtPath:storePath
                                                                        fromSeedDatabaseAtPath:nil
                                                                             withConfiguration:nil
                                                                                       options:nil
                                                                                         error:&error];
    DDLogVerbose(@"database %@", addPersistentStore);
    [self.objectManager createManagedObjectContexts];
    [NSManagedObjectContext MR_setDefaultContext:self.objectManager.mainQueueManagedObjectContext];
    [NSManagedObjectContext MR_setRootSavingContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    if (!self.downloadSession) {
        self.downloadSession = myDownloadSession;
    }
    
    if (!self.operationTrackingPool) {
        self.operationTrackingPool = [[NSMutableSet alloc] init];
    }
}

+ (void)closeCommunicationManager{
    [MagicalRecord cleanUp];
}

#pragma mark - UDPSearch

- (void)startUDPSearch:(void(^)(NSDictionary *dataDic))findOneBlock{
    NSError *error = nil;
    //create udp
    if (!_udpSocket) {
        _udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
        [_udpSocket bindToPort:udpPort error:&error];
        [_udpSocket enableBroadcast:YES error:&error];
        [_udpSocket beginReceiving:&error];
    }
    
    //search command
    FINDER_CMD_HEADER pHeader;
    memset(&pHeader, 0, sizeof(pHeader));
    memcpy(&(pHeader.Preamble[6]), "Cl", 2);// Mean packet send client side
    pHeader.Cmd = 1;
    
    //send out
    NSData *data = [NSData dataWithBytes:&pHeader length:sizeof(pHeader)];
    [_udpSocket sendData:data toHost:@"255.255.255.255" port:udpPort withTimeout:-1 tag:1];
    
    DDLogVerbose(@"udp broadcast: %@", data);
    _findOneBlock = findOneBlock;
}


#pragma mark - UDPDelegate
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag{
    if (tag == 1) {
        DDLogVerbose(@"searching NAS ...");
    }
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock
   didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext{
    const uint8_t *buf = [data bytes];
    if(buf[6] =='S' && buf[7] =='e') // Find server response => key word "XXXXXXSe"
    {
        NSDictionary *rDic = [self udpDataFetchingWithTag:TAG_SERVER_NAME withInputData:data fromAddress:address isInt:NO];
        NSString *serverName = rDic[@"tagValue"];
        NSString *host = rDic[@"host"];
        
        NSString *modelName = [self udpDataFetchingWithTag:TAG_MODEL_NAME withInputData:data fromAddress:address isInt:NO][@"tagValue"];
        NSString *portString = [self udpDataFetchingWithTag:TAG_PORT_NUMBER withInputData:data fromAddress:address isInt:YES][@"tagValue"];
        NSString *sslString = [self udpDataFetchingWithTag:TAG_EXT_SECURE_ADMINPORT_NUMBER withInputData:data fromAddress:address isInt:YES][@"tagValue"];
        NSDictionary *dataDic = @{@"serverName":(serverName)?serverName:@"",
                                  @"modelName":(modelName)?modelName:@"",
                                  @"host":(host)?host:@"",
                                  @"port":(portString)?@([portString intValue]):@(0),
                                  @"sslPort":(sslString)?@([sslString intValue]):@(0)};
        _findOneBlock(dataDic);
        
    }
}

- (NSDictionary *)udpDataFetchingWithTag:(NSInteger)tag withInputData:(NSData *)data fromAddress:(NSData *)address isInt:(BOOL)isInt{
    uint8_t tagData[MAX_TAG_DATA_SIZE];
    const uint8_t *buf = [data bytes];
    NSInteger packet_len = [data length];
    NSString *_tmpString = nil;
    NSString *rString = nil;
    
    NSDictionary *rDic = nil;
    NSInteger len = [self getTagData:buf packetBufLen:packet_len tagId:tag tagBuf:&tagData tagBufSize:MAX_TAG_DATA_SIZE];
    if (len>0){
        uint16_t _tmp = 0;
        [GCDAsyncUdpSocket getHost:&_tmpString port:&_tmp fromAddress:address];
        NSMutableData *tmpData = [[NSMutableData alloc] init];
        [tmpData appendBytes:&tagData length:len];
        
        if (isInt) {
            int value = CFSwapInt32LittleToHost(*(int*)([tmpData bytes]));
            rString = [NSString stringWithFormat:@"%i", value];
        }else
            rString = [[NSString alloc] initWithData:tmpData encoding:NSUTF8StringEncoding];
        rDic =  @{@"tagValue": rString, @"host": _tmpString};
    }
    return  rDic;
}

-(NSInteger)getTagData:(const uint8_t *)packet_buf packetBufLen:(NSInteger)packet_buf_len tagId:(NSInteger)tag_id tagBuf:(void *)tag_buf tagBufSize:(NSInteger)tag_buf_size {
    DDLogVerbose(@"getTagData start");
    unsigned char *buf = (unsigned char *) packet_buf;
    
    for (int i = sizeof(FINDER_CMD_HEADER); i < packet_buf_len;)
    {
        DATA_TAG *tag = (DATA_TAG *) &(buf[i]);
        if (tag->tag == tag_id)
        {
            int l = tag->len;
            if (tag->len > tag_buf_size) l = tag_buf_size;
            memcpy(tag_buf, tag->data, l);
            return l;
        }else if(tag->tag == TAG_EXTENSION)
        {
            EXT_DATA_TAG *ext_tag = (EXT_DATA_TAG *) tag->data;
            if (ext_tag->tag == tag_id)
            {
                int m = ext_tag->len;
                if (ext_tag->len > tag_buf_size) m = tag_buf_size;
                memcpy(tag_buf, ext_tag->data, m);
                return m;
            }
        }
        
        i += (tag->len + 2);
    }
    DDLogVerbose(@"getTagData end");
    return 0;
}

@end
