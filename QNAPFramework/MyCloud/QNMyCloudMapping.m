//
//  QNMyCloudMapping.m
//  QNAPFramework
//
//  Created by Chen-chih Liao on 13/9/3.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNMyCloudMapping.h"
#import "QNAPCommunicationManager.h"
#import <RestKit/RestKit.h>
@implementation QNMyCloudMapping

+ (RKEntityMapping *)mappingForUser{
    return [self entityMapping:@"MyCloudUser" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:NO];
}

+ (RKEntityMapping *)mappingForResponse{
    return [self entityMapping:@"MyCloudResponse" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:NO];
}

+ (RKEntityMapping *)mappingForUserActivities{
    //設定activity本身自己的mapping
    RKEntityMapping *activitiesMapping = [self entityMapping:@"MyCloudUserActivity" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:NO];
    activitiesMapping.identificationAttributes = @[@"user_activity_id"];
    //設定app 的mapping與relationship
    //因為app 裡有使用id當key, 是ObjC關鍵字, entity另外取為appId
    RKEntityMapping *appMapping =  [RKEntityMapping mappingForEntityForName:@"MyCloudApp" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    [appMapping addAttributeMappingsFromDictionary:@{@"id":@"appId"}];
    
    RKRelationshipMapping *appRelationShip = [RKRelationshipMapping relationshipMappingFromKeyPath:@"app" toKeyPath:@"relationship_App" withMapping:appMapping];
    
    //設定sourceInfo 的mapping與relationship
    RKEntityMapping *sourceMapping = [self entityMapping:@"MyCloudSourceInfo" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:NO];
    RKRelationshipMapping *sourceInfoRelationShip = [RKRelationshipMapping relationshipMappingFromKeyPath:@"from" toKeyPath:@"relationship_SourceInfo" withMapping:sourceMapping];
    
    //將appRelationship以及sourceInfoRelationship都加入activity
    [activitiesMapping addPropertyMapping:appRelationShip];
    [activitiesMapping addPropertyMapping:sourceInfoRelationShip];
    
    return activitiesMapping;
}

+ (RKEntityMapping *)mappingForCloudlink{
    RKEntityMapping *cloudLink = [self entityMapping:@"MyCloudCloudLinkResponse"
                              withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                                         isXMLParser:NO];
    [cloudLink setIdentificationAttributes:@[@"cloud_link_id", @"created_at", @"updated_at"]];
    return cloudLink;
}

+ (RKEntityMapping *)mappingForSearchDevice{
    //WTF
    RKEntityMapping *deviceResponeMapping = [self entityMapping:@"MyCloudDeviceResponse"
                                         withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                                                    isXMLParser:NO];
    RKEntityMapping *deviceMapping = [self entityMapping:@"MyCloudDevice"
                                  withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                                             isXMLParser:NO];
    RKEntityMapping *deviceInfoMapping = [self entityMapping:@"MyCloudDeviceInfo"
                                      withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                                                 isXMLParser:NO];
    RKEntityMapping *deviceLocalServiceMapping = [self entityMapping:@"MyCloudDeviceLocalService"
                                              withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                                                         isXMLParser:NO];
    RKEntityMapping *allIPs = [self entityMapping:@"MyCloudIPs"
                           withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                                      isXMLParser:NO];
    RKRelationshipMapping *deviceRelationShip = [RKRelationshipMapping relationshipMappingFromKeyPath:@"result"
                                                                                            toKeyPath:@"relationship_result"
                                                                                          withMapping:deviceMapping];
    [deviceResponeMapping addPropertyMapping:deviceRelationShip];
    
    RKRelationshipMapping *deviceInfo = [RKRelationshipMapping relationshipMappingFromKeyPath:@"info"
                                                                                            toKeyPath:@"relationship_info"
                                                                                          withMapping:deviceInfoMapping];
    [deviceMapping addPropertyMapping:deviceInfo];
    RKRelationshipMapping *deviceLocalService = [RKRelationshipMapping relationshipMappingFromKeyPath:@"local_services"
                                                                                            toKeyPath:@"relationship_localService"
                                                                                          withMapping:deviceLocalServiceMapping];
    [deviceMapping addPropertyMapping:deviceLocalService];
    RKRelationshipMapping *lanIPMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"lan_ips"
                                                                                   toKeyPath:@"relationship_lan_ips"
                                                                                 withMapping:allIPs];
    RKRelationshipMapping *wanIPMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"wan_ips"
                                                                                      toKeyPath:@"relationship_wan_ips"
                                                                                    withMapping:allIPs];
    [deviceInfoMapping addPropertyMapping:lanIPMapping];
    [deviceInfoMapping addPropertyMapping:wanIPMapping];
    return deviceResponeMapping;
}

+ (RKEntityMapping *)mappingForCloudlinkResponse{
    RKEntityMapping *cloudLinkResponse = [self entityMapping:@"MCCloudLinkFromDeviceResponse"
                                      withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                                                 isXMLParser:NO];
    RKEntityMapping *cloudLink = [QNMyCloudMapping mappingForCloudlink];
    RKRelationshipMapping *relationMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"result"
                                                                                         toKeyPath:@"relationship_result"
                                                                                       withMapping:cloudLink];
    [cloudLinkResponse addPropertyMapping:relationMapping];
    return cloudLinkResponse;
}

+ (RKEntityMapping *)basicResponseMappingWithResult:(RKEntityMapping *)resultMapping{
    RKEntityMapping *responseMapping = [QNMappingProtoType entityMapping:@"MyCloudResponse"
                                                  withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                                                             isXMLParser:NO];
    [responseMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"result"
                                                                                    toKeyPath:@"relationship_result"
                                                                                  withMapping:responseMapping]];
    return responseMapping;
}

@end
