//
//  MyCloudDeviceLocalService.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "MyCloudDeviceLocalService.h"


@implementation MyCloudDeviceLocalService

@dynamic service_name;
@dynamic external_port;
@dynamic internal_port;
@dynamic protocol;
@dynamic uri;
@dynamic private;
@dynamic published;

@end
