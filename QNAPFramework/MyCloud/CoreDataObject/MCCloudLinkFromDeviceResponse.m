//
//  MCCloudLinkFromDeviceResponse.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/11.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "MCCloudLinkFromDeviceResponse.h"
#import "MyCloudCloudLinkResponse.h"


@implementation MCCloudLinkFromDeviceResponse

@dynamic relationship_result;

@end
