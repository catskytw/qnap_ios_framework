//
//  MyCloudDeviceResponse.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MyCloudResponse.h"

@class MyCloudDevice;

@interface MyCloudDeviceResponse : MyCloudResponse

@property (nonatomic, retain) NSSet *relationship_result;
@end

@interface MyCloudDeviceResponse (CoreDataGeneratedAccessors)

- (void)addRelationship_resultObject:(MyCloudDevice *)value;
- (void)removeRelationship_resultObject:(MyCloudDevice *)value;
- (void)addRelationship_result:(NSSet *)values;
- (void)removeRelationship_result:(NSSet *)values;

@end
