//
//  MyCloudIPs.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MyCloudIPs : NSManagedObject

@property (nonatomic, retain) NSString * ipv4;
@property (nonatomic, retain) NSString * ipv6;

@end
