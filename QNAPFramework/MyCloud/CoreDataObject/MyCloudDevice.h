//
//  MyCloudDevice.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MyCloudDeviceInfo, MyCloudDeviceLocalService;

@interface MyCloudDevice : NSManagedObject

@property (nonatomic, retain) NSString * device_id;
@property (nonatomic, retain) NSString * device_name;
@property (nonatomic, retain) MyCloudDeviceInfo *relationship_info;
@property (nonatomic, retain) NSSet *relationship_localService;
@end

@interface MyCloudDevice (CoreDataGeneratedAccessors)

- (void)addRelationship_localServiceObject:(MyCloudDeviceLocalService *)value;
- (void)removeRelationship_localServiceObject:(MyCloudDeviceLocalService *)value;
- (void)addRelationship_localService:(NSSet *)values;
- (void)removeRelationship_localService:(NSSet *)values;

@end
