//
//  MyCloudDeviceInfo.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "MyCloudDeviceInfo.h"
#import "MyCloudIPs.h"


@implementation MyCloudDeviceInfo

@dynamic lan_gateway;
@dynamic relationship_lan_ips;
@dynamic relationship_wan_ips;

@end
