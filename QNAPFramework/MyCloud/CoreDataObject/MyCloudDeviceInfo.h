//
//  MyCloudDeviceInfo.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MyCloudIPs;

@interface MyCloudDeviceInfo : NSManagedObject

@property (nonatomic, retain) NSString * lan_gateway;
@property (nonatomic, retain) NSSet *relationship_lan_ips;
@property (nonatomic, retain) NSSet *relationship_wan_ips;
@end

@interface MyCloudDeviceInfo (CoreDataGeneratedAccessors)

- (void)addRelationship_lan_ipsObject:(MyCloudIPs *)value;
- (void)removeRelationship_lan_ipsObject:(MyCloudIPs *)value;
- (void)addRelationship_lan_ips:(NSSet *)values;
- (void)removeRelationship_lan_ips:(NSSet *)values;

- (void)addRelationship_wan_ipsObject:(MyCloudIPs *)value;
- (void)removeRelationship_wan_ipsObject:(MyCloudIPs *)value;
- (void)addRelationship_wan_ips:(NSSet *)values;
- (void)removeRelationship_wan_ips:(NSSet *)values;

@end
