//
//  MyCloudDevice.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "MyCloudDevice.h"
#import "MyCloudDeviceInfo.h"
#import "MyCloudDeviceLocalService.h"


@implementation MyCloudDevice

@dynamic device_id;
@dynamic device_name;
@dynamic relationship_info;
@dynamic relationship_localService;

@end
