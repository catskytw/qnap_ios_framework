//
//  MyCloudDeviceLocalService.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/10.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MyCloudDeviceLocalService : NSManagedObject

@property (nonatomic, retain) NSString * service_name;
@property (nonatomic, retain) NSNumber * external_port;
@property (nonatomic, retain) NSNumber * internal_port;
@property (nonatomic, retain) NSString * protocol;
@property (nonatomic, retain) NSString * uri;
@property (nonatomic, retain) NSNumber * private;
@property (nonatomic, retain) NSNumber * published;

@end
