//
//  MCCloudLinkFromDeviceResponse.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/11.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MyCloudResponse.h"

@class MyCloudCloudLinkResponse;

@interface MCCloudLinkFromDeviceResponse : MyCloudResponse

@property (nonatomic, retain) MyCloudCloudLinkResponse *relationship_result;

@end
