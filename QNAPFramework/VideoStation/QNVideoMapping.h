//
//  QNVideoMapping.h
//  QNAPFramework
//
//  Created by Change.Liao on 13/9/27.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNMappingProtoType.h"
#import <RestKit/RestKit.h>
@interface QNVideoMapping : QNMappingProtoType
+ (RKEntityMapping *)mappingForStatusMessage;
+ (RKEntityMapping *)mappingforOnlyStatus;
+ (RKEntityMapping *)mappingForGetAllVideoList;
+ (RKEntityMapping *)mappingForTimeLineList;
+ (RKEntityMapping *)mappingforCollectionList;
+ (RKEntityMapping *)mappingForFolderItems;

+ (RKEntityMapping *)mappingForRenderList;
+ (RKEntityMapping *)mappingForPlayerList;
+ (RKEntityMapping *)mappingForContentList;
+ (RKEntityMapping *)mappingForPlayState;
+ (RKEntityMapping *)mappingForLogin;
+ (RKEntityMapping *)mappingForCreateCollection;

@end
