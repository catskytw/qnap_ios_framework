//
//  QNVideoStationAPIManager.h
//  QNAPFramework
//
//  Created by Change.Liao on 13/9/27.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNModuleBaseObject.h"
#import "QNAPFramework.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "QNLogin.h"

#define CountInPage 200
/**
 *  Sort type of video files
 *
 *  @param NSInteger             NSInteger Type
 *  @param VideoFileListSortType name of thie type definition
 *
 *  @return a enum of VideoFileListSortType
 */
typedef NS_ENUM(NSInteger, VideoFileListSortType){
    videoFileListSortByName,
    videoFileListSortByDBtime,
    videoFileListSortByCreate,
    videoFileListSortByModify,
    videoFileListSortByColor,
    videoFilelistSortByRating,
    videoFileListSortByTimeLine,
    videoFileListSortByRandom
};

typedef NS_ENUM(NSInteger, VideoFileHomePath){
    videoFileHomePathPublic,
    videoFileHomePathPrivate,
    videoFileHomePathQSync,
    videoFileHomePathCollections,
    videoFileHomePathSmartCollections,
    videoFileHomePathTrashCan
};

typedef NS_ENUM(NSInteger, VideoAlbumType){
    videoAlbums,
    videoSmartAlbums
};

typedef NS_ENUM(NSInteger, VideoTransCodeDegree){
    videoDegree90,
    videoDegree180,
    videoDegree270,
    videoDegree360
};

typedef NS_ENUM(NSInteger, VideoTransCodeType){
    VideoTransCode240p,
    VideoTransCode360p,
    VideoTransCode480p,
    VideoTransCode720p,
    VideoTransCode1080p
};

@interface QNVideoStationAPIManager : QNModuleBaseObject{
    NSURLSession *_delegateSession;
    NSMutableArray *_downloadTasks;
    NSMutableArray *_uploadTasks;
}

@property(nonatomic, strong) QNLogin *loginInfo;
/**
 *  <#Description#>
 *
 *  @param account  <#account description#>
 *  @param password <#password description#>
 *  @param success  <#success description#>
 *  @param failure  <#failure description#>
 */
- (void)authLoginViaVideoStation:(NSString *)account withPassword:(NSString *)password withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  <#Description#>
 *
 *  @param account  <#account description#>
 *  @param password <#password description#>
 *  @param success  <#success description#>
 *  @param failure  <#failure description#>
 */
- (void)loginViaVideoStation:(NSString *)account withPassword:(NSString *)password withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;
/**
 *  get all video files list
 *
 *  @param sortType      Sorting, see typedef 'VideoFileListSortType'
 *  @param pageNumber    Which page you want to fetch on.
 *  @param countsPerPage How many records in one page.
 *  @param homePath      Which path you want to search for?
 *  @param isASC         If yes, ASC; if no, DESC.
 *  @param success       successBlock
 *  @param failure       failureBlock
 */
- (void)getAllFileListWithSortBy:(VideoFileListSortType)sortType withPageNumber:(NSInteger)pageNumber withCountsPerPage:(NSInteger)countsPerPage withHomePath:(VideoFileHomePath)homePath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic isASC:(BOOL)isASC withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  get the list of timeLine.
 *
 *  @param homePath which path you want to list
 *  @param success  successBlock, a block excuting after the request receives a response successfully
 *  @param failure  failureBlock, a block excuting after the request has any error, including error status,
 */
- (void)getTimeLineListWithHomePath:(VideoFileHomePath)homePath withSuccessBlock:(QNQNVideoTimeLineResponseSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  Get video files in a specified date.
 *
 *  @param timeLineLabel This parameter's value is from content of timeline xml tag <YearMonth>, e.g. 2013-09
 *  @param sortType      sort by
 *  @param pageNumber    current page.
 *  @param countPerPage  how many records in one page.
 *  @param homePath      which path you want to list
 *  @param isASC         is ASC? If no, DESC.
 *  @param success       successBlock, a block excuting after the request receives a response successfully
 *  @param failure       failureBlock, a block excuting after the request has any error, including error status
 */
- (void)getTimeLineFileListWithTimeLineLabel:(NSString *)timeLineLabel withSortBy:(VideoFileListSortType)sortType withPageNumber:(NSInteger)pageNumber withCountsPerPage:(NSInteger)countsPerPage withHomePath:(VideoFileHomePath)homePath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic isASC:(BOOL)isASC  withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailueBlock:(QNFailureBlock)failure;

/**
 *  lazyloading the image asynchrony.
 *
 *  @param targetView  the imageView which wants the lazyloading
 *  @param placeHolder the place-holder image.
 *  @param fileId      image file's id
 *  @param progress    progressing block, present by DDLogVerbose.
 *  @param completed   completed block, invoked after the download is completed.
 */
- (void)lazyloadingImage:(UIImageView *)targetView withPlaceHolder:(UIImage *)placeHolder withFileId:(NSString *)fileId withProgressBlock:(void(^)(NSUInteger r, long long e))progress withCompletedBlock:(void(^)(UIImage *image, NSError *e, SDImageCacheType cacheType))completed;

- (void)uploadVideoFilm:(NSString *)albumID withLocalPath:(NSURL *)fileURL withHomePath:(VideoFileHomePath)homePath withRemotePath:(NSString *)remotePath withNSURLSessionDelegate:(id<NSURLSessionTaskDelegate>)delegate withStartupBlock:(void(^)(NSURLSessionUploadTask *uploadTask))startUploadingBlock withCompletedBlock:(void(^)(id response, NSError *error))completed;
/**
 *  1. create downloadTask
 *  2. assign the delegate by yourself, that means you should handle the resume data by yourself too.
 *     why we do this? There are many requirements from each station about downloading, thus it's no sense if we give the delegate forcly in
 *     a same place. But we add the operaiont into a same download queue because of the limited bandwith of mobile device.
 *
 *  @param filmURL   film url
 *  @param localPath local path, no effect now.
 *  @param delegate  delegate instance, need NSURLSessionDownloadDelegate protocol
 */
- (void)downloadVideoFilm:(NSString *)filmURL withLocalPath:(NSString *)localPath witFilmID:(NSString *)filmID withDelegate:(id<NSURLSessionDownloadDelegate>)delegate;

- (void)downloadResumeTask:(NSData *)resumeData withTaskID:(NSString *)taskID;

/**
 *  get only folder names under the specified folder
 *
 *  @param dir
 *  @param sortType   folder path
 *  @param isASC      asc or desc
 *  @param pageNumber <#pageNumber description#>
 *  @param count      <#count description#>
 *  @param success    <#success description#>
 *  @param failure    <#failure description#>
 */
- (void)getFolderList:(NSString *)dir withSortType:(VideoFileListSortType)sortType isASC:(BOOL)isASC pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  get the folders & videoFiles under the specified folder
 *
 *  @param dir        dir path, @"" means the root and homePath is neccessary at the same time.
 *  @param sortType   sortType
 *  @param isASC      asc or desc
 *  @param pageNumber which page
 *  @param count      how many records in one page
 *  @param success    success block
 *  @param failure    failure block
 */
- (void)getFolderFileList:(NSString *)dir withHomePath:(VideoFileHomePath)homePath withSortType:(VideoFileListSortType)sortType  pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic isASC:(BOOL)isASC withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

#pragma mark - Collection Management
/**
 *  Get all collections in videoStation. Until this version released, there are totaly 2 collections of videoStation: Albums, SmartAlbum.
 *  The purpose of this method is to fetch the correct albumId for CollectionFileList API. That's all.
 *
 *  @param albumType  albumType, enum of VideoAlbumType
 *  @param sortType   sortType. Not sure these values: color, timeline, dbtime are valid in this method.
 *  @param isASC      ASC or DESC ?
 *  @param pageNumber page number from 1 to ?
 *  @param count      How many items in one page?
 *  @param success    success block
 *  @param failure    failure block
 */
- (void)getCollections:(VideoAlbumType)albumType sortType:(VideoFileListSortType)sortType isASC:(BOOL)isASC pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count withFilterDic:(NSDictionary *)filterDic withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  get all video items in one collection, which's collectionID fetched by the above API.
 *
 *  @param collectionID collection ID
 *  @param sortType     sort type
 *  @param isASC        ASC or DESC
 *  @param pageNumber   pageNumber, from 1 to ?
 *  @param count        how many items in one page
 *  @param homePath     including public, private, qsync
 *  @param success      success block
 *  @param failure      failure block
 */
- (void)getCollectionVideoItems:(NSString *)collectionID sortType:(VideoFileListSortType)sortType pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count homePath:(VideoFileHomePath)homePath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic isASC:(BOOL)isASC withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  create a collection
 *
 *  @param collectionName  collection name
 *  @param albumType       albumType, including smartAlbum and album
 *  @param isOpened        is opened?
 *  @param isShared        is shared?
 *  @param isEditByO       is edited by owner
 *  @param expiredDateTime expiredDateTime, datetime syntax: yyyy/MM/dd HH:mm:ss
 *  @param success         success block
 *  @param failure         failure block
 */
- (void)createCollection:(NSString *)collectionName collectionType:(VideoAlbumType)albumType isOpened:(BOOL)isOpened isShared:(BOOL)isShared isEditByOwner:(BOOL)isEditByO  withExpireDateTime:(NSString *)expiredDateTime withSuccessBlock:(QNQNVideoCollectionCreateResponseSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  Remove the collection only; the real video files are still existing.
 *
 *  @param collectionID collection ID
 *  @param success      success block
 *  @param failure      failure block
 */
- (void)removeCollection:(NSString *)collectionID withSuceessBlock:(QNSuccessBlock)success withFailureblock:(QNFailureBlock)failure;

/**
 *  add video items into a specified collection.
 *
 *  @param collectionId the collection ID you want to add into.
 *  @param fIDs         the all IDs of video items you want to add into.
 *  @param success      success block
 *  @param failure      failure block
 */
- (void)addToCollection:(NSString *)collectionId withFileIDS:(NSArray *)fIDs withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  <#Description#>
 *
 *  @param fIDs         <#fIDs description#>
 *  @param collectionID <#collectionID description#>
 *  @param success      <#success description#>
 *  @param failure      <#failure description#>
 */
- (void)removeFromCollection:(NSArray *)fIDs withCollectionID:(NSString *)collectionID withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

#pragma mark - TrashCan Management
/**
 *  get all video items in trash can
 *
 *  @param sortType   sort type
 *  @param isASC      ASC or DESC?
 *  @param pageNumber page number
 *  @param success    success block
 *  @param failure    failure block
 */
- (void)getTrashCanWithSortType:(VideoFileListSortType)sortType isASC:(BOOL)isASC pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  delete files to trashcan
 *
 *  @param homePath public, private, qsync
 *  @param fileIDs  file IDs
 *  @param success  success block
 *  @param failure  failure block
 */
- (void)deleteFileToTrashCan:(VideoFileHomePath)homePath withFiles:(NSArray *)fileIDs withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  recover video files which are in trashcan to their original file paths.
 *
 *  @param fileIDs file IDs
 *  @param success success block
 *  @param failure failure block
 */
- (void)recoverFileFromTrashCan:(NSArray *)fileIDs withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  delete files in trashcan
 *
 *  @param fileIDs file IDs
 *  @param success success block
 *  @param failure failure block
 */
- (void)deleteFileFromTrashCan:(NSArray *)fileIDs withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  clean all files in trashcan
 *
 *  @param success success block
 *  @param failure failure block
 */
- (void)cleanTrashCan:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

#pragma mark - FileManage
/**
 *  Setting videoFile Title
 *
 *  @param fileID   video file ID
 *  @param newTitle new video title
 *  @param success  success block
 *  @param failure  failure block
 */
- (void)setVideoFileTitle:(NSString *)fileID withNewTitle:(NSString *)newTitle withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  Setting Video Media Type
 *
 *  @param vt          mediaType, 0:other, 1:movie, 2:TV, 3:MV, 4:home
 *  @param videoFileId video file id
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)setVideoMediaType:(NSInteger)vt withFileID:(NSString *)videoFileId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  set the video transCode
 *
 *  @param degree      VideoTransCodeDegree: 90,180, 270, 360(?); give the enum of VideoTransCodeDegree
 *  @param videoFileID video file ID
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)videoTransCode:(VideoTransCodeDegree)degree withResolutions:(NSArray *)resolutions withFileID:(NSString *)videoFileID withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  setting the video rating
 *
 *  @param videoFileID video file ID
 *  @param rate        rate, 0~100
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)setVideoRating:(NSString *)videoFileID withRate:(NSInteger)rate withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  setting the video color label
 *
 *  @param videoFileID file ID
 *  @param color       1~6
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)setVideoColorLabel:(NSString *)videoFileID withColor:(NSInteger)color withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  giving the keyword of one video
 *
 *  @param videoFileID video ID
 *  @param keywords    keywords, you may give directly by 'xxxx,yyyy,zzzz'
 *  @param isAppend    is append or not? if not, clean it and set a new keyword.
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)setVideoKeyword:(NSString *)videoFileID withKeywords:(NSArray *)keywords isAppend:(BOOL)isAppend withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

/**
 *  setting the video comment
 *
 *  @param videoFileID video file ID
 *  @param comment     Comment string
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)setVideoComment:(NSString *)videoFileID withComment:(NSString *)comment withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)sendEmailForSharing:(NSString *)collectionId withIP:(NSString *)ip sender:(NSString *)senderEmail receiver:(NSString *)receiverEmail withSubtitle:(NSString *)subtitle withComment:(NSString *)comment withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;
#pragma mark - Movie Info
/**
 *  get the IMBD information, you may lazyload the poster by the url in the response.
 *
 *  @param videoFileID film file ID
 *  @param success     success block
 *  @param failure     failure block
 */
- (void)getIMDBInfo:(NSString *)videoFileID withSuccessBlock:(QNSuccessBlock)success withFailureblock:(QNFailureBlock)failure;
#pragma mark - UploadTask Operation
- (void)pauseAllUploadTask;

- (void)resumeAllUploadTask;

- (void)cancelAllUploadTask;

- (void)pauseUploadTask:(NSString *)taskID;

- (void)cancelUploadTask:(NSString *)taskID;

- (void)resumeUploadTask:(NSString *)taskID;

- (NSURLSessionUploadTask *)findUploadTask:(NSString *)taskID;

#pragma mark - DownloadTask Operation
/**
 *  pause all download task
 */
- (void)pauseAllDownloadTasks;

/**
 *  resume all download tasks
 */
- (void)resumeAllDownloadTask;

/**
 *  cancel all downloadtask
 *
 *  @param resumeOperation:
 *  After canceling, the download task would return a resume key(binaray), we here save the resume key into CoreData for resuming task or something else you want to do.
 */
- (void)cancelAllDownloadTask:(void(^)(NSString *filmID, NSData *resumeKey))resumeOperation;

/**
 *  Restore all download tasks. Actually, this function resumes all tasks calceled by User used the avobe function.
 *
 *  @param restoreBlock <#restoreBlock description#>
 */
- (void)restoreAllDownloadTask:(void(^)(void))restoreBlock;

/**
 *  pause single one download task
 *
 *  @param taskID task ID
 */
- (void)pauseDownloadTaskWithID:(NSString *)taskID;

/**
 *  resume the specified one task which paused now.
 *
 *  @param taskID <#taskID description#>
 */
- (void)resumeDownloadTaskWithID:(NSString *)taskID;

- (void)cancelDownloadTaskWithID:(NSString *)taskID;

- (NSURLSessionDownloadTask *)findDownloadTask:(NSString *)taskID;

#pragma mark - DLNA & AirPlay
- (void)getRenders:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)getPlayers:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)listContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)getPlayerStatus:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)playContent:(NSString *)renderId withFileId:(NSArray *)fileIds withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)stopContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)pauseContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)nextContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)previousContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)seekContent:(NSString *)renderId seekTime:(NSInteger)sec withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)jumpContent:(NSString *)renderId trackOrder:(NSInteger)trackOrder withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)setRepeatMode:(NSString *)renderId repeatMode:(NSInteger)repeatMode withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

- (void)setVolume:(NSString *)renderId volume:(NSInteger)volume withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;
@end
