//
//  QNVideoCollectionResponse.h
//  QNAPFramework
//
//  Created by Change.Liao on 2013/12/30.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QNCollection;

@interface QNVideoCollectionResponse : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSSet *relationship_DataList;
@end

@interface QNVideoCollectionResponse (CoreDataGeneratedAccessors)

- (void)addRelationship_DataListObject:(QNCollection *)value;
- (void)removeRelationship_DataListObject:(QNCollection *)value;
- (void)addRelationship_DataList:(NSSet *)values;
- (void)removeRelationship_DataList:(NSSet *)values;

@end
