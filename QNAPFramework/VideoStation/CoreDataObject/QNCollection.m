//
//  QNCollection.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/7/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNCollection.h"


@implementation QNCollection

@dynamic albumType;
@dynamic cAlbumTitle;
@dynamic config;
@dynamic coverCode;
@dynamic coverHeight;
@dynamic coverType;
@dynamic coverWidth;
@dynamic dateCreated;
@dynamic dateModified;
@dynamic editbyo;
@dynamic expiration;
@dynamic iAlbumCover;
@dynamic invalidFlag;
@dynamic isExpired;
@dynamic iVideoAlbumId;
@dynamic owner;
@dynamic protectionStatus;
@dynamic protectionStatus2;
@dynamic public;
@dynamic qts;
@dynamic shared;
@dynamic videoCount;

@end
