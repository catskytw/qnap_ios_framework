//
//  QNVideoContentItem.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNVideoContentItem : NSManagedObject

@property (nonatomic, retain) NSString * playListID;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * mediaType;
@property (nonatomic, retain) NSString * contentType;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * upnpID;
@property (nonatomic, retain) NSNumber * orderNum;

@end
