//
//  QNVideoContentList.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoContentList.h"
#import "QNVideoContentItem.h"


@implementation QNVideoContentList

@dynamic status;
@dynamic itemCount;
@dynamic relationship_contentItem;

@end
