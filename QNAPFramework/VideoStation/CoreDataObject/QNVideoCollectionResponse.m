//
//  QNVideoCollectionResponse.m
//  QNAPFramework
//
//  Created by Change.Liao on 2013/12/30.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoCollectionResponse.h"
#import "QNCollection.h"


@implementation QNVideoCollectionResponse

@dynamic status;
@dynamic relationship_DataList;

@end
