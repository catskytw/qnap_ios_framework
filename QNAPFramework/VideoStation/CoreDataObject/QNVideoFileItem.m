//
//  QNVideoFileItem.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/7/17.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoFileItem.h"


@implementation QNVideoFileItem

@dynamic addToDbTime;
@dynamic cFileName;
@dynamic colorLevel;
@dynamic comment;
@dynamic cPictureTitle;
@dynamic dateCreated;
@dynamic dateModified;
@dynamic dateTime;
@dynamic duration;
@dynamic f_id;
@dynamic iFileSize;
@dynamic iHeight;
@dynamic importYearMonthDay;
@dynamic isNew;
@dynamic iWidth;
@dynamic keywords;
@dynamic mask;
@dynamic mediaType;
@dynamic mime;
@dynamic orientation;
@dynamic prefix;
@dynamic rating;
@dynamic status;
@dynamic transcodeStatus;
@dynamic type;
@dynamic uid;
@dynamic uuid;
@dynamic v240P;
@dynamic v360P;
@dynamic v480P;
@dynamic v720P;
@dynamic v1080P;
@dynamic yearMonth;
@dynamic yearMonthDay;

@end
