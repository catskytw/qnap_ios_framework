//
//  QNCollection.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/7/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNCollection : NSManagedObject

@property (nonatomic, retain) NSString * albumType;
@property (nonatomic, retain) NSString * cAlbumTitle;
@property (nonatomic, retain) NSString * config;
@property (nonatomic, retain) NSString * coverCode;
@property (nonatomic, retain) NSNumber * coverHeight;
@property (nonatomic, retain) NSString * coverType;
@property (nonatomic, retain) NSNumber * coverWidth;
@property (nonatomic, retain) NSNumber * dateCreated;
@property (nonatomic, retain) NSNumber * dateModified;
@property (nonatomic, retain) NSNumber * editbyo;
@property (nonatomic, retain) NSString * expiration;
@property (nonatomic, retain) NSString * iAlbumCover;
@property (nonatomic, retain) NSNumber * invalidFlag;
@property (nonatomic, retain) NSNumber * isExpired;
@property (nonatomic, retain) NSString * iVideoAlbumId;
@property (nonatomic, retain) NSNumber * owner;
@property (nonatomic, retain) NSString * protectionStatus;
@property (nonatomic, retain) NSString * protectionStatus2;
@property (nonatomic, retain) NSNumber * public;
@property (nonatomic, retain) NSNumber * qts;
@property (nonatomic, retain) NSNumber * shared;
@property (nonatomic, retain) NSNumber * videoCount;

@end
