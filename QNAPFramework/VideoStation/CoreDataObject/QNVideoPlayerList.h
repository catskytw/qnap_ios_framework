//
//  QNVideoPlayerList.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNVideoPlayerList : NSManagedObject

@property (nonatomic, retain) NSNumber * playCount;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSSet *relationship_player;
@end

@interface QNVideoPlayerList (CoreDataGeneratedAccessors)

- (void)addRelationship_playerObject:(NSManagedObject *)value;
- (void)removeRelationship_playerObject:(NSManagedObject *)value;
- (void)addRelationship_player:(NSSet *)values;
- (void)removeRelationship_player:(NSSet *)values;

@end
