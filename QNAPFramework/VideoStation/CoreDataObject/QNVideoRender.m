//
//  QNVideoRender.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoRender.h"


@implementation QNVideoRender

@dynamic deviceName;
@dynamic deviceId;
@dynamic ip;
@dynamic type;
@dynamic maxVolume;
@dynamic mac;
@dynamic deviceType;
@dynamic active;

@end
