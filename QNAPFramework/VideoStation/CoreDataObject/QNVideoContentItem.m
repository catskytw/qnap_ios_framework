//
//  QNVideoContentItem.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoContentItem.h"


@implementation QNVideoContentItem

@dynamic playListID;
@dynamic uuid;
@dynamic mediaType;
@dynamic contentType;
@dynamic content;
@dynamic upnpID;
@dynamic orderNum;

@end
