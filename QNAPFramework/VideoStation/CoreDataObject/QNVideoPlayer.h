//
//  QNVideoPlayer.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNVideoPlayer : NSManagedObject

@property (nonatomic, retain) NSString * renderID;
@property (nonatomic, retain) NSString * deviceName;

@end
