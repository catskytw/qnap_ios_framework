//
//  QNVideoPlayerStatus.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoPlayerStatus.h"


@implementation QNVideoPlayerStatus

@dynamic status;
@dynamic deviceName;
@dynamic playerType;
@dynamic playerState;
@dynamic playMode;
@dynamic playlistTitle;
@dynamic appType;
@dynamic trackType;
@dynamic trackContentType;
@dynamic trackContent;
@dynamic currTrack;
@dynamic totalTracks;
@dynamic currTime;
@dynamic totalTime;
@dynamic volume;
@dynamic repeatmode;

@end
