//
//  QNVideoCollectionCreateResponse.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/3/21.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoCollectionCreateResponse.h"


@implementation QNVideoCollectionCreateResponse

@dynamic status;
@dynamic output;

@end
