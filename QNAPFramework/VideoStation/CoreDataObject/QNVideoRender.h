//
//  QNVideoRender.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNVideoRender : NSManagedObject

@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) NSString * deviceId;
@property (nonatomic, retain) NSString * ip;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * maxVolume;
@property (nonatomic, retain) NSString * mac;
@property (nonatomic, retain) NSString * deviceType;
@property (nonatomic, retain) NSNumber * active;

@end
