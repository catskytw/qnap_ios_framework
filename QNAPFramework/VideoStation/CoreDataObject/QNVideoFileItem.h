//
//  QNVideoFileItem.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/7/17.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNVideoFileItem : NSManagedObject

@property (nonatomic, retain) NSNumber * addToDbTime;
@property (nonatomic, retain) NSString * cFileName;
@property (nonatomic, retain) NSNumber * colorLevel;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * cPictureTitle;
@property (nonatomic, retain) NSString * dateCreated;
@property (nonatomic, retain) NSString * dateModified;
@property (nonatomic, retain) NSString * dateTime;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSString * f_id;
@property (nonatomic, retain) NSNumber * iFileSize;
@property (nonatomic, retain) NSNumber * iHeight;
@property (nonatomic, retain) NSString * importYearMonthDay;
@property (nonatomic, retain) NSNumber * isNew;
@property (nonatomic, retain) NSNumber * iWidth;
@property (nonatomic, retain) NSString * keywords;
@property (nonatomic, retain) NSNumber * mask;
@property (nonatomic, retain) NSString * mediaType;
@property (nonatomic, retain) NSString * mime;
@property (nonatomic, retain) NSNumber * orientation;
@property (nonatomic, retain) NSString * prefix;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSNumber * transcodeStatus;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * uid;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSNumber * v240P;
@property (nonatomic, retain) NSNumber * v360P;
@property (nonatomic, retain) NSNumber * v480P;
@property (nonatomic, retain) NSNumber * v720P;
@property (nonatomic, retain) NSNumber * v1080P;
@property (nonatomic, retain) NSString * yearMonth;
@property (nonatomic, retain) NSString * yearMonthDay;

@end
