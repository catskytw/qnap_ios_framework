//
//  QNVideoCollectionCreateResponse.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/3/21.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNVideoCollectionCreateResponse : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * output;

@end
