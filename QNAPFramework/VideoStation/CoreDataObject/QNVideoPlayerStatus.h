//
//  QNVideoPlayerStatus.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNVideoPlayerStatus : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) NSString * playerType;
@property (nonatomic, retain) NSString * playerState;
@property (nonatomic, retain) NSString * playMode;
@property (nonatomic, retain) NSString * playlistTitle;
@property (nonatomic, retain) NSString * appType;
@property (nonatomic, retain) NSString * trackType;
@property (nonatomic, retain) NSString * trackContentType;
@property (nonatomic, retain) NSString * trackContent;
@property (nonatomic, retain) NSNumber * currTrack;
@property (nonatomic, retain) NSNumber * totalTracks;
@property (nonatomic, retain) NSString * currTime;
@property (nonatomic, retain) NSString * totalTime;
@property (nonatomic, retain) NSNumber * volume;
@property (nonatomic, retain) NSNumber * repeatmode;

@end
