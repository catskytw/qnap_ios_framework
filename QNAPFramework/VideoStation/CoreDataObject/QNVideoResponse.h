//
//  QNVideoResponse.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/3/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNVideoResponse : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * msg;

@end
