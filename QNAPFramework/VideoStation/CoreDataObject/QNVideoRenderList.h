//
//  QNVideoRenderList.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QNVideoRender;

@interface QNVideoRenderList : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSNumber * deviceCount;
@property (nonatomic, retain) NSSet *relationship_Renders;
@end

@interface QNVideoRenderList (CoreDataGeneratedAccessors)

- (void)addRelationship_RendersObject:(QNVideoRender *)value;
- (void)removeRelationship_RendersObject:(QNVideoRender *)value;
- (void)addRelationship_Renders:(NSSet *)values;
- (void)removeRelationship_Renders:(NSSet *)values;

@end
