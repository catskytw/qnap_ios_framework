//
//  QNVideoContentList.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QNVideoContentItem;

@interface QNVideoContentList : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSNumber * itemCount;
@property (nonatomic, retain) NSSet *relationship_contentItem;
@end

@interface QNVideoContentList (CoreDataGeneratedAccessors)

- (void)addRelationship_contentItemObject:(QNVideoContentItem *)value;
- (void)removeRelationship_contentItemObject:(QNVideoContentItem *)value;
- (void)addRelationship_contentItem:(NSSet *)values;
- (void)removeRelationship_contentItem:(NSSet *)values;

@end
