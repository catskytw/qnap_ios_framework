//
//  QNLogin.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/26.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNLogin : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSNumber * is_admin; //是否administrator group
@property (nonatomic, retain) NSString * sid;
@property (nonatomic, retain) NSString * usr_id;
@property (nonatomic, retain) NSString * usr_name;
@property (nonatomic, retain) NSString * usr_ip;
@property (nonatomic, retain) NSString * usr_email;
@property (nonatomic, retain) NSNumber * usr_home; //有無home
@property (nonatomic, retain) NSNumber * usr_recycle; //有無開啟recycle
@property (nonatomic, retain) NSNumber * rt_transcode; //有無real time transcode功能
@property (nonatomic, retain) NSNumber * qsync;
@property (nonatomic, retain) NSNumber * transcodeServer;
@property (nonatomic, retain) NSString * notification; //發notification要出去時, 要帶的; 是否就不能share
@property (nonatomic, retain) NSString * last_login;
@property (nonatomic, retain) NSNumber * login_count;
@property (nonatomic, retain) NSString * writable;
@property (nonatomic, retain) NSString * readonly;
@property (nonatomic, retain) NSString * deny;
@property (nonatomic, retain) NSString * photoShares;
@property (nonatomic, retain) NSString * videoShares;
@property (nonatomic, retain) NSNumber * multimedia;
@property (nonatomic, retain) NSNumber * doubleConfirm; //是否每次進入homes, private, qsync是否要詢問帳號密碼
@property (nonatomic, retain) NSNumber * quickGuide;
@property (nonatomic, retain) NSNumber * x_search;
@property (nonatomic, retain) NSNumber * x_download;
@property (nonatomic, retain) NSNumber * x_transcode;
@property (nonatomic, retain) NSNumber * x_rtt;
@property (nonatomic, retain) NSNumber * is_mobile;
@property (nonatomic, retain) NSNumber * s_start_time;
@property (nonatomic, retain) NSNumber * s_last_visit;
@property (nonatomic, retain) NSString * defaultLat;
@property (nonatomic, retain) NSString * defaultLon;
@property (nonatomic, retain) NSString * auth_by;
@property (nonatomic, retain) NSString * credential;
@property (nonatomic, retain) NSString * builtinFirmwareVersion;
@property (nonatomic, retain) NSString * appVersion;
@property (nonatomic, retain) NSString * appBuildNum;
@property (nonatomic, retain) NSNumber * auth;

@end
