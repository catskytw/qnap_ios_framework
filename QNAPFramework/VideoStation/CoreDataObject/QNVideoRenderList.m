//
//  QNVideoRenderList.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/24.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNVideoRenderList.h"
#import "QNVideoRender.h"


@implementation QNVideoRenderList

@dynamic status;
@dynamic deviceCount;
@dynamic relationship_Renders;

@end
