//
//  QNLogin.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/2/26.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNLogin.h"


@implementation QNLogin

@dynamic status;
@dynamic is_admin;
@dynamic sid;
@dynamic usr_id;
@dynamic usr_name;
@dynamic usr_ip;
@dynamic usr_email;
@dynamic usr_home;
@dynamic usr_recycle;
@dynamic rt_transcode;
@dynamic qsync;
@dynamic transcodeServer;
@dynamic notification;
@dynamic last_login;
@dynamic login_count;
@dynamic writable;
@dynamic readonly;
@dynamic deny;
@dynamic photoShares;
@dynamic videoShares;
@dynamic multimedia;
@dynamic doubleConfirm;
@dynamic quickGuide;
@dynamic x_search;
@dynamic x_download;
@dynamic x_transcode;
@dynamic x_rtt;
@dynamic is_mobile;
@dynamic s_start_time;
@dynamic s_last_visit;
@dynamic defaultLat;
@dynamic defaultLon;
@dynamic auth_by;
@dynamic credential;
@dynamic builtinFirmwareVersion;
@dynamic appVersion;
@dynamic appBuildNum;
@dynamic auth;

@end
