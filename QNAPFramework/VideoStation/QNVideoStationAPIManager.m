//
//  QNVideoStationAPIManager.m
//  QNAPFramework
//
//  Created by Change.Liao on 13/9/27.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoStationAPIManager.h"
#import <AFNetworking/AFNetworking.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "QNVideoMapping.h"
#import "QNAPCommunicationManager.h"
#import "QNVideoFileList.h"
#import "QNVideoFileItem.h"
#import "QNVideoTimeLineResponse.h"
#import "QNCollection.h"
#import "QNVideoCollectionResponse.h"
#import "QNLogin.h"
#import "QNAPFrameworkUtil.h"
#import "QNNormalSuccessResponse.h"
#import "QNVideoResponse.h"

@implementation QNVideoStationAPIManager
- (void)authLoginViaVideoStation:(NSString *)account withPassword:(NSString *)password withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *entityMapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:entityMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSString *encodingPassword = [QNAPFrameworkUtil ezEncode:password];
    NSDictionary * parameters = @{@"a": @"auth",
                                  @"u":account,
                                  @"p":encodingPassword};
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/user.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                    QNNormalSuccessResponse *response = r.firstObject;
                                    if ([response.status intValue] == 1) {
                                        success(o, r);
                                    }else
                                        failure(o, [NSError errorWithDomain:NSLocalizedString(@"IncorrectLoginInfo", nil) code:-1 userInfo:nil]);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
}

/**
 *  login via videoStation
 *
 *  @param account  account
 *  @param password password
 *  @param success  success block
 *  @param failure  failure block, nserror code: 10000: wrong password, code -1011:no videoStation
 */
- (void)loginViaVideoStation:(NSString *)account withPassword:(NSString *)password withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *entityMapping = [QNVideoMapping mappingForLogin];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:entityMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSString *encodingPassword = [QNAPFrameworkUtil ezEncode:password];
    NSDictionary * parameters = @{@"a": @"login",
                                  @"u":account,
                                  @"p":encodingPassword};
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/user.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                    QNLogin *mappingObject = [r firstObject];
                                    if (mappingObject.status && [mappingObject.status integerValue] == 0) {
                                        failure(o, [NSError errorWithDomain:@"Login Error, wrong password?" code:10000 userInfo:nil]);
                                    }else if (!mappingObject.status)
                                        failure(o, [NSError errorWithDomain:@"Login sid error, no videoStation?" code:9999 userInfo:nil]);
                                    else{
                                        [QNAPCommunicationManager share].sidForMultimedia = [mappingObject.sid mutableCopy];
                                        self.loginInfo = mappingObject;
                                        success(o, r);
                                    }
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
}

#pragma mark - VideoFileList
- (void)getAllFileListWithSortBy:(VideoFileListSortType)sortType withPageNumber:(NSInteger)pageNumber withCountsPerPage:(NSInteger)countsPerPage withHomePath:(VideoFileHomePath)homePath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic isASC:(BOOL)isASC withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *fileListMapping = [QNVideoMapping mappingForGetAllVideoList];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:fileListMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    __block NSNumber *typeHomePath = [NSNumber numberWithInt:[self valueForHomePath:homePath]];
    NSMutableDictionary *parameters = [@{@"t":@"videos",
                                         @"s": [self sortString:sortType],
                                         @"sd":(isASC)?@"ASC":@"DESC",
                                         @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                                         @"p": [NSNumber numberWithInt:pageNumber],
                                         @"c": [NSNumber numberWithInt:countsPerPage],
                                         @"h": [NSNumber numberWithInt:[self valueForHomePath:homePath]]
                                         } mutableCopy];
    
    [parameters setValuesForKeysWithDictionary:filterDic];
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/list.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    QNVideoFileList *mappingObject = [m firstObject];
                                    
                                    [self saveUUIDIntoFileItem:mappingObject withUUID:uuid withAddtionBlock:^(QNVideoFileItem *item){
                                        item.type = [NSNumber numberWithInteger:[typeHomePath integerValue]];
                                    }];
                                    
                                    success(o, m, mappingObject);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
}

- (void)getTimeLineListWithHomePath:(VideoFileHomePath)homePath withSuccessBlock:(QNQNVideoTimeLineResponseSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *timeLineMapping = [QNVideoMapping mappingForTimeLineList];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:timeLineMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSDictionary * parameters = @{@"t": @"timeline-v",
                                  @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                                  @"h":[NSNumber numberWithInt:[self valueForHomePath:homePath]]};
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/list.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                    QNVideoTimeLineResponse *mappingObject = [r firstObject];
                                    success(o, r, mappingObject);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
}

- (void)getTimeLineFileListWithTimeLineLabel:(NSString *)timeLineLabel withSortBy:(VideoFileListSortType)sortType withPageNumber:(NSInteger)pageNumber withCountsPerPage:(NSInteger)countsPerPage withHomePath:(VideoFileHomePath)homePath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic isASC:(BOOL)isASC  withSuccessBlock:(QNQNVideoFileListSuccessBlock)success withFailueBlock:(QNFailureBlock)failure{
    RKEntityMapping *fileListMapping = [QNVideoMapping mappingForGetAllVideoList];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:fileListMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSMutableDictionary *parameters = [@{@"t":@"videos",
                                         @"d":timeLineLabel,
                                         @"s": [self sortString:sortType],
                                         @"sd":(isASC)?@"ASC":@"DESC",
                                         @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                                         @"p": [NSNumber numberWithInt:pageNumber],
                                         @"c": [NSNumber numberWithInt:countsPerPage],
                                         @"h": [NSNumber numberWithInt:[self valueForHomePath:homePath]]
                                         } mutableCopy];
    if (filterDic) {
        [parameters setValuesForKeysWithDictionary:filterDic];
    }
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/list.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    QNVideoFileList *mappingObject = [m firstObject];
                                    [self saveUUIDIntoFileItem:mappingObject withUUID:uuid withAddtionBlock:^(QNVideoFileItem *item){
                                        //nothing
                                    }];
                                    
                                    success(o, m, mappingObject);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
}

- (void)lazyloadingImage:(UIImageView *)targetView withPlaceHolder:(UIImage *)placeHolder withFileId:(NSString *)fileId withProgressBlock:(void(^)(NSUInteger r, long long e))progress withCompletedBlock:(void(^)(UIImage *image, NSError *e, SDImageCacheType cacheType))completed{
    NSString *urlString = [NSString stringWithFormat:@"%@video/api/thumb.php?s=1&m=display&f=%@&t=video&o=1&sid=%@", self.baseURL, fileId, [QNAPCommunicationManager share].sidForMultimedia];
    
    [targetView setImageWithURL:[NSURL URLWithString:urlString]
               placeholderImage:placeHolder
                        options:0
                       progress:^(NSInteger r, NSInteger e){
                           if (progress)
                               progress(r, e);
                       }
                      completed:^(UIImage *image, NSError *e, SDImageCacheType cacheType){
                          if (e) {
                              DDLogError(@"webimage error: %@, url:%@", e, urlString);
                          }
                      }];
}

- (void)downloadVideoFilm:(NSString *)filmURL withLocalPath:(NSString *)localPath witFilmID:(NSString *)filmID withDelegate:(id<NSURLSessionDownloadDelegate>)delegate{
    [self initialCollection];
    
    if (![self findDownloadTask:filmID]) {
        NSURLSessionDownloadTask *_downloadTask = [[QNAPCommunicationManager share].downloadSession downloadTaskWithURL:[NSURL URLWithString:filmURL]];
        [_downloadTasks addObject:_downloadTask];
        _downloadTask.taskDescription = filmID;
        [_downloadTask resume];
    }
}

- (void)uploadVideoFilm:(NSString *)albumID withLocalPath:(NSURL *)fileURL withHomePath:(VideoFileHomePath)homePath withRemotePath:(NSString *)remotePath withNSURLSessionDelegate:(id<NSURLSessionTaskDelegate>)delegate withStartupBlock:(void(^)(NSURLSessionUploadTask *uploadTask))startUploadingBlock withCompletedBlock:(void(^)(id response, NSError *error))completed{
    //albumID, VideoFileHomePath, destinationPath
    NSString *uploadURLString = [self urlStringForUploadVideoFile:@{@"albumID":(albumID)?albumID:@"",
                                                                    @"VideoFileHomePath": @(homePath),
                                                                    @"destinationPath":(remotePath)?remotePath:@""}];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:uploadURLString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0f];
    //setting the http request
    [request setHTTPMethod:@"POST"];
    NSString *boundary = [self boundaryString];
    [request addValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];

    /**
     *  Content-Disposition: form-data; name="files[]"; filename="test.mp4"
     *  Content-Type: video/mp4
     */

    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig
                                                          delegate:delegate
                                                     delegateQueue:[NSOperationQueue mainQueue]];
    
    
    NSData *fileData = [NSData dataWithContentsOfFile:[fileURL path]];
    NSData *data = [self createBodyWithBoundary:boundary username:nil password:nil data:fileData filename:[[fileURL path] lastPathComponent]];

    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
                                                               fromData:data
                                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                                                          if (error) {
                                                              if (completed) {
                                                                  completed(nil, error);
                                                              }
                                                          }
                                                          
                                                          NSError *parsingError = nil;
                                                          id jsonDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parsingError];
                                                          if (parsingError) {
                                                              if (completed) {
                                                                  completed(nil, parsingError);
                                                              }
                                                          }else{
                                                              if (completed) {
                                                                  completed(jsonDic, nil);
                                                              }
                                                          }
                                                      }];
                                          //generate a taskID for searching, resuming, canceling...etc.
    NSString *taskID = [[NSUUID UUID] UUIDString];
    uploadTask.taskDescription = taskID;
    [_uploadTasks addObject:uploadTask];
    [uploadTask resume];
    
    if (startUploadingBlock) {
        startUploadingBlock(uploadTask);
    }

}

- (void)downloadResumeTask:(NSData *)resumeData withTaskID:(NSString *)taskID{
    [self initialCollection];
    NSURLSessionDownloadTask *_downloadTask = [[QNAPCommunicationManager share].downloadSession downloadTaskWithResumeData:resumeData];
    _downloadTask.taskDescription = taskID;
    [_downloadTasks addObject:_downloadTask];
    [_downloadTask resume];
}

- (void)getFolderList:(NSString *)dir withSortType:(VideoFileListSortType)sortType isASC:(BOOL)isASC pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    
}

- (void)getFolderFileList:(NSString *)dir withHomePath:(VideoFileHomePath)homePath withSortType:(VideoFileListSortType)sortType  pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic isASC:(BOOL)isASC withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    
    //check error, put it on AOP?
    if (homePath != videoFileHomePathPublic && homePath != videoFileHomePathQSync && homePath != videoFileHomePathPrivate) {
        DDLogError(@"The parameter of HomePath sould be Public/QSync/Private while sending getFolderFileList");
        return;
    }
    
    RKEntityMapping *mapping = [QNVideoMapping mappingForFolderItems];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSMutableDictionary *parameters = [@{@"t":@"folderItems",
                                         @"s": [self sortString:sortType],
                                         @"sd":(isASC)?@"ASC":@"DESC",
                                         @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                                         @"h": @([self valueForHomePath:homePath]),
                                         @"p": @(pageNumber),
                                         @"c": @(count),
                                         @"dir":(dir)?dir:@""
                                         } mutableCopy];
    if (filterDic) {
        [parameters setValuesForKeysWithDictionary:filterDic];
    }
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/list.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    QNVideoFileList *mappingObject = [m firstObject];
                                    [self saveUUIDIntoFileItem:mappingObject withUUID:uuid withAddtionBlock:^(QNVideoFileItem *item){
                                        //nothing
                                    }];
                                    
                                    success(o, m);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
    
}

#pragma mark - Collection
- (void)createCollection:(NSString *)collectionName collectionType:(VideoAlbumType)albumType isOpened:(BOOL)isOpened isShared:(BOOL)isShared isEditByOwner:(BOOL)isEditByO  withExpireDateTime:(NSString *)expiredDateTime withSuccessBlock:(QNQNVideoCollectionCreateResponseSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *createCollectionMapping = [QNVideoMapping mappingForCreateCollection];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:createCollectionMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSDictionary *parameters = @{@"a":@"createAlbum",
                                 @"type":(albumType == videoAlbums)?@"default":@"smart",
                                 @"name":collectionName,
                                 @"shared":@(isShared),
                                 @"opened":@(isOpened),
                                 @"expire":expiredDateTime,
                                 @"editbyo":@(isEditByO),
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia};
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/album.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    success(o, m, (QNVideoCollectionCreateResponse *)m.firstObject);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
}

- (void)getCollections:(VideoAlbumType)albumType sortType:(VideoFileListSortType)sortType isASC:(BOOL)isASC pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count withFilterDic:(NSDictionary *)filterDic withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *collectionMapping = [QNVideoMapping mappingforCollectionList];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:collectionMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSMutableDictionary *parameters = [@{@"t":[self valueForAlbumType:albumType],
                                         @"s": [self sortString:sortType],
                                         @"sd":(isASC)?@"ASC":@"DESC",
                                         @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                                         @"p": [NSNumber numberWithInt:pageNumber],
                                         //videoStation use this parameter as 200, so be it.
                                         @"c": [NSNumber numberWithInt:count]
                                         } mutableCopy];
    if (filterDic) {
        [parameters setValuesForKeysWithDictionary:filterDic];
    }
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/list.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    success(o, m);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
    
}

- (void)getCollectionVideoItems:(NSString *)collectionID sortType:(VideoFileListSortType)sortType pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count homePath:(VideoFileHomePath)homePath withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic isASC:(BOOL)isASC withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *fileListMapping = [QNVideoMapping mappingForGetAllVideoList];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:fileListMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSMutableDictionary *parameters = [@{@"t":@"videos",
                                         @"a":collectionID,
                                         @"s": [self sortString:sortType],
                                         @"sd":(isASC)?@"ASC":@"DESC",
                                         @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                                         @"p": [NSNumber numberWithInt:pageNumber],
                                         @"c": [NSNumber numberWithInt:count],
                                         @"h": [NSNumber numberWithInt:[self valueForHomePath:homePath]]
                                         } mutableCopy];
    if (filterDic) {
        [parameters setValuesForKeysWithDictionary:filterDic];
    }
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/list.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    QNVideoFileList *mappingObject = m.firstObject;
                                    [self saveUUIDIntoFileItem:mappingObject withUUID:uuid withAddtionBlock:^(QNVideoFileItem *item){
                                    }];
                                    
                                    success(o, m);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
    
}

- (void)removeCollection:(NSString *)collectionID withSuceessBlock:(QNSuccessBlock)success withFailureblock:(QNFailureBlock)failure{
    RKEntityMapping *statusMapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:statusMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSDictionary *parameters = @{@"a":@"removeAlbum",
                                 @"f":collectionID,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/album.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    QNNormalSuccessResponse *response = (QNNormalSuccessResponse *)m.firstObject;
                                    if ([response.success boolValue])
                                        success(o, m);
                                    else
                                        failure(o, [NSError errorWithDomain:[NSString stringWithFormat:@"The response status is %i", [response.success intValue]]
                                                                       code:[response.success intValue]
                                                                   userInfo:nil]);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
}

- (void)addToCollection:(NSString *)collectionId withFileIDS:(NSArray *)fIDs withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    
    NSMutableString *customParameter = [NSMutableString string];
    BOOL isFirstAdd = YES;
    for (NSString *fID in fIDs) {
        [customParameter appendFormat:(isFirstAdd)?@"?f[]=%@":@"&f[]=%@", fID];
        isFirstAdd = NO;
    }
    NSString *customPath = [NSString stringWithFormat:@"video/api/video.php%@",customParameter];
    NSDictionary *dic = @{@"a":@"addToAlbum",
                          @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                          @"album":collectionId
                          };
    
    [self.weakRKObjectManager getObject:nil
                                   path:customPath
                             parameters:dic
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    QNNormalSuccessResponse *response = m.firstObject;
                                    if ([response.status intValue] == 0) {
                                        success(o, m);
                                    }else
                                        failure(o, [NSError errorWithDomain:@"API action failure" code:-9999 userInfo:nil]);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
}

- (void)removeFromCollection:(NSArray *)fIDs withCollectionID:(NSString *)collectionID withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    
    NSMutableString *customParameter = [NSMutableString string];
    BOOL isFirstAdd = YES;
    for (NSString *fID in fIDs) {
        [customParameter appendFormat:(isFirstAdd)?@"?f[]=%@":@"&f[]=%@", fID];
        isFirstAdd = NO;
    }
    NSString *customPath = [NSString stringWithFormat:@"video/api/video.php%@",customParameter];
    
    NSDictionary *dic = @{@"a": @"removeFromAlbum",
                          @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                          @"album":collectionID};
    [self.weakRKObjectManager getObject:nil
                                   path:customPath
                             parameters:dic
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    QNNormalSuccessResponse *response = m.firstObject;
                                    if ([response.status intValue] == 0) {
                                        success(o, m);
                                    }else
                                        failure(o, [NSError errorWithDomain:@"API action failure" code:-9999 userInfo:nil]);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
    
}

- (void)sendEmailForSharing:(NSString *)collectionId withIP:(NSString *)ip sender:(NSString *)senderEmail receiver:(NSString *)receiverEmail withSubtitle:(NSString *)subtitle withComment:(NSString *)comment withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *videoMapping = [QNVideoMapping mappingForStatusMessage];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:videoMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSString *urlString = [NSString stringWithFormat:@"http://%@/video/play.php?a=%@", ip, collectionId];
    NSDictionary *parameters = @{@"a":@"send",
                                 @"c":comment,
                                 @"s":subtitle,
                                 @"u":urlString,
                                 @"se":senderEmail,
                                 @"sn":senderEmail,
                                 @"re":receiverEmail,
                                 @"rn":receiverEmail,
                                 @"f":collectionId,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                                 };
    
    [self.weakRKObjectManager postObject:nil
                                    path:@"video/api/email.php"
                              parameters:parameters
                                 success:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                     QNVideoResponse *response = r.firstObject;
                                     if ([response.status intValue] == 0) {
                                         success(o, r);
                                     }else{
                                         failure(o, [NSError errorWithDomain:response.msg code:[response.status intValue] userInfo:nil]);
                                     }
                                 }
                                 failure:^(RKObjectRequestOperation *o, NSError *e){
                                     failure(o, e);
                                 }];
}

#pragma mark - TrashCan
- (void)getTrashCanWithSortType:(VideoFileListSortType)sortType isASC:(BOOL)isASC pageNumber:(NSInteger)pageNumber countInPage:(NSInteger)count withUUID:(NSString *)uuid withFilterDic:(NSDictionary *)filterDic withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *fileListMapping = [QNVideoMapping mappingForGetAllVideoList];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:fileListMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    NSMutableArray *parameters = [@{@"t":@"trash",
                                    @"s": [self sortString:sortType],
                                    @"sd":(isASC)?@"ASC":@"DESC",
                                    @"sid":[QNAPCommunicationManager share].sidForMultimedia,
                                    @"p": [NSNumber numberWithInt:pageNumber],
                                    @"c": [NSNumber numberWithInt:count],
                                    } mutableCopy];
    if (filterDic) {
        [parameters setValuesForKeysWithDictionary:filterDic];
    }
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/list.php"
                             parameters:parameters
                                success:^(RKObjectRequestOperation *o, RKMappingResult *m){
                                    QNVideoFileList *mappingObject = m.firstObject;
                                    [self saveUUIDIntoFileItem:mappingObject withUUID:uuid withAddtionBlock:^(QNVideoFileItem *item){
                                    }];
                                    
                                    success(o, m);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    failure(o, e);
                                }];
}

- (void)deleteFileToTrashCan:(VideoFileHomePath)homePath withFiles:(NSArray *)fileIDs withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"trash",
                                 @"h":[NSNumber numberWithInt: [self valueForHomePath:homePath]],
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    NSMutableString *path_fIDs = [NSMutableString stringWithString:@"video/api/video.php"];
    BOOL isFirstString = YES;
    //WTF
    for (NSString *fileID in fileIDs) {
        NSString *tmpString = [NSString stringWithFormat:@"%@f[]=%@",(isFirstString)?@"?":@"&",fileID];
        [path_fIDs appendString:tmpString];
        isFirstString = NO;
    }
    [self.weakRKObjectManager getObject:nil
                                   path:path_fIDs
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)recoverFileFromTrashCan:(NSArray *)fileIDs withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"recover",
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    NSMutableString *path_fIDs = [NSMutableString stringWithString:@"video/api/video.php"];
    BOOL isFirstString = YES;
    for (NSString *fileID in fileIDs) {
        NSString *tmpString = [NSString stringWithFormat:@"%@f[]=%@",(isFirstString)?@"?":@"&",fileID];
        [path_fIDs appendString:tmpString];
        isFirstString = NO;
    }
    [self.weakRKObjectManager getObject:nil
                                   path:path_fIDs
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)deleteFileFromTrashCan:(NSArray *)fileIDs withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"cleanFile",
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    NSMutableString *path_fIDs = [NSMutableString stringWithString:@"video/api/video.php"];
    BOOL isFirstString = YES;
    for (NSString *fileID in fileIDs) {
        NSString *tmpString = [NSString stringWithFormat:@"%@f[]=%@",(isFirstString)?@"?":@"&",fileID];
        [path_fIDs appendString:tmpString];
        isFirstString = NO;
    }
    [self.weakRKObjectManager getObject:nil
                                   path:path_fIDs
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)cleanTrashCan:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"cleanAll",
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/video.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}


#pragma mark - FileManage
- (void)setVideoFileTitle:(NSString *)fileID withNewTitle:(NSString *)newTitle withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"setTitle",
                                 @"f":fileID,
                                 @"title":newTitle,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/video.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)videoTransCode:(VideoTransCodeDegree)degree withResolutions:(NSArray *)resolutions withFileID:(NSString *)videoFileID withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    /**
     *  degree:-1
     *  mask:240P|360P|720P
     */
    NSInteger rotate = -1;
    switch (degree) {
        case videoDegree90:
            rotate = 90;
            break;
        case videoDegree180:
            rotate = 180;
            break;
        case videoDegree270:
            rotate = 270;
            break;
        default:
            break;
    }
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSMutableString *resolutionString = [NSMutableString string];
    BOOL isFirst = YES;
    for (NSString *resolution in resolutions) {
        [resolutionString appendFormat:isFirst?@"%@":@"|%@", resolution];
        isFirst = NO;
    }
    NSString *upper = [resolutionString uppercaseString];
    
    NSDictionary *parameters = @{@"a":@"transcode",
                                 @"f":videoFileID,
                                 @"degree":@(rotate),
                                 @"mask":upper, //fix this value
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/video.php"
                             parameters:parameters
                                success:success
                                failure:failure];
    
}

- (void)setVideoRating:(NSString *)videoFileID withRate:(NSInteger)rate withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"setRatings",
                                 @"f":videoFileID,
                                 @"rating":@(rate),
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/video.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)setVideoColorLabel:(NSString *)videoFileID withColor:(NSInteger)color withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"setColorLabel",
                                 @"f":videoFileID,
                                 @"color":@(color),
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/video.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)setVideoMediaType:(NSInteger)vt withFileID:(NSString *)videoFileId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"setvideotype",
                                 @"f":videoFileId,
                                 @"vt":@(vt),
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/video.php"
                             parameters:parameters
                                success:success
                                failure:failure];
    
}

- (void)setVideoKeyword:(NSString *)videoFileID withKeywords:(NSArray *)keywords isAppend:(BOOL)isAppend withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSMutableString *keywordString = [NSMutableString string];
    BOOL isFirst = YES;
    for (NSString *keyword in keywords) {
        if (isFirst)
            [keywordString appendFormat:@"%@", keyword];
        else
            [keywordString appendFormat:@";%@", keyword];
        isFirst = NO;
    }
    NSDictionary *parameters = @{@"a":@"setKeyword",
                                 @"f":videoFileID,
                                 @"keyword":keywordString,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/video.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)setVideoComment:(NSString *)videoFileID withComment:(NSString *)comment withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"setComment",
                                 @"f":videoFileID,
                                 @"comment":comment,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/video.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

#pragma mark - Movie Info
- (void)getIMDBInfo:(NSString *)videoFileID withSuccessBlock:(QNSuccessBlock)success withFailureblock:(QNFailureBlock)failure{
    
}

#pragma mark - UploadTask Operation
- (void)pauseAllUploadTask{
    if (_uploadTasks) {
        for (NSURLSessionUploadTask *uploadTask in _uploadTasks)
            [uploadTask suspend];
    }
}

- (void)resumeAllUploadTask{
    if (_uploadTasks) {
        for (NSURLSessionUploadTask *uploadTask in _uploadTasks)
            [uploadTask resume];
    }
}

- (void)cancelAllUploadTask{
    if (_uploadTasks) {
        for (NSURLSessionUploadTask *uploadTask in _uploadTasks)
            [uploadTask cancel];
    }
}

- (void)pauseUploadTask:(NSString *)taskID{
    NSURLSessionUploadTask *uploadTask = [self findUploadTask:taskID];
    [uploadTask suspend];
}

- (void)cancelUploadTask:(NSString *)taskID{
    NSURLSessionUploadTask *uploadTask = [self findUploadTask:taskID];
    [uploadTask cancel];
}

- (void)resumeUploadTask:(NSString *)taskID{
    NSURLSessionUploadTask *uploadTask = [self findUploadTask:taskID];
    [uploadTask resume];
}

- (NSURLSessionUploadTask *)findUploadTask:(NSString *)taskID{
    NSURLSessionUploadTask *rTask = nil;
    for (NSURLSessionUploadTask *uploadTask in _uploadTasks) {
        if ([uploadTask.taskDescription isEqualToString:taskID]) {
            rTask = uploadTask;
            break;
        }
    }
    return rTask;
}

#pragma mark - DownloadTask Operation
/**
 Use SYNC here!
 Q: Why you ask SYNC block in background queue?
 A: We don't know where the developers invoke these method and we don't want that there is concurrency between them happened if
 invoked by different threads. Force them running in same GCD queue and avoiding the resource's concurrency problem.
 */
- (void)pauseAllDownloadTasks{
    for (NSURLSessionDownloadTask *downloadTask in _downloadTasks) {
        [downloadTask suspend];
    }
}

- (void)resumeAllDownloadTask{
    for (NSURLSessionDownloadTask *downloadTask in _downloadTasks) {
        [downloadTask resume];
    }
}

- (void)cancelAllDownloadTask:(void(^)(NSString *filmID, NSData *resumeKey))resumeOperation{
    
    for (NSURLSessionDownloadTask *downloadTask in _downloadTasks) {
        [downloadTask cancelByProducingResumeData:^(NSData *resumeData){
            DDLogVerbose(@"resumeData %@", resumeData);
            if (resumeOperation) {
                resumeOperation(downloadTask.taskDescription, resumeData);
            }
        }];
    }
    [_downloadTasks removeAllObjects];
}

- (void)restoreAllDownloadTask:(void(^)(void))restoreBlock{
    restoreBlock();
}

- (void)pauseDownloadTaskWithID:(NSString *)taskID{
    NSURLSessionDownloadTask *thisTask = [self findDownloadTask:taskID];
    [thisTask suspend];
}

- (void)resumeDownloadTaskWithID:(NSString *)taskID{
    NSURLSessionDownloadTask *thisTask = [self findDownloadTask:taskID];
    [thisTask resume];
}

- (void)cancelDownloadTaskWithID:(NSString *)taskID{
    NSURLSessionDownloadTask *thisTask = [self findDownloadTask:taskID];
    [thisTask cancelByProducingResumeData:^(NSData *reumseData){
        //TODO
    }];
    [_downloadTasks removeObject:thisTask];
}

#pragma mark - DLNA & AirPlay
- (void)getRenders:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingForRenderList];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"listRenderers",
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)getPlayers:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingForPlayerList];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"listPlayers",
                                 @"type":@"history",
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)listContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingForContentList];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"listContents",
                                 @"t":renderId,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)getPlayerStatus:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingForPlayState];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"getPlayerStatus",
                                 @"d":renderId,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
    
}

- (void)playContent:(NSString *)renderId withFileId:(NSArray *)fileIds withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"playItems",
                                 @"d":renderId,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    NSMutableString *path = [NSMutableString stringWithFormat:@"video/api/dmc.php"];
    
    BOOL isFirst = YES;
    for (NSString *eachFileID in fileIds) {
        [path appendFormat:(isFirst)?@"?f[]=video|%@":@"&f[]=video|%@", eachFileID];
    }
    NSString *encodingPath = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self.weakRKObjectManager getObject:nil
                                   path:encodingPath
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)stopContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"stop",
                                 @"d":renderId,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)pauseContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"pause",
                                 @"d":renderId,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)nextContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"next",
                                 @"d":renderId,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)previousContent:(NSString *)renderId withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"prev",
                                 @"d":renderId,
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)seekContent:(NSString *)renderId seekTime:(NSInteger)sec withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"seek",
                                 @"d":renderId,
                                 @"s":@(sec),
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)jumpContent:(NSString *)renderId trackOrder:(NSInteger)trackOrder withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"jump",
                                 @"d":renderId,
                                 @"o":@(trackOrder),
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)setRepeatMode:(NSString *)renderId repeatMode:(NSInteger)repeatMode withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"setRepeatMode",
                                 @"d":renderId,
                                 @"r":@(repeatMode),
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

- (void)setVolume:(NSString *)renderId volume:(NSInteger)volume withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNVideoMapping mappingforOnlyStatus];
    RKResponseDescriptor *rDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                     method:RKRequestMethodGET
                                                                                pathPattern:nil
                                                                                    keyPath:@"QDocRoot"
                                                                                statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:rDescriptor];
    NSDictionary *parameters = @{@"a":@"setVolume",
                                 @"d":renderId,
                                 @"v":@(volume),
                                 @"sid":[QNAPCommunicationManager share].sidForMultimedia
                                 };
    
    [self.weakRKObjectManager getObject:nil
                                   path:@"video/api/dmc.php"
                             parameters:parameters
                                success:success
                                failure:failure];
}

#pragma mark - PrivateMethod
/**
 *  <#Description#>
 *
 *  @param infoDic albumID, VideoFileHomePath, destinationPath
 *
 *  @return <#return value description#>
 */
- (NSString *)urlStringForUploadVideoFile:(NSDictionary *)infoDic{
    //TODO, generate the upload url
    //albumID, VideoFileHomePath, destinationPath
    
    NSString *albumID = infoDic[@"albumID"];
    NSInteger h = [infoDic[@"VideoFileHomePath"] intValue];
    NSString *dPath = infoDic[@"destinationPath"];
    
    NSString *rString = [NSString stringWithFormat:@"%@/video/api/upload.php?album=%@&h=%i&d=%@&sid=%@", self.baseURL, albumID, h, dPath, [QNAPCommunicationManager share].sidForMultimedia];
    
    return rString;
}

- (NSString *)sortString:(VideoFileListSortType)sortType{
    NSString *s = nil;
    switch (sortType) {
        case videoFileListSortByColor:
            s = @"color";
            break;
        case videoFileListSortByCreate:
            s = @"create";
            break;
        case videoFileListSortByDBtime:
            s = @"dbtime";
            break;
        case videoFileListSortByModify:
            s = @"modify";
            break;
        case videoFileListSortByName:
            s = @"name";
            break;
        case videoFilelistSortByRating:
            s = @"rating";
            break;
        case videoFileListSortByTimeLine:
            s = @"timeline";
            break;
        case videoFileListSortByRandom:
        default:
            s = @"random";
            break;
    }
    return s;
}

- (NSString *)stringForHomePath:(VideoFileHomePath)homePath{
    //Public/QSync/Private
    NSString *r = nil;
    switch (homePath) {
        case videoFileHomePathPrivate:
            r = @"Private";
            break;
        case videoFileHomePathQSync:
            r = @"QSync";
            break;
        default:
        case videoFileHomePathPublic:
            r = @"Public";
            break;
    }
    return r;
}

- (NSInteger)valueForHomePath:(VideoFileHomePath)homePath{
    NSInteger r = 0;
    switch (homePath) {
        case videoFileHomePathPrivate:
            r = 1;
            break;
        case videoFileHomePathQSync:
            r = 2;
            break;
        default:
        case videoFileHomePathPublic:
            r = 0;
            break;
    }
    return r;
}

- (NSString *)valueForAlbumType:(VideoAlbumType)albumType{
    NSString *r = nil;
    switch (albumType) {
        default:
        case videoAlbums:
            r = @"albums";
            break;
        case videoSmartAlbums:
            r = @"smartAlbums";
            break;
    }
    return r;
}

- (NSURLSessionDownloadTask *)findDownloadTask:(NSString *)taskID{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"taskDescription == %@", taskID];
    NSArray *resultArray = [_downloadTasks filteredArrayUsingPredicate:predicate];
    return ([resultArray count] > 0)?(NSURLSessionDownloadTask *)[resultArray objectAtIndex:0]:nil;
}

- (void)initialCollection{
    if (!_downloadTasks) {
        _downloadTasks = [NSMutableArray array];
    }
    
    if (!_uploadTasks) {
        _uploadTasks = [NSMutableArray array];
    }
}

- (void)saveUUIDIntoFileItem:(QNVideoFileList *)mappingObject withUUID:(NSString *)uuid withAddtionBlock:(void(^)(QNVideoFileItem *fileItem))addtionBlock{
    NSArray *allFileItems = [mappingObject.relationship_FileItem allObjects];
    for (QNVideoFileItem *item in allFileItems) {
        item.uuid = uuid;
        addtionBlock(item);
    }
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

- (NSData *) createBodyWithBoundary:(NSString *)boundary username:(NSString*)username password:(NSString*)password data:(NSData*)data filename:(NSString *)filename
{
    NSMutableData *body = [NSMutableData data];
    
    if (data) {
        //only send these methods when transferring data as well as username and password
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"files[]\"; filename=\"%@\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", [self mimeTypeForPath:filename]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:data];
    }
    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n%@", username] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n%@", password] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *newStr = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
    DDLogVerbose(@"body %@", newStr);
    return body;
}

- (NSString *)boundaryString
{
    // generate boundary string
    //
    // adapted from http://developer.apple.com/library/ios/#samplecode/SimpleURLConnections
    
    CFUUIDRef  uuid;
    NSString  *uuidStr;
    
    uuid = CFUUIDCreate(NULL);
    assert(uuid != NULL);
    
    uuidStr = CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
    assert(uuidStr != NULL);
    
    CFRelease(uuid);
    
    return [NSString stringWithFormat:@"iOSQVideoBoundary-%@", uuidStr];
}

- (NSString *)mimeTypeForPath:(NSString *)path
{
    // get a mime type for an extension using MobileCoreServices.framework
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}
@end
