//
//  QNVideoMapping.m
//  QNAPFramework
//
//  Created by Change.Liao on 13/9/27.
//  Copyright (c) 2013年 QNAP. All rights reserved.
//

#import "QNVideoMapping.h"
#import "QNAPCommunicationManager.h"

@implementation QNVideoMapping
+ (RKEntityMapping *)mappingForLogin{
    return [self entityMapping:@"QNLogin"
        withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                   isXMLParser:YES];
}

+ (RKEntityMapping *)mappingForStatusMessage{
    return [self entityMapping:@"QNVideoResponse"
        withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                   isXMLParser:YES];
}

+ (RKEntityMapping *)mappingforOnlyStatus{
    return [self entityMapping:@"QNNormalSuccessResponse"
        withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                   isXMLParser:YES];
}

+ (RKEntityMapping *)mappingForCreateCollection{
    return [self entityMapping:@"QNVideoCollectionCreateResponse"
        withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                   isXMLParser:YES];
}
+ (RKEntityMapping *)mappingForGetAllVideoList{
    RKEntityMapping * fileListMapping = [RKEntityMapping mappingForEntityForName:@"QNVideoFileList" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    RKEntityMapping * fileItemMapping = [RKEntityMapping mappingForEntityForName:@"QNVideoFileItem" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    [fileListMapping addAttributeMappingsFromDictionary:@{@"status.text":@"status",
                                                          @"videoCount.text":@"videoCount",
                                                          @"Queries.text":@"queries",
                                                          @"KeywordList.text":@"keywordList"
                                                          }];
    [fileItemMapping addAttributeMappingsFromDictionary:[self attributeDictionaryForGetAllVideoItem]];
    [fileItemMapping setIdentificationAttributes:@[@"f_id"]];
    RKRelationshipMapping *relation_FileItem = [RKRelationshipMapping relationshipMappingFromKeyPath:@"DataList.FileItem"
                                                                                           toKeyPath:@"relationship_FileItem"
                                                                                         withMapping:fileItemMapping];
    [fileListMapping addPropertyMapping:relation_FileItem];
    return fileListMapping;
}

+ (RKEntityMapping *)mappingForTimeLineList{
    RKEntityMapping *timeLineResponse = [RKEntityMapping mappingForEntityForName:@"QNVideoTimeLineResponse" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    [timeLineResponse addAttributeMappingsFromDictionary:@{@"status.text":@"status",
                                                           @"Queries.text":@"queries"
                                                           }];
    RKEntityMapping *timeLineListMapping = [RKEntityMapping mappingForEntityForName:@"QNVideoTimeLine"
                                                               inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    [timeLineListMapping addAttributeMappingsFromDictionary:@{
                                                              @"YearMonth.text":@"yearMonth",
                                                              @"Year.text":@"year",
                                                              @"Month.text":@"month",
                                                              @"Count.text":@"count"
                                                              }];
    RKEntityMapping *timeLineDateItem = [RKEntityMapping mappingForEntityForName:@"QNVideoDateItem"
                                                            inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    [timeLineDateItem addAttributeMappingsFromDictionary:@{@"c.text":@"count",
                                                           @"d.text":@"date"
                                                           }];
    RKRelationshipMapping *relation_timeLine = [RKRelationshipMapping relationshipMappingFromKeyPath:@"DataList.Timeline"
                                                                                           toKeyPath:@"relationship_timeLine"
                                                                                         withMapping:timeLineListMapping];
    
    RKRelationshipMapping *relation_dateItem = [RKRelationshipMapping relationshipMappingFromKeyPath:@"date"
                                                                                           toKeyPath:@"relationship_dateItem"
                                                                                         withMapping:timeLineDateItem];
    [timeLineResponse addPropertyMapping:relation_timeLine];
    [timeLineListMapping addPropertyMapping:relation_dateItem];
    
    return timeLineResponse;
}

+ (RKEntityMapping *)mappingforCollectionList{
    RKEntityMapping *collectionsResponse = [RKEntityMapping mappingForEntityForName:@"QNVideoCollectionResponse" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    RKEntityMapping *collectionMapping =
    [RKEntityMapping mappingForEntityForName:@"QNCollection" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
//    [self entityMapping:@"QNCollection" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:YES];
    [collectionMapping addAttributeMappingsFromDictionary:[self attributeDictionaryForGetCollection]];
    RKRelationshipMapping *relation_collection = [RKRelationshipMapping relationshipMappingFromKeyPath:@"DataList.FileItem"
                                                                                             toKeyPath:@"relationship_DataList"
                                                                                           withMapping:collectionMapping];
    [collectionsResponse addPropertyMapping:relation_collection];
    return collectionsResponse;
}

+ (RKEntityMapping *)mappingForFolderItems{
    RKEntityMapping * fileListMapping = [RKEntityMapping mappingForEntityForName:@"QNVideoFileList" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    RKEntityMapping * fileItemMapping = [RKEntityMapping mappingForEntityForName:@"QNVideoFileItem" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    [fileListMapping addAttributeMappingsFromDictionary:@{@"status.text":@"status",
                                                          @"videoCount.text":@"videoCount",
//                                                          @"folderCount.text":@"folderCount",
                                                          @"Queries.text":@"queries",
                                                          }];
    [fileItemMapping addAttributeMappingsFromDictionary:[self attributeDictionaryForGetAllVideoItem]];
    [fileItemMapping setIdentificationAttributes:@[@"f_id"]];
    RKRelationshipMapping *relation_FileItem = [RKRelationshipMapping relationshipMappingFromKeyPath:@"DataList.FileItem"
                                                                                           toKeyPath:@"relationship_FileItem"
                                                                                         withMapping:fileItemMapping];
    [fileListMapping addPropertyMapping:relation_FileItem];
    return fileListMapping;
}

+ (RKEntityMapping *)mappingForRenderList{
    RKEntityMapping * renderListMapping = [RKEntityMapping mappingForEntityForName:@"QNVideoRenderList" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    RKEntityMapping * renderMapping = [self entityMapping:@"QNVideoRender" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:YES];
    
    [renderListMapping addAttributeMappingsFromDictionary:@{@"status.text":@"status",
                                                          @"deviceCount.text":@"deviceCount",
                                                          }];
    [renderMapping setIdentificationAttributes:@[@"mac"]];
    RKRelationshipMapping *relation_render = [RKRelationshipMapping relationshipMappingFromKeyPath:@"devices.device"
                                                                                           toKeyPath:@"relationship_Renders"
                                                                                         withMapping:renderMapping];
    [renderListMapping addPropertyMapping:relation_render];
    return renderListMapping;
}

+ (RKEntityMapping *)mappingForPlayerList{
    RKEntityMapping *playerListMapping = [RKEntityMapping mappingForEntityForName:@"QNVideoPlayerList" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    RKEntityMapping *playerMapping = [self entityMapping:@"QNVideoPlayer" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:YES];
    [playerListMapping addAttributeMappingsFromDictionary:@{@"status.text":@"status",
                                                            @"playCount.text":@"playCount",
                                                            }];
    [playerMapping setIdentificationAttributes:@[@"renderID"]];
    RKRelationshipMapping *relation_player = [RKRelationshipMapping relationshipMappingFromKeyPath:@"players.player" toKeyPath:@"relationship_player" withMapping:playerMapping];
    [playerListMapping addPropertyMapping:relation_player];
    return playerListMapping;
}

+ (RKEntityMapping *)mappingForContentList{
    RKEntityMapping *contentListMapping = [RKEntityMapping mappingForEntityForName:@"QNVideoContentList" inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    RKEntityMapping *contentMapping = [self entityMapping:@"QNVideoContentItem" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:YES];
    [contentListMapping addAttributeMappingsFromDictionary:@{@"status.text":@"status",
                                                            @"itemCount.text":@"itemCount",
                                                            }];
    [contentMapping setIdentificationAttributes:@[@"upnpID", @"playlistID", @"orderNum"]];
    RKRelationshipMapping *relation_content = [RKRelationshipMapping relationshipMappingFromKeyPath:@"items.item" toKeyPath:@"relationship_contentItem" withMapping:contentMapping];
    [contentListMapping addPropertyMapping:relation_content];
    return contentListMapping;
}

+ (RKEntityMapping *)mappingForPlayState{
    RKEntityMapping *playStateMapping = [self entityMapping:@"QNVideoPlayerStatus" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:YES];
    return playStateMapping;
}

+ (NSDictionary *)attributeDictionaryForGetCollection{
    return @{@"DateCreated.text":@"dateCreated",
             @"DateModified.text":@"dateModified",
             @"Config.text":@"config",
             @"InvalidFlag.text":@"invalidFlag",
             @"ProtectionStatus.text":@"protectionStatus",
             @"ProtectionStatus2.text":@"protectionStatus2",
             @"VideoCount.text":@"videoCount",
             @"albumType.text":@"albumType",
             @"cAlbumTitle.text":@"cAlbumTitle",
             @"coverCode.text":@"coverCode",
             @"coverHeight.text":@"coverHeight",
             @"coverType.text":@"coverType",
             @"coverWidth.text":@"coverWidth",
             @"editbyo.text":@"editbyo",
             @"expiration.text":@"expiration",
             @"iAlbumCover.text":@"iAlbumCover",
             @"isExpired.text":@"isExpired",
             @"iVideoAlbumId.text":@"iVideoAlbumId",
             @"owner.text":@"owner",
             @"public.text":@"public",
             @"qts.text":@"qts",
             @"shared.text":@"shared"
             };
}
+ (NSDictionary *)attributeDictionaryForGetAllVideoItem{
    return @{
             @"MediaType.text":@"mediaType",
             @"id.text":@"f_id",
             @"cFilename.text":@"cFileName",
             @"cPictureTitle.text":@"cPictureTitle",
             @"comment.text":@"comment",
             @"mime.text":@"mime",
             @"iFileSize.text":@"iFileSize",
             @"iWidth.text":@"iWidth",
             @"iHeight.text":@"iHeight",
             @"Duration.text":@"duration",
             @"DateCreated.text":@"dateCreated",
             @"DateModified.text":@"dateModified",
             @"AddToDbTime.text":@"addToDbTime",
             @"YearMonth.text":@"yearMonth",
             @"YearMonthDay.text":@"yearMonthDay",
             @"dateTime.text":@"dateTime",
             @"status.text":@"status",
             @"TranscodeStatus.text":@"transcodeStatus",
             @"ColorLevel.text":@"colorLevel",
             @"Orientation.text":@"orientation",
             @"mask.text":@"mask",
             @"prefix.text":@"prefix",
             @"keywords.text":@"keywords",
             @"rating.text":@"rating",
             @"uid.text":@"uid",
             @"ImportYearMonthDay.text":@"importYearMonthDay",
             @"V720P.text":@"v720P",
             @"V360P.text":@"v360P",
             @"V240P.text":@"v240P",
             @"V480P.text":@"v480P",
             @"V1080P.text":@"v1080P",
             @"new.text":@"isNew"
             };
}

@end
