//
//  QNAppCenterAPIManager.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/3.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNModuleBaseObject.h"
#import "QNAPFramework.h"

@interface QNAppCenterAPIManager : QNModuleBaseObject

- (void)getNASServiceStatus:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;
- (void)installQPKGFromURL:(NSString *)url withInternalName:(NSString *)internalName withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;
- (void)settingQPKGActivationWithQPKGName:(NSString *)qpkgName enableValue:(BOOL)enable withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;
- (void)searchQPKGDownloadInfo:(NSString *)targetStationName withSuccessBlock:(QNQPKGItemSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;
- (void)getInstallStatus:(QNQNQPKGInstallStatusSuccessBlock)success withFailureBlock:(QNFailureBlock)failure;

@end
