//
//  QPKGPlatform.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/7.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QPKGPlatform : NSManagedObject

@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * platformID;

@end
