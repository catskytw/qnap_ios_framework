//
//  QNService.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/7.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNService : NSManagedObject

@property (nonatomic, retain) NSNumber * allowed;
@property (nonatomic, retain) NSNumber * enabled;
@property (nonatomic, retain) NSNumber * iconShowUp;
@property (nonatomic, retain) NSNumber * installed;
@property (nonatomic, retain) NSNumber * port;
@property (nonatomic, retain) NSNumber * portEnabled;
@property (nonatomic, retain) NSNumber * removed;
@property (nonatomic, retain) NSString * service_id;
@property (nonatomic, retain) NSNumber * sslEnalbed;
@property (nonatomic, retain) NSNumber * sslPort;
@property (nonatomic, retain) NSNumber * supported;
@property (nonatomic, retain) NSNumber * upgraded;
@property (nonatomic, retain) NSString * url;

@end
