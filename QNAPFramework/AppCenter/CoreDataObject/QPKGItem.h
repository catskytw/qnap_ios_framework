//
//  QPKGItem.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/23.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QPKGPlatform;

@interface QPKGItem : NSManagedObject

@property (nonatomic, retain) NSString * bannerImg;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * developer;
@property (nonatomic, retain) NSString * forumLink;
@property (nonatomic, retain) NSString * fwVersion;
@property (nonatomic, retain) NSString * icon80;
@property (nonatomic, retain) NSString * icon100;
@property (nonatomic, retain) NSString * internalName;
@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSString * maintainer;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * qpkg_description;
@property (nonatomic, retain) NSString * snapshot;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * version;
@property (nonatomic, retain) NSSet *relationship_platform;
@end

@interface QPKGItem (CoreDataGeneratedAccessors)

- (void)addRelationship_platformObject:(QPKGPlatform *)value;
- (void)removeRelationship_platformObject:(QPKGPlatform *)value;
- (void)addRelationship_platform:(NSSet *)values;
- (void)removeRelationship_platform:(NSSet *)values;

@end
