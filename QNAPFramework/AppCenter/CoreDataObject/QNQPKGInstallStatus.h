//
//  QNQPKGInstallStatus.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/7.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNQPKGInstallStatus : NSManagedObject

@property (nonatomic, retain) NSNumber * updateStatus;
@property (nonatomic, retain) NSString * updateQPKG;
@property (nonatomic, retain) NSString * processQPKG;
@property (nonatomic, retain) NSNumber * downloadStatus;
@property (nonatomic, retain) NSNumber * downloadPercent;
@property (nonatomic, retain) NSString * lastQPKGN;
@property (nonatomic, retain) NSString * lastQPKGDN;

@end
