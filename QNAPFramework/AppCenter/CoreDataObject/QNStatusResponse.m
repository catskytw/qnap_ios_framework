//
//  QNStatusResponse.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/23.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNStatusResponse.h"


@implementation QNStatusResponse

@dynamic status;
@dynamic authPassed;

@end
