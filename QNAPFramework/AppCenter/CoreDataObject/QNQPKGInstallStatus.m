//
//  QNQPKGInstallStatus.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/7.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNQPKGInstallStatus.h"


@implementation QNQPKGInstallStatus

@dynamic updateStatus;
@dynamic updateQPKG;
@dynamic processQPKG;
@dynamic downloadStatus;
@dynamic downloadPercent;
@dynamic lastQPKGN;
@dynamic lastQPKGDN;

@end
