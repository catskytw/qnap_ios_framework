//
//  QNAppCenterResponse.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/3.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNAppCenterResponse : NSManagedObject

@property (nonatomic, retain) NSNumber * authPassed;
@property (nonatomic, retain) NSString * model;
@property (nonatomic, retain) NSString * firmware;
@property (nonatomic, retain) NSString * specVersion;
@property (nonatomic, retain) NSString * hostName;

@end
