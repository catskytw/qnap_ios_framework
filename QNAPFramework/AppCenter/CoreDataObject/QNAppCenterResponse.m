//
//  QNAppCenterResponse.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/3.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNAppCenterResponse.h"


@implementation QNAppCenterResponse

@dynamic authPassed;
@dynamic model;
@dynamic firmware;
@dynamic specVersion;
@dynamic hostName;

@end
