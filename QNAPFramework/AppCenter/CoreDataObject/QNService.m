//
//  QNService.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/7.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNService.h"


@implementation QNService

@dynamic allowed;
@dynamic enabled;
@dynamic iconShowUp;
@dynamic installed;
@dynamic port;
@dynamic portEnabled;
@dynamic removed;
@dynamic service_id;
@dynamic sslEnalbed;
@dynamic sslPort;
@dynamic supported;
@dynamic upgraded;
@dynamic url;

@end
