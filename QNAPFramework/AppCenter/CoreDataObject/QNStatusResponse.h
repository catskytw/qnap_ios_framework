//
//  QNStatusResponse.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/23.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface QNStatusResponse : NSManagedObject

@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSNumber * authPassed;

@end
