//
//  QPKGItem.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/6/23.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QPKGItem.h"
#import "QPKGPlatform.h"


@implementation QPKGItem

@dynamic bannerImg;
@dynamic category;
@dynamic developer;
@dynamic forumLink;
@dynamic fwVersion;
@dynamic icon80;
@dynamic icon100;
@dynamic internalName;
@dynamic language;
@dynamic maintainer;
@dynamic name;
@dynamic qpkg_description;
@dynamic snapshot;
@dynamic type;
@dynamic version;
@dynamic relationship_platform;

@end
