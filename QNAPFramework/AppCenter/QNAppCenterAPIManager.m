//
//  QNAppCenterAPIManager.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/3.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNAppCenterAPIManager.h"
#import "QNAppCenterMapping.h"
#import "QPKGItem.h"
#import "QNAPCommunicationManager.h"
#import "QNStatusResponse.h"

@implementation QNAppCenterAPIManager
- (void)getNASServiceStatus:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNAppCenterMapping mappingForService];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot.serivceList.service"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    [self.weakRKObjectManager getObject:nil
                                    path:@"cgi-bin/sysinfoReq.cgi"
                              parameters:nil
                                 success:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                     DDLogVerbose(@"lookup services' status successfully!");
                                     success(o, r);
                                 }
                                 failure:^(RKObjectRequestOperation *o, NSError *e){
                                     DDLogError(@"error while looking up the services' status");
                                     failure(o, e);
                                 }];

}

- (void)installQPKGFromURL:(NSString *)url withInternalName:(NSString *)internalName withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNAppCenterMapping mappingForAppCenterResponse];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    NSDictionary *dic = @{@"subfunc":@"qpkg",
                          @"sid":[QNAPCommunicationManager share].sidForQTS,
                          @"apply":@"8",
                          @"isZip":@"1",
                          @"qname":internalName,
                          @"location":url
                          };
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    [self.weakRKObjectManager postObject:nil
                                   path:@"cgi-bin/application/appRequest.cgi"
                             parameters:dic
                                success:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                    success(o, r);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    DDLogError(@"error while looking up the services' status");
                                    failure(o, e);
                                }];
}

- (void)settingQPKGActivationWithQPKGName:(NSString *)qpkgName enableValue:(BOOL)enable withSuccessBlock:(QNSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    NSInteger activateValue = (enable)?3:4;
    RKEntityMapping *mapping = [QNAppCenterMapping mappingForStatus];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    NSDictionary *dic = @{@"subfunc":@"qpkg",
                          @"sid":[QNAPCommunicationManager share].sidForQTS,
                          @"apply":@(activateValue),
                          @"qname":qpkgName,
                          };
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    [self.weakRKObjectManager postObject:nil
                                    path:@"cgi-bin/application/appRequest.cgi"
                              parameters:dic
                                 success:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                     QNStatusResponse *response = (QNStatusResponse *)r.firstObject;
                                     if ([response.authPassed boolValue]) {
                                         success(o, r);
                                     }else
                                         failure(o, [NSError errorWithDomain:@"failed in authPassed" code:-99 userInfo:nil]);

                                 }
                                 failure:^(RKObjectRequestOperation *o, NSError *e){
                                     DDLogError(@"error while activating qpkg: %@", e);
                                     failure(o, e);
                                 }];
}

- (void)searchQPKGDownloadInfo:(NSString *)targetStationName withSuccessBlock:(QNQPKGItemSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKMapping *qpkgItemMapping = [QNAppCenterMapping mappingForQPKGItem];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:qpkgItemMapping
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:nil
                                                                                           keyPath:@"plugins.item"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    [self.weakRKObjectManager getObject:nil
                                   path:@"RSS/rssdoc/qpkgcenter_eng.xml"
                             parameters:nil
                                success:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                    DDLogVerbose(@"fetching qpkg-items successfully!");
                                    for (QPKGItem *item in r.array) {
                                        if ([item.internalName isEqualToString:targetStationName]) {
                                            success(o, r, item);
                                            return;
                                        }
                                    }
                                    failure(o, [NSError errorWithDomain:NSLocalizedString(@"NoQPKGInfo", nil)
                                                                   code:9999
                                                               userInfo:nil]);
                                }
                                failure:^(RKObjectRequestOperation *o, NSError *e){
                                    DDLogError(@"error while looking up the services' status");
                                    failure(o, e);
                                }];

}

- (void)getInstallStatus:(QNQNQPKGInstallStatusSuccessBlock)success withFailureBlock:(QNFailureBlock)failure{
    RKEntityMapping *mapping = [QNAppCenterMapping mappingForInstallStatus];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:nil
                                                                                           keyPath:@"QDocRoot.func.ownContent"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    NSDictionary *dic = @{@"subfunc":@"qpkg",
                          @"sid":[QNAPCommunicationManager share].sidForQTS,
                          @"apply":@(1),
                          @"getstatus":@(1),
                          };
    [self.weakRKObjectManager addResponseDescriptor:responseDescriptor];
    [self.weakRKObjectManager postObject:nil
                                    path:@"cgi-bin/application/appRequest.cgi"
                              parameters:dic
                                 success:^(RKObjectRequestOperation *o, RKMappingResult *r){
                                     DDLogVerbose(@"active qpkg successfully!, %@", r.firstObject);
                                     success(o, r, r.firstObject);
                                 }
                                 failure:^(RKObjectRequestOperation *o, NSError *e){
                                     DDLogError(@"error while activating qpkg: %@", e);
                                     failure(o, e);
                                 }];

}
@end
