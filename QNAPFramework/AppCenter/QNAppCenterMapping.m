//
//  QNAppCenterMapping.m
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/3.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNAppCenterMapping.h"
#import "QNAPCommunicationManager.h"

@implementation QNAppCenterMapping

+ (RKEntityMapping *)mappingForService{
    RKEntityMapping *serviceMapping = [RKEntityMapping mappingForEntityForName:@"QNService"
                                                          inManagedObjectStore:[QNAPCommunicationManager share].objectManager];
    [serviceMapping addAttributeMappingsFromDictionary:[QNAppCenterMapping attributeDictionaryForService]];
    [serviceMapping setIdentificationAttributes:@[@"service_id"]];
    return serviceMapping;
}

+ (RKEntityMapping *)mappingForAppCenterResponse{
    return [self entityMapping:@"QNAppCenterResponse"
        withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                   isXMLParser:YES];
}

+ (RKEntityMapping *)mappingForQPKGItem{
    RKEntityMapping *itemMapping = [self entityMapping:@"QPKGItem"
                                withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                                           isXMLParser:YES];
    
    //TODO you should remove the description-mapping before add another description-mapping
//    [itemMapping addAttributeMappingsFromDictionary:@{@"description.text":@"qpkg_description"}];
    RKEntityMapping *platformMapping = [self entityMapping:@"QPKGPlatform" withManagerObjectStore:[QNAPCommunicationManager share].objectManager isXMLParser:YES];
    [platformMapping setIdentificationAttributes:@[@"location", @"platformID"]];
    RKRelationshipMapping *relation_platform = [RKRelationshipMapping relationshipMappingFromKeyPath:@"platform"
                                                                                           toKeyPath:@"relationship_platform"
                                                                                         withMapping:platformMapping];

    [itemMapping addPropertyMapping:relation_platform];

    [itemMapping setIdentificationAttributes:@[@"internalName"]];
    return itemMapping;

}

+ (RKEntityMapping *)mappingForStatus{
    return [self entityMapping:@"QNStatusResponse"
        withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                   isXMLParser:YES];
}

+ (RKEntityMapping *)mappingForInstallStatus{
    return [self entityMapping:@"QNQPKGInstallStatus"
        withManagerObjectStore:[QNAPCommunicationManager share].objectManager
                   isXMLParser:YES];
}

+ (NSDictionary *)attributeDictionaryForService{
    return @{@"allowed.text":@"allowed",
             @"enabled.text":@"enabled",
             @"iconShowUp.text":@"iconShowUp",
             @"installed.text":@"installed",
             @"port.text":@"port",
             @"portEnabled.text":@"portEnabled",
             @"removed.text":@"removed",
             @"sslEnalbed.text":@"sslEnalbed",
             @"sslPort.text":@"sslPort",
             @"supported.text":@"supported",
             @"upgraded.text":@"upgraded",
             @"url.text":@"url",
             @"id.text":@"service_id"
             };
}
@end
