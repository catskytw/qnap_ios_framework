//
//  QNAppCenterMapping.h
//  QNAPFramework
//
//  Created by Change.Liao on 2014/4/3.
//  Copyright (c) 2014年 QNAP. All rights reserved.
//

#import "QNMappingProtoType.h"

@interface QNAppCenterMapping : QNMappingProtoType

+ (RKEntityMapping *)mappingForService;
+ (RKEntityMapping *)mappingForAppCenterResponse;
+ (RKEntityMapping *)mappingForQPKGItem;
+ (RKEntityMapping *)mappingForStatus;
+ (RKEntityMapping *)mappingForInstallStatus;

+ (NSDictionary *)attributeDictionaryForService;

@end
