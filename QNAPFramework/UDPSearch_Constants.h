

////////////////////////////////////////////
#define	DATA_TAG_MAX_SIZE			128
#define EXT_DATA_TAG_MAX_SIZE       123
#define	MAX_TAG_DATA_SIZE			23400

//	define Tag base protocol
//
#define TAG_NULL                0
#define TAG_SERVER_NAME         1		// DATA: 16 Bytes
#define TAG_VERSION             2		//
#define TAG_DATE                3		// DATA: 8 Bytes
#define TAG_TIME                4		// DATA: 6 Bytes
#define TAG_ETH00_IP_ADDRESS	5		// DATA: 4 Bytes
#define TAG_ETH00_GATEWAY		6		// DATA: 4 Bytes
#define TAG_ETH00_SUBNET_MASK	7		// DATA: 4 Bytes
#define TAG_ETH00_DHCP			8		// DATA: 1 Bytes
#define TAG_WORKGROUP           9		// DATA: 16 Bytes	
#define TAG_PASSWORD            10
#define TAG_ADMIN_NAME          11
#define TAG_ETH00_MAC_ADDRESS	12
#define TAG_DISK_NUMBER         13		// configurable disk number, originally TagDiskNum


#define TAG_REBOOT              16
#define TAG_NEW_PROTOCOL        17		// new: 2+ nics
#define TAG_PPPOE_USER          18			
#define TAG_PPPOE_PASSWORD      19			
#define TAG_PPTP_IP             20
#define TAG_PPTP_USER           21
#define TAG_PPTP_PASSWORD       22

#define TAG_ETH01_IP_ADDRESS	23
#define TAG_ETH01_GATEWAY		24
#define TAG_ETH01_SUBNET_MASK	25
#define TAG_ETH01_DHCP			26
#define TAG_ETH01_MAC_ADDRESS	27
#define TAG_ETH01_DHCP_SERVER	28
#define TAG_ETH01_DHCP_START	29
#define TAR_ETH01_DHCP_END      30
#define TAG_MODEL_NAME          40
#define TAG_EXTENSION           202
#define TAG_PORT_NUMBER			53		// specify TCP port number...
#define TAG_EXT_SECURE_ADMINPORT_NUMBER		263


#define kDefaultPortUDP         8097

typedef struct FINDER_CMD_HEADER 
{
    unsigned char Preamble[8];
    unsigned short SeqNum;
    unsigned short Cmd;
    unsigned short Result;
    unsigned short pad;
} FINDER_CMD_HEADER;

typedef struct 
{
	unsigned char tag;
	unsigned char len;
	unsigned char data[DATA_TAG_MAX_SIZE];
}	DATA_TAG;

typedef struct
{
    unsigned int tag;
    unsigned char len;
    unsigned char data[EXT_DATA_TAG_MAX_SIZE];
} EXT_DATA_TAG;

// Struct should be 8bytes alignment
typedef struct FINDER_CMD_PACKET 
{
    FINDER_CMD_HEADER Cmd_Hdr;
    DATA_TAG   Tag_List[];
} FINDER_CMD_PACKET;